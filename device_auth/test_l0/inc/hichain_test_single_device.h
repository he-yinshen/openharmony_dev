#ifndef HICHAIN_TEST_SINGLE_DEVICE_H
#define HICHAIN_TEST_SINGLE_DEVICE_H

#ifdef __cplusplus
extern "C" {
#endif

bool TestRegCallback(void);
void TestUnregCallback(void);
void TestRegDataChangeListener(void);
void TestUnregDataChangeListener(void);
bool TestCreateGroup(void);
void TestAddMember(void);
void TestDeleteMember(void);
void TestAuthDevice(void);
void TestCheckAccessToGroup(void);
void TestGetPkInfoList(void);
void TestGetGroupInfoById(void);
void TestGetGroupInfo(void);
void TestGetJoinedGroups(void);
void TestGetRelatedGroups(void);
void TestGetDeviceInfoById(void);
void TestGetTrustedDevices(void);
void TestIsDeviceInGroup(void);
void TestDeleteGroup(void);
void TestBindAndCancel(void);
void TestAuthAndCancel(void);

#ifdef __cplusplus
}
#endif

#endif