#ifndef HICHAIN_TEST_DOUBLE_DEVICE_H
#define HICHAIN_TEST_DOUBLE_DEVICE_H

#ifdef __cplusplus
extern "C" {
#endif

void TestBindAuthDoubleDevice(void);

#ifdef __cplusplus
}
#endif

#endif