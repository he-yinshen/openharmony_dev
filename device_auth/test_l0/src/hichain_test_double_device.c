#include "hichain_test_double_device.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <unistd.h>
#include <getopt.h>
#include "hc_log.h"
#include "hc_mutex.h"
#include "device_auth.h"
#include "securec.h"
#include "json_utils.h"
#include "string_util.h"
#include "hc_tlv_parser.h"
#include "alg_loader.h"
#include "account_module_defines.h"
#include "broadcast_manager.h"
#include "common_defs.h"
#include <pthread.h>
#include "hc_condition.h"
#include "alg_loader.h"
#include "group_operation_common.h"

#define SUCCESS 0
#define FAIL 1
#define PORT 10001
#define CONCURRENT_NUM 20

#define TEST_APP_NAME "com.huawei.security"
#define TEST_PIN_CODE "123456"
#define TEST_REQUEST_ID 123
#define TEST_REQUEST_ID2 321
#define TEST_REQUEST_ID3 132

#define MAX_AUTH_DATA_SIZE 2048

char g_data[MAX_AUTH_DATA_SIZE] = {0};

// client端和server端需要分别将此变量设置为true和false
static int g_isClient = 0;
// static int g_retryTimes = 0;
static int clientfd = -1;
static char localSessionKey[128] = {0};
static int localSessionKeyLen = 0;
static int stopTask = 0;
static int isError = 0;
static int isProcessing = 0;
// 如果是认证，需要将此变量设置为true
static bool g_isAuth = false;
// 默认是false，L2上支持命令行设置，L0上不支持
static bool g_isConcurrent = false;
static const GroupAuthManager *g_gaInstance = NULL;
static const DeviceGroupManager *g_gmInstance = NULL;
static DeviceAuthCallback gmCallback = { NULL };
static DeviceAuthCallback gaCallback;
static DataChangeListener g_testListener = { NULL };
static HcCondition g_testCondition;
static const AlgLoader *g_algLoader = NULL;
static HcMutex *g_mutex;

static char g_peerUdid[128] = "82BF9E41059C5CF39E5E406E412E3698229BDA05E2816E4712B375FE807D01E9";
static char g_clientUdid[128] = "5420459D93FE773F9945FD64277FBA2CAB8FB996DDC1D0B97676FBB1242B3930";
static char g_serverUdid[128] = "52E2706717D5C39D736E134CC1E3BE1BAA2AA52DB7C76A37C749558BD2E6492C";
char g_groupId[128] = "FF9B5A053D74CC8835DB98FAADBCE1A8E4C441637A3A170CB95D4DDD83319C49";

void Test_OnGroupCreated(const char *groupInfo)
{
    if (groupInfo == NULL) {
        printf("not expected\n");
        return;
    }
    printf("-------------OnGroupCreated---------------\n");
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnGroupCreated---------------\n");
}

void Test_OnGroupDeleted(const char *groupInfo)
{
    if (groupInfo == NULL) {
        printf("not expected\n");
        return;
    }
    printf("-------------OnGroupDeleted---------------\n");
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnGroupDeleted---------------\n");
}

void Test_OnDeviceBound(const char *peerUdid, const char* groupInfo)
{
    printf("-------------OnDeviceBound---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnDeviceBound---------------\n");
}
void Test_OnDeviceUnBound(const char *peerUdid, const char* groupInfo)
{
    printf("-------------OnDeviceUnBound---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnDeviceUnBound---------------\n");
}
void Test_OnDeviceNotTrusted(const char *peerUdid)
{
    printf("-------------OnDeviceNotTrusted---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("-------------OnDeviceNotTrusted---------------\n");
}
void Test_OnLastGroupDeleted(const char *peerUdid, int groupType)
{
    printf("-------------OnLastGroupDeleted---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupType:\n%d\n", groupType);
    printf("-------------OnLastGroupDeleted---------------\n");
}
void Test_OnTrustedDeviceNumChanged(int curTrustedDeviceNum)
{
    printf("-------------OnTrustedDeviceNumChanged---------------\n");
    printf("CurTrustedDeviceNum:\n%d\n", curTrustedDeviceNum);
    printf("-------------OnTrustedDeviceNumChanged---------------\n");
}

static bool OnTransmitNet(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    int32_t cnt;
    printf("-------------OnTransmit---------------\n");
    printf("|  RequestId: %-30lld  |\n", requestId);
    printf("|  SendData: %s  |\n", (char *)data);
    CJson *json = CreateJsonFromString((char *)data);
    int64_t tmpReqId = 0;
    if (GetInt64FromJson(json, FIELD_REQUEST_ID, &tmpReqId) != 0) {
        AddInt64StringToJson(json, FIELD_REQUEST_ID, requestId);
    }
    char *sendData = PackJsonToString(json);
    FreeJson(json);
    uint32_t sendDataLen = strlen(sendData) + 1;
    cnt = send(clientfd, (uint8_t *)sendData, sendDataLen, 0);
    FreeJsonString(sendData);
    usleep(100000);
    // sleep(1);
    printf("send size %d, process size %d\n", dataLen, cnt);
    return true;
}

// static void TestDeleteMember(void)
// {
//     printf("start to delete member.\n");
//     CJson *delParam = CreateJson();
//     AddStringToJson(delParam, FIELD_DELETE_ID, "server");
//     AddStringToJson(delParam, FIELD_GROUP_ID, g_groupId);
//     char * delParamStr = PackJsonToString(delParam);
//     FreeJson(delParam);
//     int ret = g_gmInstance->deleteMemberFromGroup(0, TEST_REQUEST_ID3, TEST_APP_NAME, delParamStr);
//     FreeJsonString(delParamStr);
//     printf("call delete member result: %d.\n", ret);
// }

static void OnSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    printf("-------------OnSessionKeyReturned---------------\n");
    printf("|  RequestId: %-40lld  |\n", requestId);
    printf("|  SessionKey: %-36d  |\n", sessionKeyLen);
    memcpy(localSessionKey, sessionKey, sessionKeyLen);
    localSessionKeyLen = sessionKeyLen;
    printf("-------------OnSessionKeyReturned---------------\n");
}

static int TestAuthDevice(bool isFromMainThread)
{
    CJson *authParam = CreateJson();
    // server udid.
    AddStringToJson(authParam, FIELD_PEER_CONN_DEVICE_ID, g_serverUdid);
    AddStringToJson(authParam, FIELD_SERVICE_PKG_NAME, TEST_APP_NAME);
    AddBoolToJson(authParam, FIELD_IS_CLIENT, true);
    char *authParamStr = PackJsonToString(authParam);
    FreeJson(authParam);
    int ret = -1;
    if (g_isConcurrent) {
        for (int i = 1; i <= CONCURRENT_NUM; i++) {
            ret = g_gaInstance->authDevice(0, i, authParamStr, &gaCallback);
        }
    } else {
        ret = g_gaInstance->authDevice(0, TEST_REQUEST_ID2, authParamStr, &gaCallback);
    }
    FreeJsonString(authParamStr);
    if (isFromMainThread) { // 如果已经绑定过，是从主线程直接发起认证的，则主线程等待
        // while(1) {}; // 一直循环可能会导致线程栈溢出
        g_testCondition.wait(&g_testCondition);
    }
    return ret;
}

static void OnFinish(int64_t requestId, int operationCode, const char *authReturn)
{
    printf("-------------OnFinish---------------\n");
    printf("|  RequestId: %-27lld  |\n", requestId);
    printf("|  OperationCode: %-23d  |\n", operationCode);
    printf("|  ReturnData: %-26s  |\n", authReturn);
    printf("-------------OnFinish---------------\n");
    if (operationCode == 4) {
        printf("delete member done, %s\n", authReturn);
        g_testCondition.notify(&g_testCondition); // 解绑成功之后通知主线程退出
        return;
    }
    CJson *dataJson = CreateJsonFromString(authReturn);
    const char* groupId = NULL;
    if (dataJson != NULL) {
        groupId = GetStringFromJson(dataJson, FIELD_GROUP_ID);
        if (groupId != NULL) {
            memcpy_s(g_groupId, strlen(groupId), groupId, strlen(groupId));
            g_groupId[strlen(groupId)] = 0;
        }
        FreeJson(dataJson);
    }
    printf("g_groupId: %s\n", strlen(g_groupId) > 0 ? g_groupId : "empty\n");
    if (operationCode == 0 && !g_isAuth) {
        // 此处只用来通知主线程群组创建成功，因为创建群组和认证onFinish的operationCode都是0，所以要加个!g_isAuth的限制
        g_testCondition.notify(&g_testCondition);
    }
    if (operationCode == 2) {
        // 绑定成功之后，如果是client端，则触发认证，需要将g_isAuth置为true
        g_isAuth = true;
        if (g_isClient) {
            printf("start to auth device\n");
            // 如果绑定成功之后需要进行并发认证的测试，则要将g_isConcurrent置为true
            // g_isConcurrent = true;
            TestAuthDevice(false);
        }
    } else {
        if (g_isAuth) {
            // 下面的4段逻辑用于测试4种不同的场景，每次只能测试一种场景，需要将其他3种场景的逻辑注释掉

            // 如果认证成功之后不进行其他操作，则通知主线程退出
            if (g_isClient) {
                g_testCondition.notify(&g_testCondition);
            }

            // 下面的逻辑用于解绑，如果是client端，认证成功之后触发解绑，需要将g_isAuth置为false
            // g_isAuth = false;
            // if (g_isClient) {
            //     TestDeleteMember();
            // }

            // 下面的逻辑用于连续认证，如果是client端，每认证成功一次，重新触发一次新的认证，并将g_retryTimes+1，当g_retryTimes达到20时，通知主线程退出
            // if (g_isClient) {
            //     if (g_retryTimes < 20) {
            //         g_retryTimes++;
            //         TestAuthDevice(false);
            //     } else {
            //         g_testCondition.notify(&g_testCondition);
            //     }
            // }

            // 下面的逻辑用于并发认证，如果是client端，每认证成功一次，将g_retryTimes+1，当g_retryTimes达到20时，通知主线程退出
            // if (g_isClient) {
            //     g_mutex->lock(g_mutex);
            //     if (g_retryTimes < 20) {
            //         g_retryTimes++;
            //         g_mutex->unlock(g_mutex);
            //     } else {
            //         g_mutex->unlock(g_mutex);
            //         g_testCondition.notify(&g_testCondition);
            //     }
            // }
        }
    }
}

static void OnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    (void)errorReturn;
    printf("-------------OnError---------------\n");
    printf("|  RequestId: %-27lld  |\n", requestId);
    printf("|  OperationCode: %-23d  |\n", operationCode);
    printf("|  ErrorCode: %-27d  |\n", errorCode);
    printf("-------------OnError---------------\n");
    if (operationCode == 0) {
        return;
    }
    isError = 1;
}

static char *OnBindRequestController(int64_t requestId, int operationCode, const char* reqParam)
{
    printf("-------------OnBindRequestController---------------\n");
    printf("|  RequestId: %-29lld  |\n", requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParam: %s  |\n", reqParam);
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, 0);
    AddStringToJson(json, FIELD_PIN_CODE, TEST_PIN_CODE);
    AddStringToJson(json, FIELD_DEVICE_ID, "server"); // 群组的deviceId字段
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    printf("|  returnDataStr: %s  |\n", returnDataStr);
    printf("-------------OnBindRequestController---------------\n");
    return returnDataStr;
}

static char *OnAuthRequestController(int64_t requestId, int operationCode, const char* reqParam)
{
    printf("-------------OnAuthRequestController---------------\n");
    printf("|  RequestId: %-29lld  |\n", requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParam: %s  |\n", reqParam);
    printf("-------------OnAuthRequestController---------------\n");
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, 0);
    // client udid.
    AddStringToJson(json, FIELD_PEER_CONN_DEVICE_ID, g_clientUdid);
    AddStringToJson(json, FIELD_SERVICE_PKG_NAME, TEST_APP_NAME);
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    return returnDataStr;
}

static int TestOpenSocket(in_addr_t dstlp, short dstPort) {
    // printf("connect ip: %s, dest port:%d\n", inet_ntoa(*((struct in_addr*)&dstlp)), dstPort);
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1) {
        printf("socket failed\n");
        return -1;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = dstlp;
    addr.sin_port = htons(dstPort);

    int res = connect(sockfd, (struct sockaddr *)&addr, sizeof(addr));
    if(res < 0) {
        // printf("connect failed\n");
        close(sockfd);
        return -1;
    } else if (res == 0) {
        printf("connect ip: %s, dest port: %d success\n", inet_ntoa(*((struct in_addr*)&dstlp)), dstPort);
    }
    return sockfd;
}

static void LocalForceDelMember(void)
{
    CJson *delParam = CreateJson();
    // delete peer device from group
    AddStringToJson(delParam, FIELD_DEVICE_ID, g_peerUdid);
    AddStringToJson(delParam, FIELD_GROUP_ID, g_groupId);
    AddBoolToJson(delParam, FIELD_IS_FORCE_DELETE, true);
    char * delParamStr = PackJsonToString(delParam);
    FreeJson(delParam);
    g_gmInstance = GetGmInstance();
    g_gmInstance->deleteMemberFromGroup(0, 1234567890000001, TEST_APP_NAME, delParamStr);
    FreeJsonString(delParamStr);
}

static int64_t GetReqIdFromData(const char *data)
{
    CJson *json = CreateJsonFromString(data);
    int64_t reqId = 123;
    GetInt64FromJson(json, FIELD_REQUEST_ID, &reqId);
    FreeJson(json);
    return reqId;
}

static void TestServer(void) {
    if (IsGroupExistByGroupId(0, g_groupId) && g_gmInstance->isDeviceInGroup(0, TEST_APP_NAME, g_groupId, "client")) {
        printf("group exists and client already exists in group.\n");
        // 如果群组存在并且已经和client端绑定过了，则直接进行认证
        g_isAuth = true;
    }
    char buf[2048] = {0};

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd == -1) {
        printf("socket failed\n");
        return;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(PORT);

    printf("start to bind socket!\n");
    int res = bind(sockfd, (struct sockaddr *)&addr, sizeof(addr));
    if (res == -1) {
        printf("bind failed\n");
        return;
    }

    printf("start to listen socket!\n");
    res = listen(sockfd, 100);
    if(res == -1) {
        printf("listen failed\n");
        close(sockfd);
        return;
    }
    printf("listen %d port, wait for new connection...\n", PORT);

    struct sockaddr_in fromaddr;
    socklen_t len = sizeof(fromaddr);
    while(1) {
        if(clientfd < 0) {
            printf("wait new connection...\n");
            clientfd = accept(sockfd, (struct sockaddr *)&fromaddr, &len);
            if(clientfd == -1) {
                printf("accept failed\n");
                continue;
            }
            printf("new client in:%s\n", inet_ntoa(fromaddr.sin_addr));
            isProcessing = 1;
            while(clientfd > 0 && isProcessing) {
                (void)memset_s(buf, sizeof(buf), 0, sizeof(buf));
                int count = recv(clientfd, buf, sizeof(buf), 0);
                printf("read %d byte from client: %s\n", count, buf);
                if(count <= 0) {
                    close(clientfd);
                    clientfd = -1;
                    break;
                }
                if (strcmp("forceDelMember\n", buf) == 0) {
                    LocalForceDelMember();
                    sleep(1);
                    continue;
                }
                int64_t tmpReqId = GetReqIdFromData(buf);
                if(!g_isAuth) {
                    printf("fuzikun: bind!\n");
                    g_gmInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count));
                } else {
                    printf("fuzikun: auth!\n");
                    g_gaInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count), &gaCallback);
                }
            }
            sleep(2);
            if(stopTask == 1) {
                DestroyDeviceAuthService();
                stopTask = 0;
                break;
            }
            printf("test processing at ending...\n");
            sleep(1);
        } else {
            if(clientfd > 0) {
                close(clientfd);
                clientfd = -1;
            }
            sleep(2);
        }
    }

    if(clientfd > 0) {
        close(clientfd);
        clientfd = -1;
    }
    close(sockfd);
}

static void *ClientSocketFunc(void *args)
{
    (void)args;
    printf("----------------------start to open socket!\n");
    in_addr_t dstIp = inet_addr("192.168.43.40");
    clientfd = TestOpenSocket(dstIp, PORT);
    while(clientfd < 0) {
        sleep(5);
        clientfd = TestOpenSocket(dstIp, PORT);
    }
    // if (clientfd < 0) {
    //     clientfd = 0;
    //     return((void *)0);
    // }
    printf("----------------------open socket success!\n");
    bool isFirst = true;
    char buf[2048] = {0};
    while (1) {
        (void)memset_s(buf, sizeof(buf), 0, sizeof(buf));
        printf("----------------------start to recv data!\n");
        if (isFirst) {
            g_testCondition.notify(&g_testCondition); // 唤醒主线程
            isFirst = false;
        }
        printf("----------------------recv data!\n");
        int count = recv(clientfd, buf, sizeof(buf), 0);
        printf("----------------------recv data success!\n");
        printf("read %d bytes from peer: %s\n", count, buf);
        if (count <= 0) {
            close(clientfd);
            clientfd = -1;
            break;
        }
        if (strcmp("forceDelMember\n", buf) == 0) {
            LocalForceDelMember();
            sleep(1);
            continue;
        }
        int64_t tmpReqId = GetReqIdFromData(buf);
        if(!g_isAuth) {
            printf("----------------------fuzikun: bind!\n");
            g_gmInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count));
        } else {
            printf("----------------------fuzikun: auth!\n");
            g_gaInstance->processData(tmpReqId, (uint8_t *)buf, (uint32_t)(count), &gaCallback);
        }
        printf("----------------------fuzikun: OK!\n");
    }
    return((void *)0);
}

static int CreateGroup3861(void) {
    CJson *json = CreateJson();
    AddStringToJson(json, FIELD_GROUP_NAME, "P2PGroup");
    AddStringToJson(json, FIELD_DEVICE_ID, "client"); // 群组的deviceId字段
    AddIntToJson(json, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
    AddIntToJson(json, FIELD_GROUP_VISIBILITY, GROUP_VISIBILITY_PUBLIC);
    AddIntToJson(json, FIELD_USER_TYPE, DEVICE_TYPE_ACCESSORY);
    AddIntToJson(json, FIELD_EXPIRE_TIME, -1);
    char *createParamsStr = PackJsonToString(json);
    FreeJson(json);
    int res = g_gmInstance->createGroup(0, 123456, TEST_APP_NAME, createParamsStr);
    FreeJsonString(createParamsStr);
    return res;
}

static int TestAddMember(void) {
    CJson *addParams = CreateJson();
    printf("group id: %s\n", g_groupId);
    AddStringToJson(addParams, FIELD_GROUP_ID, g_groupId);
    AddIntToJson(addParams, FIELD_GROUP_TYPE, PEER_TO_PEER_GROUP);
    AddStringToJson(addParams, FIELD_PIN_CODE, TEST_PIN_CODE);
    AddBoolToJson(addParams, FIELD_IS_ADMIN, true);
    char *addParamsStr = PackJsonToString(addParams);
    FreeJson(addParams);
    int ret = -1;
    if (g_isConcurrent) {
        for (int i = 1; i <= CONCURRENT_NUM; i++) {
            char tmpAppId[256] = { 0 };
            sprintf(tmpAppId, "%s%d", TEST_APP_NAME, i);
            ret = g_gmInstance->addMemberToGroup(0, i, tmpAppId, addParamsStr);
        }
    } else {
        ret = g_gmInstance->addMemberToGroup(0, TEST_REQUEST_ID, TEST_APP_NAME, addParamsStr);
    }
    FreeJsonString(addParamsStr);
    // while(1) {}; // 一直循环可能会导致线程栈溢出
    g_testCondition.wait(&g_testCondition);
    return ret;
}

void TestClient(void)
{
    int ret = -1;
    if (!IsGroupExistByGroupId(0, g_groupId)) {
        ret = CreateGroup3861();
        if (ret != 0) {
            printf("Create group failed\n");
            return;
        }
        // 等待群组创建完成，群组创建成功之后在onFinish中会调用notify唤醒
        g_testCondition.wait(&g_testCondition);
    } else if (g_gmInstance->isDeviceInGroup(0, TEST_APP_NAME, g_groupId, "server")) {
        printf("group exists and server already exists in group!\n");
        // 如果已经和server端绑定过，则直接进行认证
        g_isAuth = true;
    }
    
    pthread_t pid;
    pthread_attr_t threadAttr;
    pthread_attr_init(&threadAttr);
    pthread_attr_setstacksize(&threadAttr, 40960);
    (void)pthread_create(&pid, &threadAttr, ClientSocketFunc, NULL);

    // 和server端的socket通道连接成功之后会调用notify唤醒
    g_testCondition.wait(&g_testCondition);

    if (!g_isAuth) {
        ret = TestAddMember();
    } else {
        ret = TestAuthDevice(true);
    }
}

static int InitEnv(void)
{
    printf("start to init env.\n");
    InitDeviceAuthService();
    g_gmInstance = GetGmInstance();
    g_gaInstance = GetGaInstance();
    if (g_gmInstance == NULL || g_gaInstance == NULL) {
        DestroyDeviceAuthService();
        printf("init module instance failed\n");
        return -1;
    }
    g_testListener.onGroupCreated = Test_OnGroupCreated;
    g_testListener.onGroupDeleted = Test_OnGroupDeleted;
    g_testListener.onDeviceBound = Test_OnDeviceBound;
    g_testListener.onDeviceUnBound = Test_OnDeviceUnBound;
    g_testListener.onDeviceNotTrusted = Test_OnDeviceNotTrusted;
    g_testListener.onLastGroupDeleted = Test_OnLastGroupDeleted;
    g_testListener.onTrustedDeviceNumChanged = Test_OnTrustedDeviceNumChanged;
    g_gmInstance->regDataChangeListener(TEST_APP_NAME, &g_testListener);

    gmCallback.onError = OnError;
    gmCallback.onFinish = OnFinish;
    gmCallback.onSessionKeyReturned = OnSessionKeyReturned;
    gmCallback.onTransmit = OnTransmitNet;
    gmCallback.onRequest = OnBindRequestController;
    g_gmInstance->regCallback(TEST_APP_NAME, &gmCallback);
    if (g_isConcurrent && !g_isAuth) {
        for (int i = 1; i <= CONCURRENT_NUM; i++) {
            char tmpAppId[256] = { 0 };
            sprintf(tmpAppId, "%s%d", TEST_APP_NAME, i);
            g_gmInstance->regCallback(tmpAppId, &gmCallback);;
            printf("test regCallback count:%d \n", i);
        }
    }

    gaCallback.onError = OnError;
    gaCallback.onFinish = OnFinish;
    gaCallback.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallback.onTransmit = OnTransmitNet;
    gaCallback.onRequest = OnAuthRequestController;
    g_algLoader = GetLoaderInstance();
    if (g_algLoader == NULL) {
        printf("Get loader failed.");
        return -1;
    }
    int32_t res = g_algLoader->initAlg();
    if (res != 0) {
        printf("initAlg failed.");
        return -1;
    }
    if (g_mutex == NULL) {
        g_mutex = (HcMutex *)HcMalloc(sizeof(HcMutex), 0);
        if (g_mutex == NULL) {
            LOGE("Alloc test mutex failed.");
            return -1;
        }
        if (InitHcMutex(g_mutex) != HC_SUCCESS) {
            LOGE("Init test mutex failed.");
            HcFree(g_mutex);
            g_mutex = NULL;
            return -1;
        }
    }
    printf("init env success.\n");
    return 0;
}

static void DeviceAuthTest(void)
{
    if (g_isClient) {
        TestClient();
    } else {
        TestServer();
    }
}

void TestBindAuthDoubleDevice(void)
{
    InitHcCond(&g_testCondition, NULL);
    if (InitEnv() != 0) {
        printf("init fail.\n");
        return;
    }
    DeviceAuthTest();
    DestroyDeviceAuthService();
    if (g_mutex != NULL) {
        DestroyHcMutex(g_mutex);
        HcFree(g_mutex);
        g_mutex = NULL;
    }
    DestroyHcCond(&g_testCondition);
}