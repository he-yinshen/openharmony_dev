#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include "device_auth.h"
#include "securec.h"
#include <pthread.h>
#include "ohos_init.h"
#include "utils_file.h"
#include "hichain_test_single_device.h"
#include "string_util.h"
#include "hc_dev_info.h"
#include "hc_types.h"
// #include "hmacsha256.h"
#include "hichain_test_double_device.h"

#define PORT 10001
#define LOGI printf
#define LOGE printf
#define LOGW printf
#define TEST_APP_NAME "com.huawei.security"
#define TEST_APP_ID "TestAppId"

static int clientfd = -1;
static int listenfd = -1;
int g_finishBind = 0;
DataChangeListener listenerCtx = {0};
const DeviceGroupManager *g_gmInst = 0;
static char requestfamot[] = \
"{\"confirmation\":%lld,\"pinCode\":\"000000\",\"deviceId\":\"\
5420459D93FE773F9945FD64277FBA2CAB8FB996DDC1D0B97676FBB1242B3930\"}";

void printbuf(const char *buf, int len)
{
#define OUTBUF 250
#define PRINTBUF 200
#define PRINTMAXCNT 10
    char out[OUTBUF] = {0};
	int outLen = 0;
	int cnt = PRINTMAXCNT;
	printf("-----begin print len %d\r\n", len);
	while (cnt) {
		if (outLen < len) {
			memcpy(out, buf + outLen, PRINTBUF);
			printf("%s\r\n", out);
			outLen += PRINTBUF;
		}
		cnt--;
	}
	printf("-----end print len %d\r\n", len);
}

static bool OnTransmitNet(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    int32_t cnt;
    LOGI("-------------OnTransmit---------------\r\n");
    LOGI("|  RequestId: %-30lld  |\r\n", requestId);
	printbuf((const char *)data, dataLen + 1);
	LOGI("-------------OnTransmit---------------\r\n");
    cnt = send(clientfd, data, dataLen, 0);
    LOGI("send size %d, process size %d\r\n", dataLen, cnt);
    return true;
}

static void OnSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
	(void)sessionKey;
    LOGI("-------------OnSessionKeyReturned---------------\r\n");
    LOGI("|  RequestId: %-40lld  |\r\n", requestId);
    LOGI("|  SessionKeyLen: %-36d  |\r\n", sessionKeyLen);
    LOGI("-------------OnSessionKeyReturned---------------\r\n");
}

static void OnFinish(int64_t requestId, int operationCode, const char *authReturn)
{
    LOGI("-------------OnFinish---------------\r\n");
    LOGI("|  RequestId: %-27lld  |\r\n", requestId);
    LOGI("|  OperationCode: %-23d  |\r\n", operationCode);
    LOGI("|  ReturnData: %-26s  |\r\n", authReturn);
	LOGI("-------------OnFinish---------------\r\n");
    if (operationCode == 1 || operationCode == 0) {
        return;
    }
	close(clientfd);
	close(listenfd);
	g_finishBind = 1;
}

static void OnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
	(void)errorReturn;
    LOGI("-------------OnError---------------\r\n");
    LOGI("|  RequestId: %-27lld  |\r\n", requestId);
    LOGI("|  OperationCode: %-23d  |\r\n", operationCode);
    LOGI("|  ErrorCode: %-27d  |\r\n", errorCode);
    LOGI("-------------OnError---------------\r\n");
}

static char *OnBindRequestController(int64_t requestId, int operationCode, const char* reqParam)
{
#define REQUESTBUF 300
#define CONFIRMVAL 0x80000006
    LOGI("-------------OnRequest---------------\r\n");
    LOGI("|  RequestId: %-29lld  |\r\n", requestId);
    LOGI("|  OperationCode: %-25d  |\r\n", operationCode);
    LOGI("|  reqParam: %s  |\r\n", (char *)reqParam);
    LOGI("-------------OnRequest---------------\r\n");
    char *buf = calloc(1, REQUESTBUF);
    int64_t val = CONFIRMVAL;
    sprintf(buf, requestfamot, val);
    printf("buf = %s\r\n", buf);
    return buf;
}

const DeviceGroupManager *registerCallbackController(DeviceAuthCallback *callback)
{
    
    callback->onRequest = OnBindRequestController;
    callback->onError = OnError;
    callback->onFinish = OnFinish;
    callback->onSessionKeyReturned = OnSessionKeyReturned;
    callback->onTransmit = OnTransmitNet;
    const DeviceGroupManager *gm = GetGmInstance();
    gm->regCallback(TEST_APP_NAME, callback);
    return gm;
}

void onGroupCreated(const char *groupInfo)
{
    LOGI("-------------onGroupCreated---------------\r\n");
	if (groupInfo == NULL) {
		LOGW("not expected");
		return;
	}
	LOGI("groupInfo: %s", groupInfo);
	LOGI("-------------onGroupCreated---------------\r\n");
}

void onGroupDeleted(const char *groupInfo)
{
    LOGI("-------------onGroupDeleted---------------\r\n");
	if (groupInfo == NULL) {
		LOGW("not expected");
		return;
	}
	LOGI("groupInfo: %s", groupInfo);
	LOGI("-------------onGroupDeleted---------------\r\n");
}
void onDeviceBound(const char *peerUdid, const char* groupinfo)
{
    (void)peerUdid;
    (void)groupinfo;
    LOGI("-------------onDeviceBound---------------");
}
void onDeviceUnBound(const char *peerUdid, const char* groupinfo)
{
    (void)peerUdid;
    (void)groupinfo;
    LOGI("-------------onDeviceUnBound---------------");
}
void onDeviceNotTrusted(const char *peerUdid)
{
    (void)peerUdid;
    LOGI("-------------onDeviceNotTrusted---------------");
}
void onLastGroupDeleted(const char *peerUdid, int groupType)
{
    (void)peerUdid;
    (void)groupType;
    LOGI("-------------onLastGroupDeleted---------------");
}
void onTrustedDeviceNumChanged(int curTrustedDeviceNum)
{
    (void)curTrustedDeviceNum;
    LOGI("-------------onTrustedDeviceNumChanged---------------");
}

int BindLocalSocket(int port)
{
    int rc;
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        LOGI("fd=%d\r\n", fd);
        return -1;
    }
    struct sockaddr_in addr = {0};
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    rc = bind(fd, (struct sockaddr *)&addr, sizeof(addr));
    if (rc < 0) {
        LOGI("bind error fd=%d,rc=%d", fd, rc);
        return -1;
    }
    return fd;
}

void TestController3861InServer(void)
{
#define RECVBUF 2048
#define REQUESTID 1234567890000000
    int ret;
    char buf[RECVBUF] = {0};
    int count;
    listenfd = BindLocalSocket(PORT);
    if (listenfd < 0) {
        LOGI("listen %d port error %d\r\n", PORT, listenfd);
        return;
    }
    ret = listen(listenfd, 1);
    if (ret == -1) {
        LOGI("listen failed");
        close(listenfd);
        return;
    }
    LOGI("listen port %d, wait for new connection... ret %d\r\n", PORT, listenfd);
    struct sockaddr_in fromaddr;
    socklen_t len = sizeof(fromaddr);
    LOGI("wait new connection...\r\n");
    while(1) {
        if (g_finishBind) {
            LOGI("finish quit...\r\n");
            break;
        }
        if (clientfd < 0) {
            clientfd = accept(listenfd, (struct sockaddr *)&fromaddr, &len);
            if (clientfd == -1) {
                LOGE("accept failed\r\n");
                sleep(1);
                continue;
            }
        }
        memset(buf, 0, sizeof(buf));
        count = recv(clientfd, buf, sizeof(buf), 0);
        if (count < 0) {
            LOGI("recv failed");
            return;
        }
        if (count > 0) {
            LOGI("recv data len %d\r\n", count);
            printbuf((const char *)buf, count);
            g_gmInst->processData(REQUESTID, (uint8_t *)buf, (uint32_t)count);
        }
        sleep(1);
    }
}

// static void TestMbedtls(void)
// {
//     char keyHexStr[] = "91B4D142823F7D20C5F08DF69122DE43F35F057A988D9619F6D3138485C9A203";
//     char messageStr[] = "8CDE46396EC2E3DB694FEBFE217F64F3EBEE620620C0F099575F710D3CFC4095";
//     uint8_t *macVal = HcMalloc(32, 0);
//     Uint8Buff outMac = { macVal, 32 };
//     uint8_t *keyVal = HcMalloc(32, 0);
//     HexStringToByte(keyHexStr, keyVal, 32);
//     Uint8Buff key = { keyVal, 32 };
//     uint8_t *messageVal = HcMalloc(32, 0);
//     HexStringToByte(messageStr, messageVal, 32);
//     Uint8Buff message = { messageVal, 32 };
//     ComputeHmac(&key, &message, &outMac);
//     HcFree(macVal);
//     HcFree(keyVal);
//     HcFree(messageVal);
// }

void TestBindWithPhone(void)
{
    InitDeviceAuthService();
    DeviceAuthCallback callback;
    g_gmInst = registerCallbackController(&callback);
    if (g_gmInst == NULL) {
        LOGI("init module instance failed\r\n");
        return;
    }
    listenerCtx.onGroupCreated = onGroupCreated;
    listenerCtx.onGroupDeleted = onGroupDeleted;
    g_gmInst->regDataChangeListener(TEST_APP_NAME, &listenerCtx);
    // TestMbedtls();
    TestController3861InServer();
}

// extern void InitSoftBusServer(void);

void TestGroupCapabilities(void)
{
    InitDeviceAuthService();
    if (!TestRegCallback()) {
        DestroyDeviceAuthService();
        return;
    }
    TestRegDataChangeListener();
    if (!TestCreateGroup()) {
        DestroyDeviceAuthService();
        return;
    }
    TestCheckAccessToGroup();
    TestGetPkInfoList();
    TestGetGroupInfoById();
    TestGetGroupInfo();
    TestGetJoinedGroups();
    TestGetRelatedGroups();
    TestGetDeviceInfoById();
    TestGetTrustedDevices();
    TestIsDeviceInGroup();
    TestBindAndCancel();
    TestAuthAndCancel();
    TestDeleteGroup();
    TestUnregDataChangeListener();
    TestUnregCallback();
    DestroyDeviceAuthService();
}

void TestBindAuthSingleDevice(void)
{
    InitDeviceAuthService();
    if (!TestRegCallback()) {
        DestroyDeviceAuthService();
        return;
    }
    if (!TestCreateGroup()) {
        DestroyDeviceAuthService();
        return;
    }
    TestAddMember();
    TestAuthDevice();
    TestDeleteMember();
    TestDeleteGroup();
    TestUnregCallback();
    DestroyDeviceAuthService();
}

void TestGetUdid(void)
{
    char *localUdid = (char *)HcMalloc(INPUT_UDID_LEN, 0);
    if (localUdid == NULL) {
        LOGE("Failed to malloc for local udid!");
        return;
    }
    int32_t res = HcGetUdid((uint8_t *)localUdid, INPUT_UDID_LEN);
    if (res == 0) {
        LOGI("udid is: %s\n", localUdid);
    }
    HcFree(localUdid);
}

void entry(void *arg)
{
    (void)arg;
    sleep(10);

    // 如果需要在系统启动时清除群组数据，则将下面的注释代码打开
    // UtilsFileDelete("/hcgroup.dat");
    // UtilsFileDelete("/data/hcgroup.dat");
    // UtilsFileDelete("/data/data/deviceauth/hcgroup.dat");

    LOGI("[test for 3861][begin]");
    
    // TestGetUdid(); // 获取本设备udid测试

    // TestBindWithPhone(); // 和手机进行绑定功能测试，启动绑定服务端流程
    // InitSoftBusServer(); // 和手机绑定成功后，初始化软总线，用于发起认证：当前L0 Hi3861公板上软总线功能不可用

    // TestBindAuthDoubleDevice(); // 双端测试绑定和认证功能

    TestBindAuthSingleDevice(); // 单端测试绑定和认证功能

    // TestGroupCapabilities(); // 单端测试群组相关功能

    LOGI("[test for 3861][end]");
}

// extern void hi_wifi_set_macaddr(char *mac, int len);

void testthread3861(void (*entry)(void *arg))
{
#define MAINLOOP_STACK_SIZE 32768
    pthread_t tid;
    pthread_attr_t threadAttr;
    pthread_attr_init(&threadAttr);
    pthread_attr_setstacksize(&threadAttr, MAINLOOP_STACK_SIZE);
    if (pthread_create(&tid, &threadAttr, (void *)entry, 0) != 0) {
        LOGE("create DeathProcTask failed!");
        return;
    }
    LOGI("[testthread3861] loop thread creating");
}

void main3861(void)
{
    // char mac[6] = {0x8c, 0x22, 0x33, 0x44, 0x55, 0x33};
    // hi_wifi_set_macaddr(mac, sizeof(mac));
    testthread3861(entry);
}

SYS_SERVICE_INIT_PRI(main3861, 4); // 初始化系统服务，系统启动时会调用main3861方法