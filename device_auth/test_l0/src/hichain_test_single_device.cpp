#include "hichain_test_single_device.h"
#include <cinttypes>
#include <unistd.h>
#include "alg_loader.h"
#include "common_defs.h"
#include "device_auth.h"
#include "device_auth_defines.h"
#include "hc_dev_info.h"
#include "hc_log.h"
#include "json_utils.h"
#include "securec.h"
#include "group_operation_common.h"

#define TEST_REQ_ID 123
#define TEST_REQ_ID2 321
#define TEST_DEV_AUTH_SLEEP_TIME 100000
#define TEST_AUTH_ID "TestAuthId"
#define TEST_APP_ID "TestAppId"
#define TEST_APP_ID1 "TestAppId1"
#define TEST_PIN_CODE "123456"
#define TEST_AUTH_ID2 "TestAuthId2"
#define TEST_GROUP_ID "E2EE6F830B176B2C96A9F99BFAE2A61F5D1490B9F4A090E9D8C2874C230C7C21"
#define TEST_UDID_CLIENT "5420459D93FE773F9945FD64277FBA2CAB8FB996DDC1D0B97676FBB1242B3930"
#define TEST_UDID_SERVER "52E2706717D5C39D736E134CC1E3BE1BAA2AA52DB7C76A37C749558BD2E6492C"

enum AsyncStatus {
    ASYNC_STATUS_WAITING = 0,
    ASYNC_STATUS_TRANSMIT = 1,
    ASYNC_STATUS_FINISH = 2,
    ASYNC_STATUS_ERROR = 3
};

static AsyncStatus volatile g_asyncStatus;
static uint32_t g_transmitDataMaxLen = 2048;
static uint8_t g_transmitData[2048] = { 0 };
static uint32_t g_transmitDataLen = 0;

static const int32_t TEST_AUTH_OS_ACCOUNT_ID = 100;
static const char *g_createParams = "{\"groupName\":\"TestGroup\",\"deviceId\":\"TestAuthId\",\"groupType\":256,\"group"
    "Visibility\":-1,\"userType\":0,\"expireTime\":-1}";
static const char *g_addParams =
    "{\"groupId\":\"E2EE6F830B176B2C96A9F99BFAE2A61F5D1490B9F4A090E9D8C2874C230C7C21\","
    "\"groupType\":256,\"pinCode\":\"123456\"}";
static const char *g_deleteParams =
    "{\"groupId\":\"E2EE6F830B176B2C96A9F99BFAE2A61F5D1490B9F4A090E9D8C2874C230C7C21\",\"deleteId\":\"TestAuthId2\"}";
static const char *g_disbandParams =
    "{\"groupId\":\"E2EE6F830B176B2C96A9F99BFAE2A61F5D1490B9F4A090E9D8C2874C230C7C21\"}";

static bool OnTransmit(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    (void)requestId;
    if (memcpy_s(g_transmitData, g_transmitDataMaxLen, data, dataLen) != EOK) {
        return false;
    }
    g_transmitDataLen = dataLen;
    g_asyncStatus = ASYNC_STATUS_TRANSMIT;
    return true;
}

static void PrintHexStr(const uint8_t *buf, int32_t buf_len)
{
    int32_t tmp_data_hex_len = buf_len * BYTE_TO_HEX_OPER_LENGTH + 1;
    char *tmp_data_hex = (char *)HcMalloc(tmp_data_hex_len, 0);
    if (tmp_data_hex == NULL) {
        LOGE("Malloc failed");
        return;
    }
    ByteToHexString(buf, buf_len, tmp_data_hex, buf_len * BYTE_TO_HEX_OPER_LENGTH + 1);
    LOGI("PrintBytes: %s", tmp_data_hex);
    HcFree(tmp_data_hex);
}

static void OnSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    (void)requestId;
    (void)sessionKey;
    (void)sessionKeyLen;
    LOGI("start to print returned session key");
    PrintHexStr(sessionKey, sessionKeyLen);
    return;
}

static void OnFinish(int64_t requestId, int operationCode, const char *authReturn)
{
    (void)requestId;
    LOGI("operation code: %d, return value is: %s", operationCode, authReturn);
    g_asyncStatus = ASYNC_STATUS_FINISH;
}

static void OnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    (void)requestId;
    LOGE("operation code: %d, error code: %d, error value is: %s", operationCode, errorCode, errorReturn);
    g_asyncStatus = ASYNC_STATUS_ERROR;
}

static char *OnBindRequest(int64_t requestId, int operationCode, const char* reqParam)
{
    (void)requestId;
    (void)operationCode;
    (void)reqParam;
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, TEST_AUTH_OS_ACCOUNT_ID);
    AddStringToJson(json, FIELD_PIN_CODE, TEST_PIN_CODE);
    AddStringToJson(json, FIELD_DEVICE_ID, TEST_AUTH_ID2);
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    return returnDataStr;
}

static char *OnAuthRequest(int64_t requestId, int operationCode, const char* reqParam)
{
    (void)requestId;
    (void)operationCode;
    (void)reqParam;
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, TEST_AUTH_OS_ACCOUNT_ID);
    AddStringToJson(json, FIELD_PEER_CONN_DEVICE_ID, TEST_UDID_CLIENT);
    AddStringToJson(json, FIELD_SERVICE_PKG_NAME, TEST_APP_ID);
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    return returnDataStr;
}

static void OnGroupCreated(const char *groupInfo)
{
    LOGI("groupInfo: %s", groupInfo);
}

static void OnGroupDeleted(const char *groupInfo)
{
    LOGI("groupInfo: %s", groupInfo);
}
    
static void OnDeviceBound(const char *peerUdid, const char *groupInfo)
{
    LOGI("peerUdid: %s, groupInfo: %s", peerUdid, groupInfo);
}
    
static void OnDeviceUnBound(const char *peerUdid, const char *groupInfo)
{
    LOGI("peerUdid: %s, groupInfo: %s", peerUdid, groupInfo);
}

static void OnDeviceNotTrusted(const char *peerUdid)
{
    LOGI("peerUdid: %s", peerUdid);
}

static void OnLastGroupDeleted(const char *peerUdid, int groupType)
{
    LOGI("peerUdid: %s, groupType: %d", peerUdid, groupType);
}

static void OnTrustedDeviceNumChanged(int curTrustedDeviceNum)
{
    LOGI("curTrustedDeviceNum: %d", curTrustedDeviceNum);
}

static DeviceAuthCallback g_gmCallback = {
    .onTransmit = OnTransmit,
    .onSessionKeyReturned = OnSessionKeyReturned,
    .onFinish = OnFinish,
    .onError = OnError,
    .onRequest = OnBindRequest
};

static DeviceAuthCallback g_gaCallback = {
    .onTransmit = OnTransmit,
    .onSessionKeyReturned = OnSessionKeyReturned,
    .onFinish = OnFinish,
    .onError = OnError,
    .onRequest = OnAuthRequest
};

static DataChangeListener g_dataChangeListener = {
    .onGroupCreated = OnGroupCreated,
    .onGroupDeleted = OnGroupDeleted,
    .onDeviceBound = OnDeviceBound,
    .onDeviceUnBound = OnDeviceUnBound,
    .onDeviceNotTrusted = OnDeviceNotTrusted,
    .onLastGroupDeleted = OnLastGroupDeleted,
    .onTrustedDeviceNumChanged = OnTrustedDeviceNumChanged
};

void TestAuthDevice(void)
{
    LOGI("start to authDevice");
    g_asyncStatus = ASYNC_STATUS_WAITING;
    bool isClient = true;
    SetDeviceStatus(isClient);
    const GroupAuthManager *ga = GetGaInstance();
    if (ga == NULL) {
        LOGE("ga instance is null!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    CJson *authParam = CreateJson();
    AddStringToJson(authParam, FIELD_PEER_CONN_DEVICE_ID, TEST_UDID_SERVER);
    AddStringToJson(authParam, FIELD_SERVICE_PKG_NAME, TEST_APP_ID);
    AddBoolToJson(authParam, FIELD_IS_CLIENT, isClient);
    char *authParamStr = PackJsonToString(authParam);
    printf("jsonStr: %s\n", authParamStr);
    FreeJson(authParam);
    int32_t ret = ga->authDevice(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, authParamStr, &g_gaCallback);
    FreeJsonString(authParamStr);
    if (ret != HC_SUCCESS) {
        LOGE("call authDevice failed!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    while (g_asyncStatus == ASYNC_STATUS_WAITING) {
        usleep(TEST_DEV_AUTH_SLEEP_TIME);
    }
    while (g_asyncStatus == ASYNC_STATUS_TRANSMIT) {
        isClient = !isClient;
        SetDeviceStatus(isClient);
        g_asyncStatus = ASYNC_STATUS_WAITING;
        if (isClient) {
            ret = ga->processData(TEST_REQ_ID, g_transmitData, g_transmitDataLen, &g_gaCallback);
        } else {
            ret = ga->processData(TEST_REQ_ID2, g_transmitData, g_transmitDataLen, &g_gaCallback);
        }
        (void)memset_s(g_transmitData, g_transmitDataMaxLen, 0, g_transmitDataMaxLen);
        g_transmitDataLen = 0;
        if (ret != HC_SUCCESS) {
            LOGE("call processData failed!");
            g_asyncStatus = ASYNC_STATUS_ERROR;
            break;
        }
        while (g_asyncStatus == ASYNC_STATUS_WAITING) {
            usleep(TEST_DEV_AUTH_SLEEP_TIME);
        }
        if (g_asyncStatus == ASYNC_STATUS_ERROR) {
            break;
        }
        if (g_transmitDataLen > 0) {
            g_asyncStatus = ASYNC_STATUS_TRANSMIT;
        }
    }
    // SetDeviceStatus(true);
    sleep(1);
    LOGI("end to authDevice");
}

void TestAddMember(void)
{
    LOGI("start to add member");
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    bool isDeviceInGroup = gm->isDeviceInGroup(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_GROUP_ID, TEST_AUTH_ID2);
    if (isDeviceInGroup) {
        LOGI("device already exists in group.");
        return;
    }
    g_asyncStatus = ASYNC_STATUS_WAITING;
    bool isClient = true;
    SetDeviceStatus(isClient);
    int32_t ret = gm->addMemberToGroup(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, TEST_APP_ID, g_addParams);
    if (ret != HC_SUCCESS) {
        LOGE("call addMemberToGroup failed!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    while (g_asyncStatus == ASYNC_STATUS_WAITING) {
        usleep(TEST_DEV_AUTH_SLEEP_TIME);
    }
    while (g_asyncStatus == ASYNC_STATUS_TRANSMIT) {
        isClient = !isClient;
        SetDeviceStatus(isClient);
        g_asyncStatus = ASYNC_STATUS_WAITING;
        if (isClient) {
            ret = gm->processData(TEST_REQ_ID, g_transmitData, g_transmitDataLen);
        } else {
            ret = gm->processData(TEST_REQ_ID2, g_transmitData, g_transmitDataLen);
        }
        (void)memset_s(g_transmitData, g_transmitDataMaxLen, 0, g_transmitDataMaxLen);
        g_transmitDataLen = 0;
        if (ret != HC_SUCCESS) {
            LOGE("call processData failed!");
            g_asyncStatus = ASYNC_STATUS_ERROR;
            break;
        }
        while (g_asyncStatus == ASYNC_STATUS_WAITING) {
            usleep(TEST_DEV_AUTH_SLEEP_TIME);
        }
        if (g_asyncStatus == ASYNC_STATUS_ERROR) {
            break;
        }
        if (g_transmitDataLen > 0) {
            g_asyncStatus = ASYNC_STATUS_TRANSMIT;
        }
    }
    // client端绑定的最后一步会有两个操作，一个是onTransmit，一个是onFinish，onTransmit的时候，调用到了279行的processData，server端开始异步处理数据，
    // 但是数据还没处理完的时候，client端又收到了onFinish的回调，将g_asyncStatus置为ASYNC_STATUS_FINISH，导致289行不会再等，跳出循环，
    // 走到302行之后udid被设置为client端了，server端添加设备时使用的就是client端的udid了，导致server端没有将自己加入群组中，
    // 将302行注释掉，这样在server端添加设备的时候使用的就是server端的udid，不会导致问题。
    // SetDeviceStatus(true);
    sleep(1);
    LOGI("end to add member");
}

void TestDeleteMember(void)
{
    LOGI("start to delete member");
    g_asyncStatus = ASYNC_STATUS_WAITING;
    bool isClient = true;
    SetDeviceStatus(isClient);
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    int32_t ret = gm->deleteMemberFromGroup(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, TEST_APP_ID, g_deleteParams);
    if (ret != HC_SUCCESS) {
        LOGE("call deleteMemberFromGroup failed!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    while (g_asyncStatus == ASYNC_STATUS_WAITING) {
        usleep(TEST_DEV_AUTH_SLEEP_TIME);
    }
    while (g_asyncStatus == ASYNC_STATUS_TRANSMIT) {
        isClient = !isClient;
        SetDeviceStatus(isClient);
        g_asyncStatus = ASYNC_STATUS_WAITING;
        if (isClient) {
            ret = gm->processData(TEST_REQ_ID, g_transmitData, g_transmitDataLen);
        } else {
            ret = gm->processData(TEST_REQ_ID2, g_transmitData, g_transmitDataLen);
        }
        (void)memset_s(g_transmitData, g_transmitDataMaxLen, 0, g_transmitDataMaxLen);
        g_transmitDataLen = 0;
        if (ret != HC_SUCCESS) {
            LOGE("call processData failed!");
            g_asyncStatus = ASYNC_STATUS_ERROR;
            break;
        }
        while (g_asyncStatus == ASYNC_STATUS_WAITING) {
            usleep(TEST_DEV_AUTH_SLEEP_TIME);
        }
        if (g_asyncStatus == ASYNC_STATUS_ERROR) {
            break;
        }
        if (g_transmitDataLen > 0) {
            g_asyncStatus = ASYNC_STATUS_TRANSMIT;
        }
    }
    // SetDeviceStatus(true);
    sleep(1);
    LOGI("end to delete member");
}

void TestBindAndCancel(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    int32_t ret = gm->addMemberToGroup(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, TEST_APP_ID, g_addParams);
    if (ret != HC_SUCCESS) {
        LOGE("call addMemberToGroup failed!");
        return;
    }
    sleep(1);
    const GroupAuthManager *ga = GetGaInstance();
    if (ga == NULL) {
        LOGE("ga instance is null!");
        return;
    }
    ga->cancelRequest(TEST_REQ_ID, TEST_APP_ID);
    sleep(1);
    gm->cancelRequest(TEST_REQ_ID2, TEST_APP_ID);
    sleep(1);
    gm->cancelRequest(TEST_REQ_ID, TEST_APP_ID1);
    sleep(1);
    gm->cancelRequest(TEST_REQ_ID, TEST_APP_ID);
    sleep(1);
}

void TestAuthAndCancel(void)
{
    bool isClient = true;
    const GroupAuthManager *ga = GetGaInstance();
    if (ga == NULL) {
        LOGE("ga instance is null!");
        return;
    }
    CJson *authParam = CreateJson();
    AddStringToJson(authParam, FIELD_PEER_CONN_DEVICE_ID, TEST_UDID_SERVER);
    AddStringToJson(authParam, FIELD_SERVICE_PKG_NAME, TEST_APP_ID);
    AddBoolToJson(authParam, FIELD_IS_CLIENT, isClient);
    char *authParamStr = PackJsonToString(authParam);
    printf("jsonStr: %s\n", authParamStr);
    FreeJson(authParam);
    int32_t ret = ga->authDevice(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, authParamStr, &g_gaCallback);
    FreeJsonString(authParamStr);
    if (ret != HC_SUCCESS) {
        LOGE("call authDevice failed!");
        return;
    }
    sleep(1);
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    gm->cancelRequest(TEST_REQ_ID, TEST_APP_ID);
    sleep(1);
    ga->cancelRequest(TEST_REQ_ID2, TEST_APP_ID);
    sleep(1);
    ga->cancelRequest(TEST_REQ_ID, TEST_APP_ID1);
    sleep(1);
    ga->cancelRequest(TEST_REQ_ID, TEST_APP_ID);
    sleep(1);
}

bool TestCreateGroup(void)
{
    bool isGroup0Exist = false;
    if (IsGroupExistByGroupId(DEFAULT_OS_ACCOUNT, TEST_GROUP_ID)) {
        LOGI("group of account 0 already exists.");
        isGroup0Exist = true;
    }
    bool isGroup100Exist = false;
    if (IsGroupExistByGroupId(TEST_AUTH_OS_ACCOUNT_ID, TEST_GROUP_ID)) {
        LOGI("group of account 100 already exists.");
        isGroup100Exist = true;
    }
    if (isGroup0Exist && isGroup100Exist) {
        return true;
    }
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return false;
    }
    g_asyncStatus = ASYNC_STATUS_WAITING;
    int32_t ret = gm->createGroup(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, TEST_APP_ID, g_createParams);
    if (ret != HC_SUCCESS) {
        LOGE("call createGroup failed");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return false;
    }
    while (g_asyncStatus == ASYNC_STATUS_WAITING) {
        usleep(TEST_DEV_AUTH_SLEEP_TIME);
    }
    return true;
}

void TestDeleteGroup(void)
{
    g_asyncStatus = ASYNC_STATUS_WAITING;
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    int32_t ret = gm->deleteGroup(DEFAULT_OS_ACCOUNT, TEST_REQ_ID, TEST_APP_ID, g_disbandParams);
    if (ret != HC_SUCCESS) {
        LOGE("call deleteGroup failed!");
        g_asyncStatus = ASYNC_STATUS_ERROR;
        return;
    }
    while (g_asyncStatus == ASYNC_STATUS_WAITING) {
        usleep(TEST_DEV_AUTH_SLEEP_TIME);
    }
}

bool TestRegCallback(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return false;
    }
    int32_t ret = gm->regCallback(TEST_APP_ID, &g_gmCallback);
    if (ret != HC_SUCCESS) {
        LOGE("register callback failed!");
        return false;
    } else {
        LOGI("register callback success!");
        return true;
    }
}

void TestUnregCallback(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    int32_t ret = gm->unRegCallback(TEST_APP_ID);
    if (ret != HC_SUCCESS) {
        LOGE("unregister callback failed!");
    } else {
        LOGI("unregister callback success!");
    }
}

void TestRegDataChangeListener(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    int32_t ret = gm->regDataChangeListener(TEST_APP_ID, &g_dataChangeListener);
    if (ret != HC_SUCCESS) {
        LOGE("register data change listener failed!");
    } else {
        LOGI("register data change listener success!");
    }
}

void TestUnregDataChangeListener(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    int32_t ret = gm->unRegDataChangeListener(TEST_APP_ID);
    if (ret != HC_SUCCESS) {
        LOGE("unregister data change listener failed!");
    } else {
        LOGI("unregister data change listener success!");
    }
}

void TestCheckAccessToGroup(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    int32_t ret = gm->checkAccessToGroup(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_GROUP_ID);
    if (ret != HC_SUCCESS) {
        LOGE("check access to group failed!");
    } else {
        LOGI("check access to group success!");
    }
}

void TestGetPkInfoList(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    uint32_t returnNum = 0;
    char selfUdid[INPUT_UDID_LEN] = { 0 };
    (void)HcGetUdid((uint8_t *)selfUdid, INPUT_UDID_LEN);
    CJson *json = CreateJson();
    AddStringToJson(json, FIELD_UDID, selfUdid);
    AddBoolToJson(json, FIELD_IS_SELF_PK, true);
    char *jsonStr = PackJsonToString(json);
    FreeJson(json);
    printf("jsonStr: %s\n", jsonStr);
    int32_t ret = gm->getPkInfoList(DEFAULT_OS_ACCOUNT, TEST_APP_ID, jsonStr, &returnData, &returnNum);
    FreeJsonString(jsonStr);
    if (ret != HC_SUCCESS) {
        LOGE("get pk info list failed!");
        return;
    }
    LOGI("get pk info list success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("pk info data is null!");
        return;
    }
    if (returnNum == 0) {
        LOGE("pk info num is 0!");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestGetGroupInfoById(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    int32_t ret = gm->getGroupInfoById(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_GROUP_ID, &returnData);
    if (ret != HC_SUCCESS) {
        LOGE("get group info by id failed!");
        return;
    }
    LOGI("get group info by id success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("group info is null!");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestGetGroupInfo(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    uint32_t returnNum = 0;
    CJson *json = CreateJson();
    AddStringToJson(json, FIELD_GROUP_OWNER, TEST_APP_ID);
    char *jsonStr = PackJsonToString(json);
    FreeJson(json);
    printf("jsonStr: %s\n", jsonStr);
    int32_t ret = gm->getGroupInfo(DEFAULT_OS_ACCOUNT, TEST_APP_ID, jsonStr, &returnData, &returnNum);
    FreeJsonString(jsonStr);
    if (ret != HC_SUCCESS) {
        LOGE("get group info failed!");
        return;
    }
    LOGI("get group info success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("group info is null!");
        return;
    }
    if (returnNum == 0) {
        LOGE("group info num is 0");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestGetJoinedGroups(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    uint32_t returnNum = 0;
    int32_t ret = gm->getJoinedGroups(DEFAULT_OS_ACCOUNT, TEST_APP_ID, PEER_TO_PEER_GROUP, &returnData, &returnNum);
    if (ret != HC_SUCCESS) {
        LOGE("get joined groups failed!");
        return;
    }
    LOGI("get joined groups success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("joined groups is null!");
        return;
    }
    if (returnNum == 0) {
        LOGE("joined groups num is 0");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestGetRelatedGroups(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    uint32_t returnNum = 0;
    int32_t ret = gm->getRelatedGroups(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_AUTH_ID, &returnData, &returnNum);
    if (ret != HC_SUCCESS) {
        LOGE("get related groups failed!");
        return;
    }
    LOGI("get related groups success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("related groups is null!");
        return;
    }
    if (returnNum == 0) {
        LOGE("related groups num is 0");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestGetDeviceInfoById(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    int32_t ret = gm->getDeviceInfoById(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_AUTH_ID, TEST_GROUP_ID, &returnData);
    if (ret != HC_SUCCESS) {
        LOGE("get device info by id failed!");
        return;
    }
    LOGI("get device info by id success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("device info is null!");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestGetTrustedDevices(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    char *returnData = nullptr;
    uint32_t returnNum = 0;
    int32_t ret = gm->getTrustedDevices(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_GROUP_ID, &returnData, &returnNum);
    if (ret != HC_SUCCESS) {
        LOGE("get trusted devices failed!");
        return;
    }
    LOGI("get trusted devices success!");
    printf("returnData: %s\n", returnData);
    if (returnData == NULL) {
        LOGE("trusted devices is null!");
        return;
    }
    if (returnNum == 0) {
        LOGE("trusted devices num is 0");
        return;
    }
    gm->destroyInfo(&returnData);
}

void TestIsDeviceInGroup(void)
{
    const DeviceGroupManager *gm = GetGmInstance();
    if (gm == NULL) {
        LOGE("gm instance is null!");
        return;
    }
    bool ret = gm->isDeviceInGroup(DEFAULT_OS_ACCOUNT, TEST_APP_ID, TEST_GROUP_ID, TEST_AUTH_ID);
    if (!ret) {
        LOGE("device not in group!");
    } else {
        LOGI("device is in group!");
    }
}