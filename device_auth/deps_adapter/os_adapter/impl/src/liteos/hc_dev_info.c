/*
 * Copyright (C) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "hc_dev_info.h"
#include "securec.h"
#include "parameter.h"
#include "hal_error.h"
#include "hc_log.h"

#ifdef __cplusplus
extern "C" {
#endif

static bool g_isClient = true;
static char *g_clientDevUdid = "5420459D93FE773F9945FD64277FBA2CAB8FB996DDC1D0B97676FBB1242B3930";
static char *g_serverDevUdid = "52E2706717D5C39D736E134CC1E3BE1BAA2AA52DB7C76A37C749558BD2E6492C";

void SetDeviceStatus(bool isClient)
{
    g_isClient = isClient;
}

int32_t HcGetUdid(uint8_t *udid, int32_t udidLen)
{
    // if (udid == NULL || udidLen < INPUT_UDID_LEN || udidLen > MAX_INPUT_UDID_LEN) {
    //     return HAL_ERR_INVALID_PARAM;
    // }
    // int32_t ret = GetDevUdid((char *)udid, udidLen);
    // if (ret != 0) {
    //     LOGE("Failed to get dev udid, ret = %d", ret);
    //     return HAL_FAILED;
    // }
    // return HAL_SUCCESS;
    char *devUdid;
    if (g_isClient) {
        devUdid = g_clientDevUdid;
        LOGI("Use mock client device udid.");
    } else {
        devUdid = g_serverDevUdid;
        LOGI("Use mock server device udid.");
    }
    if (memcpy_s(udid, udidLen, devUdid, INPUT_UDID_LEN) != EOK) {
        LOGE("Failed to copy udid!");
        return HAL_FAILED;
    }
    return HAL_SUCCESS;
}

const char *GetStoragePath(void)
{
    return AUTH_STORAGE_PATH "/hcgroup.dat";
}

const char *GetStorageDirPath(void)
{
    return AUTH_STORAGE_PATH;
}

#ifdef __cplusplus
}
#endif
