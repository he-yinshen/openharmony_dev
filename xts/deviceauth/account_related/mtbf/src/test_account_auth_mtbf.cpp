/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "device_auth_func.h"
#include "device_auth_socket.h"
}


using namespace testing::ext;
using namespace std;
namespace {

int g_sockfd;
static int g_index = 0;
const char *g_localDeviceId;
const char *g_peerDeviceId;

const char *g_groupId;
const char *g_userId1 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4";
// const char *g_userId1 = "1111111111111111111111111111111111111111111111111111111111111111";
const char *g_userId2 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3";
// const char *g_userId2 = "2222222222222222222222222222222222222222222222222222222222222222";

static int32_t g_osAccountId1 = 100;
static int32_t g_osAccountId2 = 101;

int32_t g_requestId1 = 11;
int32_t g_requestId2 = 1233;
const char *g_appName = "TestApp";

static int g_accountType = 1;
static int g_croAccountType = 1282;
bool g_isClient = true;
bool g_isDeviceLevel = false;
const char *g_queryAccParams = "{\"groupType\":1,\"groupOwner\":\"TestApp\"}";
// const char *g_queryCroParams = "{\"groupType\":1282,\"groupOwner\":\"TestApp\"}";
const char *g_clientBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientBaseInfo1 = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E3\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientCredential;
// const char *g_clientBaseInfo = "{\"groupType\":1,,\"userId\":\"1111111111111111"
// "111111111111111111111111111111111111111111111111\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
// "\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";

const char *g_serverCredential;
const char *g_serverBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,"
"\"authCode\":\"1234567812345678123456781234567812345678123456781234567812345678\"}}";
// const char *g_serverBaseInfo1 = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
// "A67EDAD08E39F207900038F91FEF95DD042FE2874E3\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,"
// "\"authCode\":\"1234567812345678123456781234567812345678123456781234567812345678\"}}";

// const char *g_serverBaseInfo1 = "{\"groupType\":1,\"userId\":\"222222222222"
// "2222222222222222222222222222222222222222222222222222\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,"
// "\"authCode\":\"1234567812345678123456781234567812345678123456781234567812345678\"}}";
// 认证参数

// 点对点参数
static int g_groupType1 = 256;
const char *g_connectParams = nullptr;
const char *g_groupName1 = "P2PGroup";
static int g_visibilityPublic = -1;
const char *g_pinCode = "123456";

// 对称凭据
// 同账号
const char *g_symmInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A"
"68F1607EFCF7796C1491F834CD92\"}}";
const char *g_symmInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDA"
"D08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":"
"\"1234123412341234123412341234123412341234123412341234123412341233\"}}";
// 跨账号
// const char *g_symmCroInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3\","
// "\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A68F1607EFCF7796C1491F834CD92\"}}";
// const char *g_symmCroInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3\","
// "\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341233\"}}";
static int g_testNum = 100000;


class AccountSySy : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - GroupManage_P2P */
void AccountSySy::SetUpTestCase()
{
    SetAccessToken();
    const char *ip = "192.168.1.11";
    static int port = 15051;
    g_sockfd = InitClient(ip, port);
    g_localDeviceId = GetLocalDeviceId();
    g_peerDeviceId = GetPeerDevId(g_sockfd);
    g_clientCredential = CreateLocalCred(g_clientBaseInfo, g_localDeviceId);
    g_serverCredential = CreateLocalCred(g_serverBaseInfo, g_peerDeviceId);
}

void AccountSySy::TearDownTestCase()
{
}

void AccountSySy::SetUp()
{
    ToServerInit(g_sockfd);
    InitEnv();

}

void AccountSySy::TearDown()
{
    Destory();
    ToServerDestroy(g_sockfd);
}

/**
* @tc.name      对称凭据同账号认证压力测试
* @tc.number    Security_DevAuth_Account_Reliability_0301
* @tc.size      MEDIUM
* @tc.type      MTBF
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Reliability_0301, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Reliability_0301");
    // 环境配置
    CreateGroupTest(g_osAccountId1, g_requestId1, g_appName, g_clientCredential);
    // 通知对端创建对称凭据
    ToServerCreateGroup(g_sockfd, g_serverCredential);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, g_accountType);
    AddMultiMembersTest(g_osAccountId1, g_appName, createSymmInfo);
    // 认证压测
    for (int i = 0; i < g_testNum; i++)
    {
        const char *authParams = AuthParams(g_peerDeviceId, g_appName, g_isClient, g_isDeviceLevel, g_groupId, 0, nullptr);
        int *ret = TestAuth(g_osAccountId1, g_requestId2, authParams, g_sockfd);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
    }
    // 环境清理
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId1, g_appName, deletParamsStr);
    ToServerDeleteGroup(g_sockfd);
}

/**
* @tc.name      对称凭据跨账号认证压力测试
* @tc.number    Security_DevAuth_Account_Reliability_0302
* @tc.size      MEDIUM
* @tc.type      MTBF
* @tc.level     Level3
*/

HWTEST_F(AccountSySy, Security_DevAuth_Account_Reliability_0302, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Reliability_0302");
    // 环境配置
    CreateGroupTest(g_osAccountId1, g_requestId1, g_appName, g_clientCredential);
    // 通知对端创建对称凭据
    ToServerCreateGroup(g_sockfd, g_serverCredential);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    // 创建跨账号群组&导入凭据
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, g_croAccountType);
    AddMultiMembersTest(g_osAccountId1, g_appName, createSymmInfo);
    // 认证压测
    for (int i = 0; i < g_testNum; i++)
    {
        const char *authParams = AuthParams(g_peerDeviceId, g_appName, g_isClient, g_isDeviceLevel, groupId, 0, g_userId1);
        int *ret = TestAuth(g_osAccountId1, g_requestId1, authParams, g_sockfd);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
    }
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    groupId = GetGroupIdfromData(groupInfo, g_index);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId1, g_appName, deletParamsStr);
}

/**
* @tc.name      非对称凭据跨账号认证压力测试
* @tc.number    Security_DevAuth_Account_Reliability_0303
* @tc.size      MEDIUM
* @tc.type      MTBF
* @tc.level     Level3
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Reliability_0303, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Reliability_0303");
    // 配置非对称环境
    g_localDeviceId = GetLocalDeviceId();
    const char *reqJsonStr = CreateReqJson(g_version, g_localDeviceId, g_userId1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // 模拟云侧签名&创建群组
    const char *accoutGroup = CreateAccoutCG(g_requestId, g_osAccountId1, g_userId1, pkInfoStr);
    CreateGroupTest(g_osAccountId1, g_requestId, g_appName, accoutGroup);
    // 获取对端公钥,模拟云侧签名,通知对端创建非对称凭据
    const char *serverPk = ToGetServerPK(g_sockfd);
    const char *serverParams = CreateCroAccoutCG(g_requestId, g_osAccountId1, g_userId1, serverPk);
    ToServerCreateGroup(g_sockfd, serverParams);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);

    // 创建跨账号群组，导入凭据
    const char *croGroupId = CreatAsyCroAccount(g_sockfd, g_localDeviceId);
    const char *importInfo = GetCroPKInfo(croGroupId, g_croAccountType);
    AddMultiMembersTest(g_osAccountId1, g_appName, importInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(g_osAccountId1, g_appName, croGroupId);
    ASSERT_EQ(deviceNum == 2, true);

    // 认证压测
    for (int i = 0; i < g_testNum; i++)
    {
        const char *authParamsStr = AuthParams(g_peerDeviceId, g_appName, g_isClient, g_isDeviceLevel, croGroupId, 0, g_userId1);
        int *ret = TestAuth(g_osAccountId1, g_requestId, authParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
    }
    
    //  清理环境
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    ToServerDeleteGroup(g_sockfd);
}

/**
* @tc.name      未导入凭据进行非对称凭据同账号认证压力测试
* @tc.number    Security_DevAuth_Account_Reliability_0304
* @tc.size      MEDIUM
* @tc.type      MTBF
* @tc.level     Level3
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Reliability_0304, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Reliability_0304");
    // 配置非对称环境
    g_localDeviceId = GetLocalDeviceId();
    const char *reqJsonStr = CreateReqJson(g_version, g_localDeviceId, g_userId1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // 模拟云侧签名&创建群组
    const char *accoutGroup = CreateAccoutCG(g_requestId, g_osAccountId1, g_userId1, pkInfoStr);
    CreateGroupTest(g_osAccountId1, g_requestId, g_appName, accoutGroup);
    // 获取对端公钥,模拟云侧签名,通知对端创建非对称凭据
    const char *serverPk = ToGetServerPK(g_sockfd);
    const char *serverParams = CreateCroAccoutCG(g_requestId, g_osAccountId1, g_userId1, serverPk);
    ToServerCreateGroup(g_sockfd, serverParams);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);

    // 认证压测
    for (int i = 0; i < g_testNum; i++)
    {
        const char *authParamsStr = AuthParams(g_peerDeviceId, g_appName, g_isClient, g_isDeviceLevel, g_groupId, 0, nullptr);
        int *ret = TestAuth(g_osAccountId1, g_requestId, authParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
    }
    
    //  清理环境
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    ToServerDeleteGroup(g_sockfd);
}

/**
* @tc.name      导入凭据进行非对称凭据同账号认证压力测试
* @tc.number    Security_DevAuth_Account_Reliability_0305
* @tc.size      MEDIUM
* @tc.type      MTBF
* @tc.level     Level3
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Reliability_0305, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Reliability_0305");
    // 配置非对称环境
    g_localDeviceId = GetLocalDeviceId();
    const char *reqJsonStr = CreateReqJson(g_version, g_localDeviceId, g_userId1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // 模拟云侧签名&创建群组
    const char *accoutGroup = CreateAccoutCG(g_requestId, g_osAccountId1, g_userId1, pkInfoStr);
    CreateGroupTest(g_osAccountId1, g_requestId, g_appName, accoutGroup);
    // 获取对端公钥,模拟云侧签名,通知对端创建非对称凭据
    const char *serverPk = ToGetServerPK(g_sockfd);
    const char *serverParams = CreateCroAccoutCG(g_requestId, g_osAccountId1, g_userId1, serverPk);
    ToServerCreateGroup(g_sockfd, serverParams);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);

    // 认证压测
    for (int i = 0; i < g_testNum; i++)
    {
        const char *authParamsStr = AuthParams(g_peerDeviceId, g_appName, g_isClient, g_isDeviceLevel, g_groupId, 0, nullptr);
        int *ret = TestAuth(g_osAccountId1, g_requestId, authParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
    }
    
    //  清理环境
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryAccParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    ToServerDeleteGroup(g_sockfd);
}
}