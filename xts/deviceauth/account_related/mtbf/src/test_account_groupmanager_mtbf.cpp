/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "device_auth_func.h"

}


using namespace testing::ext;
using namespace std;
namespace {

const char *g_groupId;
// 获取公钥参数
const char *g_version = "1.0.0";
const char *g_deviceId = "123456789";
// 创建同/跨账号群组参数
const char *g_userId1 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4";
const char *g_userId2 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3";
static int32_t g_osAccountId1 = 100;
// static int32_t g_osAccountId2 = 101;
static int32_t g_requestId = 10;
const char *g_appName = "TestApp";
static int g_index = 0;
const char *g_queryCroParams = "{\"groupType\":1282,\"groupOwner\":\"TestApp\"}";
const char *g_queryParams = "{\"groupType\":1,\"groupOwner\":\"TestApp\"}";

const char *g_mixNumCred = "{\"deviceList\":["
"{\"udid\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"deviceId\":\"EB436BFFEC2E0DF8065865DCE"
"7620A33C6CCC1FF505F5B6B6FE388D640D70546\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"554869710F869256197A57BC57A3320032632D7741F66C84F"
"EA3E4D3D55923F0\"}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D03010703420004FD065BECA2B0116C81F534389A1F7F504F"
"616C3ECDFF83AE36A6A509F07B470B72440B5213FCF8EA461547150B2DB5C7BBFBE581B1D5A734939782C7A07567EE\","
"\"pkInfoSignature\":\"3046022100DAC381E355560D2AA13017C7D01D5637F5EED24A1861841AB3CB8EC727AC17FF022100E0B37CD275421B70C"
"7D81B3C91C34614EFB0A6ADBC04A664B628F282B2A3BE8A\",\"credentialType\":2,\"pkInfo\":{\"devicePk\":\"3059301306072A8648CE3"
"D020106082A8648CE3D03010703420004E0B675367CCC32EAC7DB7DAD769C1C901BAC887DF8041119CE3A8DDBC580CDAC3017045EBE8D61BF87A79F"
"2990EE24056FA1E00070FF935F417342903446D96B\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874"
"E4\",\"deviceId\":\"devC\",\"version\":\"1.0.0\"}}}]}";

// 批量导入参数
static int g_accountType = 1;
static int g_croAccountType = 1282;

static int g_testNum = 100000;
const char *g_createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";

class AccountMtbf : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - Account groupmanage */
void AccountMtbf::SetUpTestCase()
{
    SetAccessToken();
    InitEnv();
}

void AccountMtbf::TearDownTestCase()
{
    Destory();
}

void AccountMtbf::SetUp()
{
}

void AccountMtbf::TearDown()
{
}

/**
* @tc.name      使用对称凭据，创建同账号群组压测
* @tc.number    Security_DevAuth_Account_Reliability_0101
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMtbf, Security_DevAuth_Account_Reliability_0101, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Reliability_0101");
    for (int i = 0; i < g_testNum; i++)
    {
        int *ret = CreateGroupTest(g_osAccountId1, g_requestId, g_appName, g_createParamsStr);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
        char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryParams);
        g_groupId = GetGroupIdfromData(groupInfo, g_index);
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    }
}

/**
* @tc.name      使用非对称凭据，创建同账号群组压测
* @tc.number    Security_DevAuth_Account_Reliability_0102
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMtbf, Security_DevAuth_Account_Reliability_0102, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Reliability_0102");
    for (int i = 0; i < g_testNum; i++)
    {
        // 获取公钥信息
        const char *reqJsonStr = CreateReqJson(g_version, g_deviceId, g_userId1);
        const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
        // 模拟云侧签名&创建群组
        const char *accoutGroup = CreateAccoutCG(g_requestId, g_osAccountId1, g_userId1, pkInfoStr);
        int *ret = CreateGroupTest(g_osAccountId1, g_requestId, g_appName, accoutGroup);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
        char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryParams);
        g_groupId = GetGroupIdfromData(groupInfo, g_index);
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    }
}

/**
* @tc.name      创建跨账号群组压测
* @tc.number    Security_DevAuth_Account_Reliability_0103
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMtbf, Security_DevAuth_Account_Reliability_0103, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Reliability_0103");
    int *ret = CreateGroupTest(g_osAccountId1, g_requestId, g_appName, g_createParamsStr);
    for (int i = 0; i < g_testNum; i++)
    {
        const char *accoutGroup = CreateDiffAccoutCG(g_requestId, g_userId1, g_userId2);
        ret = CreateGroupTest(g_osAccountId1, g_requestId, g_appName, accoutGroup);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
        char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryParams);
        g_groupId = GetGroupIdfromData(groupInfo, g_index);
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    }
}

/**
* @tc.name      批量导入对称和非对称凭据压力测试
* @tc.number    Security_DevAuth_Account_Reliability_0103
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMtbf, Security_DevAuth_Account_Reliability_0103, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Reliability_0103");
    int *ret = CreateGroupTest(g_osAccountId1, g_requestId, g_appName, g_createParamsStr);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    for (int i = 0; i < g_testNum; i++)
    {
        const char *createMixInfo = CreateImportData(g_mixNumCred, g_groupId, g_accountType);
        AddMultiMembersTest(g_osAccountId1, g_appName, createMixInfo);
        sleep(1);
        uint32_t deviceNum = GetTrustedDevicesTest(g_osAccountId1, g_appName, g_groupId);
        ASSERT_EQ(deviceNum == 3, true);

        const char *delSymmInfo = DelSymmInfo(g_groupId, g_accountType);
        DelMultiMembersTest(g_osAccountId1, g_appName, delSymmInfo);
        deviceNum = GetTrustedDevicesTest(g_osAccountId1, g_appName, g_groupId);
        ASSERT_EQ(deviceNum == 1, true);
    }
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
}
}