/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "common_utils.h"
#include "device_auth_func.h"
#include "device_auth_define.h"
#include "device_auth_socket.h"
}


using namespace testing::ext;
using namespace std;
namespace {

int g_sockfd;
char g_localDeviceId[INPUT_UDID_LEN] = { 0 };
const char *g_peerDeviceId;
const char *g_groupId;
const char *g_clientBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientBaseInfo1 = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E3\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientCredential;
const char *g_serverCredential;
const char *g_serverBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,"
"\"authCode\":\"1234567812345678123456781234567812345678123456781234567812345678\"}}";
const char *g_symmInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A"
"68F1607EFCF7796C1491F834CD92\"}}";
const char *g_symmInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDA"
"D08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":"
"\"1234123412341234123412341234123412341234123412341234123412341233\"}}";
// The parameters of P2P
const char *g_connectParams = nullptr;

class AccountSySy : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - GroupManage_P2P */
void AccountSySy::SetUpTestCase()
{
    SetAccessToken();
    const char *ip = "192.168.1.11";
    static int port = 15051;
    g_sockfd = InitClient(ip, port);
    GetLocalDeviceId(g_localDeviceId);
    g_peerDeviceId = GetPeerDevId(g_sockfd);
    g_clientCredential = CreateLocalCred(g_clientBaseInfo, g_localDeviceId);
    g_serverCredential = CreateLocalCred(g_serverBaseInfo, g_peerDeviceId);
}

void AccountSySy::TearDownTestCase()
{
    ToServerClose(g_sockfd);
    sleep(1);
    CloseClient(g_sockfd);
}

void AccountSySy::SetUp()
{
    ToServerInit(g_sockfd);
    InitEnv();
    // Configure the client
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_clientCredential);
    // Notifies the peer to create symmetric credentials
    ToServerCreateGroup(g_sockfd, g_serverCredential);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
}

void AccountSySy::TearDown()
{
    CancelAuth(REQUEST_ID, APPNAME);
    ToSerCancelAuth(g_sockfd, REQUEST_ID, APPNAME);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    Destory();
    ToServerDeleteGroup(g_sockfd);
    ToServerDestroy(g_sockfd);
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_Account_Func_0701
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0701, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0701");
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    const char *serverData = RecvData(g_sockfd);
    ret = ProcessAuthData(serverData, AUTH, g_sockfd);
    RecvData(g_sockfd);
    ret = ProcessAuthData(serverData, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_FINISH, false);
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for cross-account authentication
                for the first time
* @tc.number    Security_DevAuth_Account_Func_0702
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0702, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0702");
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, groupId, 0, USER_ID1);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    const char *serverData = RecvData(g_sockfd);
    ret = ProcessAuthData(serverData, AUTH, g_sockfd);
    // serverData = RecvData(g_sockfd);
    // ret = ProcessAuthData(serverData, AUTH, g_sockfd);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    printf("delete end\n");
    ASSERT_EQ(ret[2] != ON_FINISH, true);
}

/**
* @tc.name      Import credentials Symmetric credentials are used for authentication with the same account for
                the first time
* @tc.number    Security_DevAuth_Account_Func_0705
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/

HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0705, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0705");
    // Import the credentials of the peer device
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device atuh
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      Import credentials The first time symmetric credentials are used for cross-account authentication
* @tc.number    Security_DevAuth_Account_Func_0707
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/

HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0707, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0707");
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, groupId, 0, USER_ID1);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);

    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      Import credentials The controlled end initiates authentication with the same account using symmetric
                credentials for the first time
* @tc.number    Security_DevAuth_Account_Func_0709
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level1
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0709, TestSize.Level1)
{
    LOGI("Start Security_DevAuth_Account_Func_0709");
    // Import the credentials of the peer device
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    int *ret = ToServerAuth(g_sockfd, g_localDeviceId, nullptr, GROUP_TYPE_ACCOUNT, false);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      Import credentials and the controlled end initiates cross-account authentication using symmetric
                credentials for the first time
* @tc.number    Security_DevAuth_Account_Func_0710
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0710, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0710");
    // Import the credentials of the peer device
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    int *ret = ToServerAuth(g_sockfd, g_localDeviceId, USER_ID2, GROUP_TYPE_CRO_ACCOUNT, true);
    ASSERT_EQ(ret[2] == ON_ERROR, false);

    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      The device has both same-account and point-to-point trusted relationships and initiates device-level
                authentication
* @tc.number    Security_DevAuth_Account_Func_0801
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level0
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0801, TestSize.Level0)
{
    LOGI("Start Security_DevAuth_Account_Func_0801");
    // Import the credentials of the peer device
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    // Create a peer-to-peer group
    const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    // Bind the peer device
    const char *queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);

    const char *addParamsStr = AddParams(groupId, GROUP_TYPE_P2P, PINCODE, g_connectParams);
    int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
    // start auth
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, true, nullptr, 0, nullptr);
    ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      The device has both same-account and point-to-point trusted relationships. The device initiates
                device-level authentication, but authentication with the same account fails
* @tc.number    Security_DevAuth_Account_Func_0802
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0802, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0802");
    // Import the credentials of the peer device
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    // Create a peer-to-peer group
    const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    // Bind the peer device
    const char *queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);

    const char *addParamsStr = AddParams(groupId, GROUP_TYPE_P2P, PINCODE, g_connectParams);
    int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
    
    // Delete the trust relationship between the local end and the account
    queryParams = "{\"groupType\":1,\"groupOwner\":\"com.huawei.security\"}";
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    // start device auth
    // ToServerAuth(g_sockfd, GetLocalDeviceId(), USER_ID1, GROUP_TYPE_ACCOUNT, true);g_localDeviceId
    ToServerAuth(g_sockfd, g_localDeviceId, USER_ID1, GROUP_TYPE_ACCOUNT, true);
    const char *recvData = RecvData(g_sockfd);
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] != ON_ERROR, true);
    queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      The device has both cross-account and point-to-point trusted relationships and initiates device-level
                authentication
* @tc.number    Security_DevAuth_Account_Func_0803
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0803, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0803");
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, groupId, 0, USER_ID1);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
    printf("start create group\n");
    // Create a peer-to-peer group
    const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    // Bind the peer device
    const char *queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);

    const char *addParamsStr = AddParams(groupId, GROUP_TYPE_P2P, PINCODE, g_connectParams);
    ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
    printf("Bind success\n");
    // start device auth
    authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, true, nullptr, 0, USER_ID1);
    ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      The device has both cross-account and peer-to-peer trusted relationships and initiates device-level
                authentication, but cross-account authentication fails
* @tc.number    Security_DevAuth_Account_Func_0804
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0804, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0804");
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, groupId, 0, USER_ID1);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
    printf("start create group\n");
    // Create a peer-to-peer group
    const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    // Bind the peer device
    const char *queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);

    const char *addParamsStr = AddParams(groupId, GROUP_TYPE_P2P, PINCODE, g_connectParams);
    ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
    printf("Bind success\n");

    // Delete the local cross-account trust relationship
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    // start device auth
    // ToServerAuth(g_sockfd, GetLocalDeviceId(), USER_ID2, GROUP_TYPE_ACCOUNT, true);g_localDeviceId
    ToServerAuth(g_sockfd, g_localDeviceId, USER_ID2, GROUP_TYPE_ACCOUNT, true);
    const char *recvData = RecvData(g_sockfd);
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] != ON_ERROR, true);
    queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      User space 2 authenticates the device with the same account bound to user space 1
* @tc.number    Security_DevAuth_Account_Func_0807
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0807, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0807");
    // Import the credentials of the peer device
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID2, REQUEST_ID, authParams, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      After the credentials are imported, the authentication fails and the trusted device relationship
                is deleted
* @tc.number    Security_DevAuth_Account_Func_0808
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0808, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0808");
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    const char *recvData = RecvData(g_sockfd);
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    recvData = RecvData(g_sockfd);
    recvData = "{\"step\":32896,\"errorCode\":16390,\"requestId\":\"11\",\"operCode\":201}";
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    int deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      In authentication with the same account, packets on the Client are replayed
* @tc.number    Security_DevAuth_Account_Func_0812
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0812, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0812");
    // start device auth
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *clientFirstMessage = "{\"authForm\":1,\"step\":80,\"credentialType\":1,\"userId\":\"4269DC28B639681698"
    "809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"devId\":\"64657641\",\"deviceId\":\"EB436BFFEC2E0DF8065865DCE7"
    "620A33C6CCC1FF505F5B6B6FE388D640D70546\",\"data\":{\"salt\":\"11190EDF5BD7E7394C93BDB1CF4478AF\",\"payload\":\"64"
    "65764145423433364246464543324530444638303635383635444345373632304133334336434343314646353035463542364236464533383"
    "844363430443730353436\",\"seed\":\"9C68A1ACA36E3C173AE6CAF4692C36300FCF3E6A5DAC8CDA5B96F0043FE56A38\"},\"groupAnd"
    "ModuleVersion\":\"2.0.1\",\"isDeviceLevel\":false,\"operCode\":201}";
    SendMess(g_sockfd, clientFirstMessage);
    const char *recvData = RecvData(g_sockfd);
    const char *clientSecondMessage = "{\"authForm\":1,\"step\":82,\"data\":{\"token\":\"0884A8576EB343B3D9686077726E2"
    "5D66CB0E75A836B651A31CD4C2FBCCF4251\"},\"groupAndModuleVersion\":\"2.0.17\",\"isDeviceLevel\":false,\"operCode\":"
    "201}";
    SendMess(g_sockfd, clientSecondMessage);
    recvData = RecvData(g_sockfd);
    int ret = GetErrorCode(recvData);
    ASSERT_EQ(ret == DEV_SUCCESS, false);
}

/**
* @tc.name      For authentication with the same account, the packets on the Server are replayed
* @tc.number    Security_DevAuth_Account_Func_0813
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0813, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0813");
    // start device auth
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    const char *recvData = RecvData(g_sockfd);
    recvData = "{\"step\":81,\"authForm\":1,\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874"
    "E4\",\"devId\":\"64657642\",\"deviceId\":\"F20C884EEE23A88A7006427F8828DDDDB77EDF761B7461B011E60D305B2331BC\",\"da"
    "ta\":{\"payload\":\"6465764246323043383834454545323341383841373030363432374638383238444444444237374544463736314237"
    "343631423031314536304433303542323333314243\",\"token\":\"22F886B6091EBE2B658EE9129752881494B63FCB334C886EA3D67B33F"
    "E168287\",\"salt\":\"80799889DF4E21F3965B1012D26DFC6\"},\"groupAndModuleVersion\":\"2.0.17\",\"isDeviceLevel\":fal"
    "se,\"requestId\":\"11\",\"operCode\":201}";
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_FINISH, false);
}

/**
* @tc.name      Authentication with the same account was initiated, but peerConnDeviceId was not transmitted. Procedure
* @tc.number    Security_DevAuth_Account_Func_0901
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0901, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0901");
    // Import the credentials of the peer device
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *authParams = AuthParams(nullptr, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Authentication with the same account is initiated, but servicePkgName is not passed. Procedure
* @tc.number    Security_DevAuth_Account_Func_0902
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0902, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0902");
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *authParams = AuthParams(g_peerDeviceId, nullptr, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Authentication with the same account is initiated, but isClient is not forwarded
* @tc.number    Security_DevAuth_Account_Func_0903
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0903, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0903");
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *authParams = AuthParamsError(g_peerDeviceId, APPNAME, true, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    ASSERT_EQ(ret[0] == 0, true);
}

/**
* @tc.name      Initiate authentication with the same account, and keyLenth is -1
* @tc.number    Security_DevAuth_Account_Func_0905
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0905, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0905");
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    // start device auth
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, -1, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Cross-account authentication is initiated, and no userid is transferred. Procedure
* @tc.number    Security_DevAuth_Account_Func_0906
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountSySy, Security_DevAuth_Account_Func_0906, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0906");
    const char *groupId = CreateSymmCroAcount(g_sockfd, g_groupId, g_clientBaseInfo1);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    // start device auth
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, groupId, 0, nullptr);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParams, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}
}