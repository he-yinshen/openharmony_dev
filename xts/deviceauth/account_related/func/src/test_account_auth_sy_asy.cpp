/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "common_utils.h"
#include "device_auth_func.h"
#include "device_auth_socket.h"
}


using namespace testing::ext;
using namespace std;
namespace {

int g_sockfd;
const char *g_groupId;
char g_localDeviceId[INPUT_UDID_LEN] = { 0 };
const char *g_peerDeviceId;
const char *g_clientBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientCredential;

class AccountSyAsy : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - GroupManage_P2P */
void AccountSyAsy::SetUpTestCase()
{
    SetAccessToken();
    const char *ip = "192.168.1.11";
    static int port = 15051;
    g_sockfd = InitClient(ip, port);
    GetLocalDeviceId(g_localDeviceId);
    g_peerDeviceId = GetPeerDevId(g_sockfd);
    g_clientCredential = CreateLocalCred(g_clientBaseInfo, g_localDeviceId);
}

void AccountSyAsy::TearDownTestCase()
{
    ToServerClose(g_sockfd);
    sleep(1);
    CloseClient(g_sockfd);
}

void AccountSyAsy::SetUp()
{
    ToServerInit(g_sockfd);
    InitEnv();
    // Configure the client
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_clientCredential);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);

    // Obtain the public key of the peer end, simulate the cloud signature, and notify the peer end to
    // create asymmetric credentials
    const char *serverPk = ToGetServerPK(g_sockfd);
    const char *serverParams = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, serverPk);
    ToServerCreateGroup(g_sockfd, serverParams);
}

void AccountSyAsy::TearDown()
{
    CancelAuth(REQUEST_ID, APPNAME);
    ToSerCancelAuth(g_sockfd, REQUEST_ID, APPNAME);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    Destory();
    ToServerDeleteGroup(g_sockfd);
    ToServerDestroy(g_sockfd);
}

/**
* @tc.name      If the local end has symmetric credentials and the peer end has asymmetric credentials,
                the local end initiates authentication with the same account
* @tc.number    Security_DevAuth_Account_Func_0805
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSyAsy, Security_DevAuth_Account_Func_0805, TestSize.Level3)
{
    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, false, nullptr, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, AUTH, g_sockfd);
    const char *recvData = RecvData(g_sockfd);
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_FINISH, false);
}

/**
* @tc.name      If the local end has symmetric credentials and the peer end has asymmetric credentials,
                the peer end initiates authentication with the same account
* @tc.number    Security_DevAuth_Account_Func_0806
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountSyAsy, Security_DevAuth_Account_Func_0806, TestSize.Level3)
{
    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, false, nullptr, 0, nullptr);
    int *ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, AUTH, g_sockfd);
    const char *recvData = RecvData(g_sockfd);
    ret = ProcessAuthData(recvData, AUTH, g_sockfd);
    ASSERT_EQ(ret[2] == ON_FINISH, false);
}
}