/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "common_utils.h"
#include "device_auth_func.h"
}

using namespace testing::ext;
using namespace std;
namespace {
const char *g_groupId;
// Obtaining Public Key Parameters
const char *g_createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";

// Symmetric credentials
// The same account
const char *g_symmInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A"
"68F1607EFCF7796C1491F834CD92\"}}";
const char *g_symmInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD"
"08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":"
"\"1234123412341234123412341234123412341234123412341234123412341233\"}}";
// The across account
const char *g_symmCroInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A68F"
"1607EFCF7796C1491F834CD92\"}}";
const char *g_symmCroInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD"
"08E39F207900038F91FEF95DD042FE2874E3\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":\"12"
"34123412341234123412341234123412341234123412341234123412341233\"}}";

// Asymmetric credentials
const char *g_asymCred = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":2,\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D0301070342000"
"41971364D21CB6EC3BB4EBCD67603A2230FF1AAE7545ED2080DD38E14B106DC40F45E37E71B1128D54396D899F7C2ADC277476FF6738CED27"
"25085FDB256958EB\",\"pkInfoSignature\":\"3046022100AF98D6A5812B8A5C14C3BD6B3642F22941F2E0BCE50CAE249ADDA5FCE702A0"
"33022100FA70C5795AB944F6DB1FCAA860ACC4B1876D0C01D2DDFDC08ACBB2467434F072\",\"pkInfo\":{\"devicePk\":\"30593013060"
"72A8648CE3D020106082A8648CE3D0301070342000470C804C2D1D805CD063E5C2AC62D0AAFE65DC6F2E2771A7B18CCC5631EC238333666F2"
"11DBDE62139FC38CB1D5AD8738AAD9426E3B8009BF87A51FD2216A5E7A\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F20790"
"0038F91FEF95DD042FE2874E4\",\"deviceId\":\"devB\",\"version\":\"1.0.0\"}}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D03010703420004FD065BECA2B0116C81F5"
"34389A1F7F504F616C3ECDFF83AE36A6A509F07B470B72440B5213FCF8EA461547150B2DB5C7BBFBE581B1D5A734939782C7A07567EE\","
"\"pkInfoSignature\":\"3046022100DAC381E355560D2AA13017C7D01D5637F5EED24A1861841AB3CB8EC727AC17FF022100E0B37CD2754"
"21B70C7D81B3C91C34614EFB0A6ADBC04A664B628F282B2A3BE8A\",\"credentialType\":2,\"pkInfo\":{\"devicePk\":\"305930130"
"6072A8648CE3D020106082A8648CE3D03010703420004E0B675367CCC32EAC7DB7DAD769C1C901BAC887DF8041119CE3A8DDBC580CDAC3017"
"045EBE8D61BF87A79F2990EE24056FA1E00070FF935F417342903446D96B\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207"
"900038F91FEF95DD042FE2874E4\",\"deviceId\":\"devC\",\"version\":\"1.0.0\"}}}]}";
const char *g_asymCroCred = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E3\",\"credential\":{\"credentialType\":2,\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D0301070342000"
"4FD065BECA2B0116C81F534389A1F7F504F616C3ECDFF83AE36A6A509F07B470B72440B5213FCF8EA461547150B2DB5C7BBFBE581B1D5A734"
"939782C7A07567EE\",\"pkInfoSignature\":\"3046022100CF3E24AFFD44BBEC689CA2834E08E7D8C04090780B3C0A4D6FB6A3A1885550"
"BA022100B0D239737D803693897B7D7F3BC8A59FB27E868176F268AC9D1C3A8C658217C3\",\"pkInfo\":{\"devicePk\":\"30593013060"
"72A8648CE3D020106082A8648CE3D03010703420004129EB615B423DE45874CFC10F9536AD8336A4315AA20C6099FA113627A95358E1F1349"
"31CC38327F80E568F2912E3D3E3EB489D6C543682D5E26BF2283307E3D\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F20790"
"0038F91FEF95DD042FE2874E3\",\"deviceId\":\"devB\",\"version\":\"1.0.0\"}}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E3\",\"credential\":{\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D03010703420004FD065BECA2B0116C81F5"
"34389A1F7F504F616C3ECDFF83AE36A6A509F07B470B72440B5213FCF8EA461547150B2DB5C7BBFBE581B1D5A734939782C7A07567EE\",\"p"
"kInfoSignature\":\"304602210088637776231DFD866A9898EC25C990C437EE10159BE3CC747F40BB0D459C2D93022100B73B53DC0F8285"
"E2036FD962E4589B2BC2F81FF25C7FAE87A3E299CD0B3E8ACF\",\"credentialType\":2,\"pkInfo\":{\"devicePk\":\"305930130607"
"2A8648CE3D020106082A8648CE3D03010703420004E0B675367CCC32EAC7DB7DAD769C1C901BAC887DF8041119CE3A8DDBC580CDAC3017045"
"EBE8D61BF87A79F2990EE24056FA1E00070FF935F417342903446D96B\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900"
"038F91FEF95DD042FE2874E3\",\"deviceId\":\"devC\",\"version\":\"1.0.0\"}}}"
"]}";
const char *g_mixNumCred = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":2,\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D0301070342000"
"41971364D21CB6EC3BB4EBCD67603A2230FF1AAE7545ED2080DD38E14B106DC40F45E37E71B1128D54396D899F7C2ADC277476FF6738CED27"
"25085FDB256958EB\",\"pkInfoSignature\":\"3046022100AF98D6A5812B8A5C14C3BD6B3642F22941F2E0BCE50CAE249ADDA5FCE702A0"
"33022100FA70C5795AB944F6DB1FCAA860ACC4B1876D0C01D2DDFDC08ACBB2467434F072\",\"pkInfo\":{\"devicePk\":\"30593013060"
"72A8648CE3D020106082A8648CE3D0301070342000470C804C2D1D805CD063E5C2AC62D0AAFE65DC6F2E2771A7B18CCC5631EC238333666F2"
"11DBDE62139FC38CB1D5AD8738AAD9426E3B8009BF87A51FD2216A5E7A\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F20790"
"0038F91FEF95DD042FE2874E4\",\"deviceId\":\"devB\",\"version\":\"1.0.0\"}}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D03010703420004FD065BECA2B0116C81F5"
"34389A1F7F504F616C3ECDFF83AE36A6A509F07B470B72440B5213FCF8EA461547150B2DB5C7BBFBE581B1D5A734939782C7A07567EE\","
"\"pkInfoSignature\":\"3046022100DAC381E355560D2AA13017C7D01D5637F5EED24A1861841AB3CB8EC727AC17FF022100E0B37CD2754"
"21B70C7D81B3C91C34614EFB0A6ADBC04A664B628F282B2A3BE8A\",\"credentialType\":2,\"pkInfo\":{\"devicePk\":\"305930130"
"6072A8648CE3D020106082A8648CE3D03010703420004E0B675367CCC32EAC7DB7DAD769C1C901BAC887DF8041119CE3A8DDBC580CDAC3017"
"045EBE8D61BF87A79F2990EE24056FA1E00070FF935F417342903446D96B\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207"
"900038F91FEF95DD042FE2874E4\",\"deviceId\":\"devC\",\"version\":\"1.0.0\"}}}]}";
const char *g_symmErrorCred = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":2,\"authCodeId\":102,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}}]}";
const char *g_asymErrorCred = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"TmpServerSignedInfo\":{\"serverPk\":\"3059301306072A8648CE3D020106082A8648CE3D030107034"
"20004638BCF778DEE24111AB15D2D4DC6A5194B781193E1855BFF681528C985539D489A5BE709120C949D8C6750D42F3A34D98DB329701D3D"
"2A84EF64B0B7F2FAC18E\",\"pkInfoSignature\":\"3045022100B6B308521E75B38CADC9F74E9D0A40FE382C71DB6939E8918202C09D87"
"57AFB3022045D2BC8E9B8D27423C80E96E474A1B0F0F172EFDD70CA0C5760338C902DC0837\"},\"credentialType\":1}}]}";
const char *g_asymErrorPKCred = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":2,\"authCodeId\":102,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}}]}";
const char *g_symmLocalCred = "{\"deviceList\":["
"{\"udid\":\"devA\",\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}}]}";
const char *g_symmMax = "{\"deviceList\":["
"{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}},"
"{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2"
"874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":\"1234123412341234123412341234123412"
"341234123412341234123412341234\"}}]}";

class AccountMultiProcess : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - Account groupmanage */
void AccountMultiProcess::SetUpTestCase()
{
    SetAccessToken();
}

void AccountMultiProcess::TearDownTestCase()
{
}

void AccountMultiProcess::SetUp()
{
    InitEnv();
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
}

void AccountMultiProcess::TearDown()
{
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    Destory();
}

/**
* @tc.name      Batch import and batch delete symmetric credentials with the account
* @tc.number    Security_DevAuth_Account_Func_0501
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level0
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0501, TestSize.Level0)
{
    LOGI("Security_DevAuth_Account_Func_0501");
    // Import symmetric credentials in batches
    ResetPeerDeviceId();
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
    // Delete symmetric credentials in bulk
    const char *delSymmInfo = DelSymmInfo(g_groupId, GROUP_TYPE_ACCOUNT);
    DelMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, delSymmInfo);
    deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 1, true);
}

/**
* @tc.name      Import asymmetric credentials of the same account in batches
* @tc.number    Security_DevAuth_Account_Func_0502
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0502, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0502");
    // Import symmetric credentials in batches
    const char *createAsymmInfo = CreateImportData(g_asymCred, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createAsymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      Batch import and batch delete cross-account symmetric credentials
* @tc.number    Security_DevAuth_Account_Func_0503
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0503, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0503");
    // Create a cross-account group
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, USER_ID2);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Import symmetric credentials in batches
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_CROACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    const char *createSymmCroInfo = CreateSymmInfo(g_symmCroInfo1, g_symmCroInfo2, g_groupId, GROUP_TYPE_CRO_ACCOUNT);
    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmCroInfo);
    sleep(3);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
    // Delete a cross-account group
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    sleep(3);
    deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 0, true);
}

/**
* @tc.name      Batch import and batch delete cross-account asymmetric credentials
* @tc.number    Security_DevAuth_Account_Func_0504
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0504, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0504");
    // Create a cross-account group
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, USER_ID2);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Import symmetric credentials in batches
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_CROACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    const char *createAsymmCroInfo = CreateImportData(g_asymCroCred, g_groupId, GROUP_TYPE_CRO_ACCOUNT);
    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createAsymmCroInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
    // Delete a cross-account group
    sleep(1);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      Batch imports symmetric credentials, one of which is of asymmetric type
* @tc.number    Security_DevAuth_Account_Func_0601
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0601, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0601");
    // Import symmetric credentials in batches
    const char *createSymmInfo = CreateImportData(g_symmErrorCred, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 2, true);
}

/**
* @tc.name      Importing asymmetric credentials in batches failed to verify the server public key. Procedure
* @tc.number    Security_DevAuth_Account_Func_0602
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0602, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0602");
    // Import symmetric credentials in batches
    const char *createSymmInfo = CreateImportData(g_asymErrorCred, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 2, true);
}

/**
* @tc.name      Batch imports asymmetric credentials, one of which is of symmetric type
* @tc.number    Security_DevAuth_Account_Func_0603
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0603, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0603");
    // Import symmetric credentials in batches
    const char *createSymmInfo = CreateImportData(g_asymErrorPKCred, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 2, true);
}

/**
* @tc.name      Import symmetric and asymmetric credentials in batches
* @tc.number    Security_DevAuth_Account_Func_0604
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0604, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0604");
    // Import symmetric credentials in batches
    const char *createMixInfo = CreateImportData(g_mixNumCred, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createMixInfo);
    sleep(3);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      Import cross-account credentials into the same account group
* @tc.number    Security_DevAuth_Account_Func_0605
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0605, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0605");
    // Import symmetric credentials in batches
    const char *createSymmCroInfo = CreateSymmInfo(g_symmCroInfo1, g_symmCroInfo2, g_groupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmCroInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 1, true);
}

/**
* @tc.name      Batch import credentials, import the device credentials
* @tc.number    Security_DevAuth_Account_Func_0606
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0606, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0606");
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    // Import symmetric credentials in batches
    const char *createAsymmInfo = CreateImportData(g_symmLocalCred, g_groupId, GROUP_TYPE_ACCOUNT);
    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createAsymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      Import the same account credentials of other devices in batches
* @tc.number    Security_DevAuth_Account_Func_0607
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0607, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0607");
    // Import symmetric credentials in batches
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);

    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);
    deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      Import the cross-account credentials of existing devices in batches
* @tc.number    Security_DevAuth_Account_Func_0608
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0608, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0608");
    // Create a cross-account group
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, USER_ID2);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Import symmetric credentials in batches
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_CROACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    const char *createSymmCroInfo = CreateSymmInfo(g_symmCroInfo1, g_symmCroInfo2, g_groupId, GROUP_TYPE_CRO_ACCOUNT);
    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmCroInfo);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Repeat batch Import
    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmCroInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      No group created, batch import credentials
* @tc.number    Security_DevAuth_Account_Func_0609
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0609, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0609");
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    // Import symmetric credentials in batches
    const char *createAsymmInfo = CreateImportData(g_symmLocalCred, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createAsymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 0, true);
}

/**
* @tc.name      Batch add 10 symmetric credentials with the same account
* @tc.number    Security_DevAuth_Account_Func_0610
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0610, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0610");
    const char *createAsymmInfo = CreateImportData(g_symmMax, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createAsymmInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 3, true);
}

/**
* @tc.name      To add trusted devices in batches, leave the input parameter empty
* @tc.number    Security_DevAuth_Account_Func_0611
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0611, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0611");
    const char *createAsymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    int *ret = AddMultiMembersTest(0, APPNAME, createAsymmInfo);
    ASSERT_EQ(ret[3] == 0, false);

    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, nullptr, createAsymmInfo);
    ASSERT_EQ(ret[3] == 0, false);

    ret = AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, nullptr);
    ASSERT_EQ(ret[3] == 0, false);
}

/**
* @tc.name      No groups are created. Delete groups in batches
* @tc.number    Security_DevAuth_Account_Func_0612
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0612, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0612");
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    // Delete symmetric credentials in bulk
    const char *delSymmInfo = DelSymmInfo(g_groupId, GROUP_TYPE_ACCOUNT);
    int *ret = DelMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, delSymmInfo);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      To delete trusted devices in batches, the input parameter is empty
* @tc.number    Security_DevAuth_Account_Func_0613
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountMultiProcess, Security_DevAuth_Account_Func_0613, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0613");
    const char *delSymmInfo = DelSymmInfo(g_groupId, GROUP_TYPE_ACCOUNT);
    int *ret = DelMultiMembersTest(0, APPNAME, delSymmInfo);
    ASSERT_EQ(ret[3] == 0, false);
    ret = DelMultiMembersTest(OS_ACCOUNT_ID1, nullptr, delSymmInfo);
    ASSERT_EQ(ret[3] == 0, false);
    ret = DelMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, nullptr);
    ASSERT_EQ(ret[3] == 0, false);
}
}