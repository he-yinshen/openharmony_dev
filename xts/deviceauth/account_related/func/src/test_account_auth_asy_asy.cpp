/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "common_utils.h"
#include "device_auth_func.h"
#include "device_auth_socket.h"
}


using namespace testing::ext;
using namespace std;
namespace {

int g_sockfd;
char g_localDeviceId[INPUT_UDID_LEN] = { 0 };
const char *g_peerDeviceId;
const char *g_groupId;

class AccountAsyAsy : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - GroupManage_P2P */
void AccountAsyAsy::SetUpTestCase()
{
    SetAccessToken();
    const char *ip = "192.168.1.11";
    static int port = 15051;
    g_sockfd = InitClient(ip, port);
    g_peerDeviceId = GetPeerDevId(g_sockfd);
    GetLocalDeviceId(g_localDeviceId);
}

void AccountAsyAsy::TearDownTestCase()
{
    ToServerClose(g_sockfd);
    sleep(1);
    CloseClient(g_sockfd);
}

void AccountAsyAsy::SetUp()
{
    ToServerInit(g_sockfd);
    InitEnv();
    // Configure the client
    const char *reqJsonStr = CreateReqJson(VERSION, g_localDeviceId, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Simulate signatures on the cloud side & Create groups
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    // Obtain the public key of the peer end, simulate the cloud signature, and notify the peer end
    // to create asymmetric credentials
    const char *serverPk = ToGetServerPK(g_sockfd);
    const char *serverParams = CreateCroAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, serverPk);
    ToServerCreateGroup(g_sockfd, serverParams);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
}

void AccountAsyAsy::TearDown()
{
    CancelAuth(REQUEST_ID, APPNAME);
    ToSerCancelAuth(g_sockfd, REQUEST_ID, APPNAME);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    Destory();
    ToServerDeleteGroup(g_sockfd);
    ToServerDestroy(g_sockfd);
}

/**
* @tc.name      Not imported credentials Not the first time asymmetric credentials are used for
                authentication with the same account
* @tc.number    Security_DevAuth_Account_Func_0703
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level1
*/

HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Func_0703, TestSize.Level1)
{
    LOGI("Start Security_DevAuth_Account_Func_0703");
    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId,
                                            0, nullptr);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
    ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      Not imported credentials Non-first time asymmetric credentials are used for
                cross-account authentication
* @tc.number    Security_DevAuth_Account_Func_0704
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Func_0704, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0704");
    const char *croGroupId = CreatAsyCroAccount(g_sockfd, g_localDeviceId);

    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, croGroupId,
                                            0, USER_ID1);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    char *deletParamsStr = DeleteGroupParams(croGroupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      Import credentials for the first time asymmetric credentials are used for authentication
                with the same account
* @tc.number    Security_DevAuth_Account_Func_0706
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Func_0706, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0706");
    const char *importInfo = GetCroPKInfo(g_groupId, GROUP_TYPE_ACCOUNT);
    LOGI("import info: %s\n", importInfo);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, importInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 2, true);
    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId,
                                            0, nullptr);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      Import credentials The first time asymmetric credentials are used for cross-account authentication
* @tc.number    Security_DevAuth_Account_Func_0708
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Func_0708, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_Account_Func_0708");
    const char *croGroupId = CreatAsyCroAccount(g_sockfd, g_localDeviceId);
    const char *importInfo = GetCroPKInfo(croGroupId, GROUP_TYPE_CRO_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, importInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, croGroupId);
    ASSERT_EQ(deviceNum == 2, true);
    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, croGroupId,
                                            0, USER_ID1);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}

/**
* @tc.name      If no credentials are imported, the authentication fails. Check whether the trusted device
                relationship is added
* @tc.number    Security_DevAuth_Account_Func_0809
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Func_0809, TestSize.Level3)
{
    LOGI("Start Security_DevAuth_Account_Func_0809");
    uint32_t deviceNumBeforeAuth = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    // start device auth
    const char *authParams = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId,
                                        0, nullptr);
    AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParams, AUTH, g_sockfd);
    const char *recvData = RecvData(g_sockfd);
    recvData = "{\"step\":-1,\"errorCode\":16390,\"requestId\":\"10\",\"operCode\":201}";
    ProcessAuthData(recvData, AUTH, g_sockfd);
    recvData = RecvData(g_sockfd);
    ProcessAuthData(recvData, AUTH, g_sockfd);
    // SendMess(g_sockfd, sendData);
    // CancelAuth(REQUEST_ID, APPNAME);
    // ToSerCancelAuth(g_sockfd, REQUEST_ID, APPNAME);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == deviceNumBeforeAuth, true);
}

/**
* @tc.name      Non-first initiated authentication with the same account Asymmetric credentials, service level
                authentication, no group id specified
* @tc.number    Security_DevAuth_Account_Func_0904
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountAsyAsy, Security_DevAuth_Account_Func_0904, TestSize.Level4)
{
    LOGI("Start Security_DevAuth_Account_Func_0904");
    // start device auth
    const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, g_groupId, 0, nullptr);
    int *ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
    ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
    ASSERT_EQ(ret[2] == ON_ERROR, false);
}
}