/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "common_utils.h"
#include "device_auth_func.h"
#include "device_auth_define.h"
}

using namespace testing::ext;
using namespace std;
namespace {

const char *g_groupId;
const char *g_createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";

class AccountGroupManage : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - Account groupmanage */
void AccountGroupManage::SetUpTestCase()
{
    SetAccessToken();
}

void AccountGroupManage::TearDownTestCase()
{
}

void AccountGroupManage::SetUp()
{
    InitEnv();
}

void AccountGroupManage::TearDown()
{
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    Destory();
}

/**
* @tc.name      Create an account group using an asymmetric key
* @tc.number    Security_DevAuth_Account_Func_0101
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level0
*/

HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0101, TestSize.Level0)
{   
    LOGI("Security_DevAuth_Account_Func_0101.");
    // Obtaining Public Key Information
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Simulate signatures on the cloud side & Create groups
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // A single device is authenticated
    char *authParams = AuthParams("devB", APPNAME, true, true, g_groupId, 0, nullptr);
    int64_t clientReqId = 111;
    int64_t serverReqId = 222;
    ret = LocalAccountAuth(OS_ACCOUNT_ID1, clientReqId, serverReqId, authParams);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
};

/**
* @tc.name      Create coaccount groups using symmetric credentials
* @tc.number    Security_DevAuth_Account_Func_0102
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level1
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0102, TestSize.Level1)
{
    LOGI("Security_DevAuth_Account_Func_0102.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      Import asymmetric credentials and create groups twice with the same userId
* @tc.number    Security_DevAuth_Account_Func_0103
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0103, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0103.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Creating a Group for the first Time
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Creating Groups Repeatedly
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Groups are created twice with the same userId using symmetric credentials
* @tc.number    Security_DevAuth_Account_Func_0104
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0104, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0104.");
    // Creating a Group for the first Time
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Creating Groups Repeatedly
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}


/**
* @tc.name      Failed to create an account group because invalid credentials were imported. Procedure
* @tc.number    Security_DevAuth_Account_Func_0105
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0105, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0105.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Impersonate invalid credential information
    const char *accoutGroup = CreateAccoutError(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Importing asymmetric credentials, serverPK verification failed, and creating the same
                account group failed
* @tc.number    Security_DevAuth_Account_Func_0106
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0106, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0106.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Impersonate invalid credential information
    const char *accoutGroup = CreateAccoutPK(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Import asymmetric credentials. The userId information is inconsistent with the local
                information. Create a group with the same account
* @tc.number    Security_DevAuth_Account_Func_0108
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0108, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0108.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Modify the userId
    const char *newInfo = FixPkInfoStr(pkInfoStr);
    // Impersonate invalid credential information
    const char *accoutGroup = CreateAccoutPK(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, newInfo);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name     If the same account group exists, create a cross-account group
* @tc.number    Security_DevAuth_Account_Func_0109
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0109, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0109.");
    // user1 create account group in first user space
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // user1 create across-account group of user2 in first user space
    accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, USER_ID2);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // user1 delete across-account group of user2 in first user space
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_CROACCOUNT_GROUP);
    const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      If the same account group does not exist, create a cross-account group
* @tc.number    Security_DevAuth_Account_Func_0110
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0110, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0110.");
    // user1 create across-account group of user2 in first user space
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, USER_ID2);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      An asymmetric key has been used to create a group, but the group data is lost
* @tc.number    Security_DevAuth_Account_Func_0201
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0201, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0201.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);

    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    char *deletParamsStr = DeleteGroupParams(USER_ID1);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    sleep(2);

    reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      An asymmetric key has been used to create a group, but the key data is lost. Delete the
                group and create a group with the same account
* @tc.number    Security_DevAuth_Account_Func_0202
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0202, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0202.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);

    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    
    reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      If symmetric credentials are used, a group is created, but the group data is lost,
                and the same group with the same account is created again
* @tc.number    Security_DevAuth_Account_Func_0203
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0203, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0203.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Delete the group data and create the group again
    char *deletParamsStr = DeleteGroupParams(USER_ID1);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    sleep(1);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      If symmetric credentials are used, a group is created, but the key data is lost,
                and the group with the same account is created again
* @tc.number    Security_DevAuth_Account_Func_0204
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0204, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0204.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Delete the key data and create the group again
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);

    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      In different user Spaces, different userids use asymmetric keys to create the
                same account group twice
* @tc.number    Security_DevAuth_Account_Func_0205
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0205, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0205.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Userspace 1 Create userId1 and an account group
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Userspace 2 Create userId2 and an account group
    accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID2, USER_ID2, pkInfoStr);
    ret = CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
    char *deletParamsStr = DeleteGroupParams(USER_ID2);
    DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      In different user Spaces, the same userId is used to create the same account
                group twice using an asymmetric key
* @tc.number    Security_DevAuth_Account_Func_0206
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0206, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0206.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Userspace 1 Create userId1 and an account group
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Userspace 2 Create userId1 and an account group
    accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID2, USER_ID1, pkInfoStr);
    ret = CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    char *deletParamsStr = DeleteGroupParams(USER_ID1);
    DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      In different user Spaces, the same userId is used to create two groups with
                the same account using symmetric credentials
* @tc.number    Security_DevAuth_Account_Func_0207
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0207, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0207.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Userspace 1 Create userId1 and an account group
    ret = CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Userspace 2 Create userId1 and an account group
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID2, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      In different user Spaces, different userids are used to create two groups with
                the same account using symmetric credentials
* @tc.number    Security_DevAuth_Account_Func_0208
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0208, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0208.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Userspace 1 Create userId1 and an account group
    ret = CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // Userspace 2 Create userId1 and an account group
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID2, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      Create a group with the same account. The length of symmetric credentials exceeds 64 bits
* @tc.number    Security_DevAuth_Account_Func_0209
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0209, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0209.");
    const char *createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
    "A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
    "\"authCode\":\"12341234123412341234123412341234123412341234123412341234123412341234\"}}";

    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Using asymmetric credentials, create the same account group twice with different userids
* @tc.number    Security_DevAuth_Account_Func_0210
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0210, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0210.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);

    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    // The same account group with different userids is created in the same user space
    reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID2);
    pkInfoStr = TestGetRegisterInfo(reqJsonStr);

    accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID2, pkInfoStr);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    char *deletParamsStr = DeleteGroupParams(USER_ID2);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      Using symmetric credentials, groups are created twice for different userids
* @tc.number    Security_DevAuth_Account_Func_0211
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0211, TestSize.Level2)
{
    LOGI("Security_DevAuth_Account_Func_0211.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    
    // Create the same account group with different userids
    const char *createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639"
    "681698809A67EDAD08E39F207900038F91FEF95DD042FE2874R5\",\"credential\":{\"credentialType\":1,"
    "\"authCodeId\":101,\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);

    ASSERT_EQ(ret[2] == ON_FINISH, true);

    g_groupId = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874R5";
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
}

/**
* @tc.name      Create a group with the same account and set groupType to 256
* @tc.number    Security_DevAuth_Account_Func_0301
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0301, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0301.");
    const char *createParamsStr = "{\"groupType\":256,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698"
    "809A67EDAD08E39F207900038F91FEF95DD042FE2874R5\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
    "\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Create a group with the same account, and no userId is passed
* @tc.number    Security_DevAuth_Account_Func_0302
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0302, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0302.");
    const char *createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":nullptr,"
    "\"credential\":{\"credentialType\":1,\"authCodeId\":101,\"authCode\":\"123412341234123412341234123412341234"
    "1234123412341234123412341234\"}}";
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    ASSERT_EQ(ret[3] == ON_FINISH, false);
}

/**
* @tc.name      Create the cross-account group without no userId is passed
* @tc.number    Security_DevAuth_Account_Func_0303
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0303, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0303.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, nullptr, USER_ID2);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Create the same account group without passing credential
* @tc.number    Security_DevAuth_Account_Func_0304
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0304, TestSize.Level4)
{
    LOGI("Security_DevAuth_Account_Func_0304.");
    const char *createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
    "A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":nullptr}";
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    ASSERT_EQ(ret[EXCUTE] == DEV_ERR_JSON_FAIL, true);
}

/**
* @tc.name      Create a cross-account group and enter peerUserId as userId
* @tc.number    Security_DevAuth_Account_Func_0305
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0305, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0305.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, USER_ID1);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      A cross-account group is created, and peerUserId is not passed in
* @tc.number    Security_DevAuth_Account_Func_0306
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0306, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0306.");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    const char *accoutGroup = CreateDiffAccoutCG(REQUEST_ID, USER_ID1, nullptr);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Create the same account group with a credential of -1
* @tc.number    Security_DevAuth_Account_Func_0307
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0307, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0307.");
    const char *createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
    "A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":-1,\"authCodeId\":101,"
    "\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Create the same account group and import symmetric credentials and non-symmetric credentials
                for credential
* @tc.number    Security_DevAuth_Account_Func_0308
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0308, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0308.");
    const char *createParamsStr = "{\"groupType\":1,\"deviceId\":\"devA\",\"userId\":\"4269DC28B639681698809"
    "A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":2,\"authCodeId\":101,"
    "\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      If no group is created, delete the corresponding group
* @tc.number    Security_DevAuth_Account_Func_0401
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0401, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0401.");
    char *deletParamsStr = DeleteGroupParams(USER_ID1);
    int *ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      If no cross-account group is created, delete the group
* @tc.number    Security_DevAuth_Account_Func_0402
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0402, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0402.");
    char *deletParamsStr = DeleteGroupParams(USER_ID2);
    int *ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    printf("%d\n", ret[2]);
    printf("%d\n", ret[0]);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      user2 Delete the same account group under user1
* @tc.number    Security_DevAuth_Account_Func_0403
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(AccountGroupManage, Security_DevAuth_Account_Func_0403, TestSize.Level3)
{
    LOGI("Security_DevAuth_Account_Func_0403.");
    const char *reqJsonStr = CreateReqJson(VERSION, ACCOUNT_DEVICE_ID, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);

    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    
    char *deletParamsStr = DeleteGroupParams(USER_ID1);
    ret = DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}
}