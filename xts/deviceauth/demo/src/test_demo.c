/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "securec.h"
#include <getopt.h>
#include "hc_log.h"
#include <signal.h>
#include <pthread.h>
#include "device_auth_func.h"
#include "common_utils.h"
#include "device_auth_socket.h"

#define SYMMETRIC_LOCAL 1
#define ASYMMETRIC_PEER 2

int32_t g_osAccountId = 100;
int32_t g_requestId = 10;
static int g_visbilityPublic = -1;
const char *g_groupId;

// base parameters
char g_localDeviceId[INPUT_UDID_LEN] = { 0 };
const char *g_peerDeviceId;
static int g_listenfd;
static int g_port = 15051;
static int g_sockfd = -1;
static int g_maxIterNum = 50;
static int g_keyType = 1;
static int g_exDevice = 1;

static const char *g_userId1 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4";
static const char *g_version = "1.0.0";
// Symmetric credentials of the account groups
const char *g_clientBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientBaseInfo1 = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E3\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientCredential;
const char *g_serverCredential;
const char *g_serverBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,"
"\"authCode\":\"1234567812345678123456781234567812345678123456781234567812345678\"}}";

// The parameters of the same account
const char *g_symmInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A"
"68F1607EFCF7796C1491F834CD92\"}}";
const char *g_symmInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDA"
"D08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":"
"\"1234123412341234123412341234123412341234123412341234123412341233\"}}";

void ConnectServer(const char *ip)
{
    const int port = 15051;
    g_sockfd = InitClient(ip, port);
    ToServerInit(g_sockfd);
}

void DemoCreate(int groupType)
{
    printf("groupType:%d\n", groupType);
    if (groupType == GROUP_TYPE_P2P)
    {
        const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, NULL, GROUPNAME, g_visbilityPublic);
        int *ret = CreateGroupTest(g_osAccountId, g_requestId, APPNAME, createParamsStr);
        if (ret[2] == ON_FINISH) {
            printf("Create group success.\n");
        } else {
            printf("Create group failed.\n");
        }
    } else if (groupType == GROUP_TYPE_ACCOUNT) {
        g_peerDeviceId = GetPeerDevId(g_sockfd);
        GetLocalDeviceId(g_localDeviceId);
        if (g_keyType == SYMMETRIC_LOCAL)
        {
            g_clientCredential = CreateLocalCred(g_clientBaseInfo, g_localDeviceId);
            g_serverCredential = CreateLocalCred(g_serverBaseInfo, g_peerDeviceId);
            CreateGroupTest(g_osAccountId, g_requestId, APPNAME, g_clientCredential);
            ToServerCreateGroup(g_sockfd, g_serverCredential);
            char *groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, QUERY_ACCOUNT_GROUP);
            const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
            const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, groupId, GROUP_TYPE_ACCOUNT);
            AddMultiMembersTest(g_osAccountId, APPNAME, createSymmInfo);
        } else {
            const char *reqJsonStr = CreateReqJson(g_version, g_localDeviceId, g_userId1);
            const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
            // Simulate signatures on the cloud side & Create groups
            const char *accoutGroup = CreateAccoutCG(g_requestId, g_osAccountId, g_userId1, pkInfoStr);
            CreateGroupTest(g_osAccountId, g_requestId, APPNAME, accoutGroup);
            // Obtain the public key of the peer end, simulate the cloud signature, and notify the peer end
            // to create asymmetric credentials
            const char *serverPk = ToGetServerPK(g_sockfd);
            const char *serverParams = CreateCroAccoutCG(g_requestId, g_osAccountId, g_userId1, serverPk);
            ToServerCreateGroup(g_sockfd, serverParams);
        }
    }
}

void DemoCreateAndBind()
{
    char *groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, QUERY_P2P_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    if (strcmp(g_groupId, "null groupId") == 0)
    {
        printf("Start create group.\n");
        const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, NULL, GROUPNAME, g_visbilityPublic);
        int *ret = CreateGroupTest(g_osAccountId, g_requestId, APPNAME, createParamsStr);
        if (ret[2] == ON_FINISH) {
            printf("Create group success.\n");
        }
    } else {
        printf("Group has already");
    }
    groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, QUERY_ALL_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, NULL);
    if (g_exDevice == SYMMETRIC_LOCAL) {
        TestBind(g_osAccountId, g_requestId, APPNAME, addParamsStr, g_sockfd);
    } else {
        ToServerBind(g_sockfd, g_groupId, PINCODE);
    }

}

void DemoDeleteGroup()
{
    char *groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, QUERY_ALL_GROUP);
    for (int i = 0; i < GetGroupNum(groupInfo); i++) {
        g_groupId = GetGroupIdfromData(groupInfo, i);
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        DeleteGroupTest(g_osAccountId, g_requestId, APPNAME, deletParamsStr);
    }
}

void DemoStarAuth(int type)
{
    if (g_exDevice == SYMMETRIC_LOCAL)
    {
        g_peerDeviceId = GetPeerDevId(g_sockfd);
        printf("peer deviceId:%s\n", g_peerDeviceId);
        if (type == GROUP_TYPE_ALL) {
            const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, true, true, NULL, 0, NULL);
            TestAuth(g_osAccountId, g_requestId, authParamsStr, g_sockfd);
        } else if (type == GROUP_TYPE_ACCOUNT) {
            const char *queryParams = "{\"groupType\":1,\"groupOwner\":\"com.huawei.security\"}";
            char *groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, queryParams);
            g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
            const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, true, false, g_groupId,
                                            0, g_userId1);
            TestAuth(g_osAccountId, g_requestId, authParamsStr, g_sockfd);
        } else if (type == GROUP_TYPE_P2P) {
            char *groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, QUERY_ALL_GROUP);
            g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
            const char *authParamsStr = AuthParams(g_peerDeviceId, APPNAME, true, false, g_groupId, 0, NULL);
            TestAuth(g_osAccountId, g_requestId, authParamsStr, g_sockfd);
        }
    } else {
        if (type == GROUP_TYPE_ALL)
        {
            ToServerAuth(g_sockfd, g_localDeviceId, NULL, GROUP_TYPE_ALL, true);
            // TODO:需要根据对端认证首报文来判断是点对点还是账号认证，如果是账号认证才需要接收处理第三包
            const char *recvData = RecvData(g_sockfd);
            ProcessAuthData(recvData, AUTH, g_sockfd);
        } else if (type == GROUP_TYPE_ACCOUNT){
            ToServerAuth(g_sockfd, g_localDeviceId, g_userId1, GROUP_TYPE_ACCOUNT, false);
            const char *recvData = RecvData(g_sockfd);
            ProcessAuthData(recvData, AUTH, g_sockfd);
        } else if (type == GROUP_TYPE_P2P){
            ToServerAuth(g_sockfd, g_localDeviceId, NULL, GROUP_TYPE_P2P, false);
            // const char *recvData = RecvData(g_sockfd);
            // ProcessAuthData(recvData, AUTH, g_sockfd);
        }
    }
}

void DemoSetKeyType(int type)
{
    g_keyType = type;
}

void DemoQueryDeviceInfo()
{
    char *groupInfo = GetGroupInfoTest(g_osAccountId, APPNAME, QUERY_ALL_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    GetTrustedDevicesTest(g_osAccountId, APPNAME, g_groupId);
}


// Handlers for SIGINT and SIGTERM
void EXIT(int sig)
{
    CloseListen(g_listenfd);
    exit(0);
}

void *PthMain(void *arg)
{
    // The arg parameter is the socket of the new client.
    int sockfd = (long) arg;
    const char *recvData = RecvData(sockfd);
    while (strcmp(recvData, "close") != 0) {
        int64_t tmpReqId = 10;
        GetReqIdFromData(&tmpReqId, recvData);
        int operCode = GetOperCodeFromData(recvData);
        printf("chose function\n");
        SwitchCase(tmpReqId, operCode, recvData, sockfd);
        recvData = RecvData(sockfd);
        printf("recvData\n");
    }
    CloseClient(sockfd);
    printf("The client has been disconnected.\n");
    return 0;
}

int DemoServer()
{
    // Ignore all signals
    for (int ii = 0; ii < g_maxIterNum; ii++) {
        if (signal(ii, SIG_IGN) != 0) {
            printf("signal SIG_IGN.\n");
        }
    }
    // Set the handlers for SIGINT and SIGTERM
    if (signal(SIGINT, EXIT) != 0) {
        printf("signal init.\n");
    }
    if (signal(SIGTERM, EXIT) != 0) {
        printf("signal term.\n");
    }
    g_listenfd = InitServer(g_port);
    if (g_listenfd == -1) {
        printf("server init failed, exit.\n");
        return -1;
    }
    printf("init sucess\n");
    // SetSocket(g_listenfd);
    pthread_attr_t atrr = {0};
    pthread_attr_init(&atrr);
    pthread_attr_setscope(&atrr, PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setdetachstate(&atrr, PTHREAD_CREATE_DETACHED);
    while (1) {
        printf("wait accept:%d\n", g_listenfd);
        int sockfd = Accept(g_listenfd);
        if (sockfd == -1) {
            continue;
        }
        pthread_t pthid;
        if (pthread_create(&pthid, NULL, PthMain, (void*)((long)sockfd)) != 0) {
            printf("Failed to create thread. Program exits.\n");
            return -1;
        }
        printf("A thread to communicate with the client has been created.\n");
    }
}

static struct option longOpts[] = {
    { "server", no_argument, NULL, 's'},
    { "requestId", required_argument, NULL, 'r'},
    { "input_ip", required_argument, NULL, 'p'},
    { "which_device", no_argument, NULL, 'e'},
    { "create_group", required_argument, NULL, 'c'},
    { "delete", no_argument, NULL, 'd'},
    { "query", no_argument, NULL, 'q'},
    { "key_type", required_argument, NULL, 'k'},
    { "bind", no_argument, NULL, 'b'},
    { "auth", required_argument, NULL, 'a'},
    { "das_ecspeke", required_argument, NULL, 'f'},
    { 0, 0, 0, 0 }
};
/*
    client process:
        p: Input ip and connect server
        c: Create p2p group
        b: Create group and bind devices
        d: Delete group
        a: start p2p device auth
        r: Input requestId
        f: Input file path of message to the ecspeke protocol
        q: Query group info and device info
        das_ecspeke: 
    server process:
        s: Start server
*/
static int32_t ParseTestParams(int argc, char **argv)
{
    int ch;
    int ret = 0;
    int idx = 0;
    const char *shortOpts = "sr:p:ec:dqk:ba:f:";
    while ((ch = getopt_long(argc, argv, shortOpts, longOpts, &idx)) >= 0) {
        switch (ch) {
            case 's':
                DemoServer();
                break;
            case 'r':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                        printf("Please input requestId.\n");
                    } else {
                        g_requestId = atoi(optarg);
                        printf("input requestId: %d\n", g_requestId);
                    }
                break;
            case 'p':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    printf("Input ip error.\n");
                } else {
                    printf("input ip: %s\n", optarg);
                    ConnectServer((const char*)optarg);
                }
                break;
            case 'e':
                g_exDevice = ASYMMETRIC_PEER;
                break;
            case 'c':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    printf("Please input groupType.\n");
                } else {
                    printf("Create a group of type %s\n", optarg);
                    DemoCreate(atoi(optarg));
                }
                break;
            case 'd':
                DemoDeleteGroup();
                break;
            case 'q':
                DemoQueryDeviceInfo();
                break;
            case 'k':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    printf("Please input the type of key.\n");
                } else {
                    printf("Set the type of key :%s\n", optarg);
                    DemoSetKeyType(atoi(optarg));
                }
                break;
            case 'b':
                DemoCreateAndBind();
                break;
            case 'a':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                    printf("Please the params of device auth.\n");
                } else {
                    printf("The type of device auth %s\n", optarg);
                    DemoStarAuth(atoi(optarg));
                }
                break;
            case 'f':
                if (optarg == NULL || strlen(optarg) == 0 || strlen(optarg) >= 128) {
                        printf("no protocol messeage.\n");
                    } else {
                        char messeageDir[2048] = {0};
                        memcpy_s(messeageDir, strlen(optarg), optarg, strlen(optarg));
                        DemoEcSpeke(messeageDir, g_requestId);
                    }
                break;
        }
    }
    if (g_sockfd != -1) {
        ToServerClose(g_sockfd);
        sleep(1);
        CloseClient(g_sockfd);
    }
    return ret;
}

int main(int argc, char **argv)
{
    SetAccessToken();
    InitEnv();
    if (ParseTestParams(argc, argv)) {
    return 0;
    }
    return 0;
}
