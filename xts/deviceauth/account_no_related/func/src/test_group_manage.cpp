/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include "hc_log.h"
extern "C" {
#include "device_auth_define.h"
#include "device_auth_func.h"
#include "common_utils.h"
}

using namespace testing::ext;
using namespace std;
namespace {

// static int g_maxGroupNum = 100;
const char *g_deviceId1 = "3C58C27533D8";
const char *g_createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, g_deviceId1, GROUPNAME, VISIBILITY_PUBLIC);
const char *g_deleteParams = DeleteGroupParams(GROUP_ID_NORMAL);

class GroupManage : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

void GroupManage::SetUpTestCase()
{
    SetAccessToken();
}

void GroupManage::TearDownTestCase()
{
}

void GroupManage::SetUp()
{
    InitEnv();
}

void GroupManage::TearDown()
{
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
    const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    if (strcmp(groupId, "null groupId") != 0) {
        char *deletParamsStr = DeleteGroupParams(groupId);
        DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    }
    DeInitTest();
}

/**
* @tc.name      Create a peer-to-peer group and check whether the group is created successfully
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0101
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level0
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0101, TestSize.Level0)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0101");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    // Compare groupId for correctness
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
    int ret1 = CompareInfo(groupInfo, FIELD_GROUP_ID, GROUP_INDEX, GROUP_ID_NORMAL);
    DestoryTest(groupInfo);
    printf("data:%d\n", ret1);
    ASSERT_EQ(ret1 == DEV_SUCCESS, true);
}

/**
* @tc.name      Create the peer-to-peer group twice, and set the same parameter for User1
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0102
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0102, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0102");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Create a point-to-point group. User1 The number of groups is max+1
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0103
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
// HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0103, TestSize.Level2)
// {
//     LOGI("start Security_DevAuth_P2P_GroupManage_Func_0103");
//     const char *testAppName;
//     int *ret;
//     for (int i = 1; i <= g_maxGroupNum; i = i + 1)
//     {
//         string src1 = "testApp";
//         string src2 = to_string(i);
//         string src3 = src1 + src2;
//         testAppName = src3.c_str();
//         const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, g_deviceId1, testAppName, VISIBILITY_PUBLIC);
//         ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr);
//         ASSERT_EQ(ret[2] == ON_FINISH, true);
//     }

//     GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
//     ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
//     ASSERT_EQ(ret[2] == ON_ERROR, true);

//     char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
//     for (int j = 0;j < g_maxGroupNum;j++)
//     {
//         const char *groupId = GetGroupIdfromData(groupInfo, j);
//         if (strcmp(groupId, "null groupId") != 0) {
//             char *deletParamsStr = DeleteGroupParams(groupId);
//             DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
//         }
//     }
// }

/**
* @tc.name      Create two peer-to-peer groups. User1 and User2 use the same parameters to create the group
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0104
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0104, TestSize.Level4)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0102");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    ret = CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, g_createParamsStr);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID2, APPNAME, QUERY_ALL_GROUP);
    const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      Create a peer-to-peer group. CreateGroup Mandatory parameters are null
                (osAccountId, requestId, appId, and createParams).
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0201
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0201, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0201");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, nullptr, g_createParamsStr);
    ASSERT_EQ(ret[3] == DEV_ERR_INVALID_PARAMS, true);
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0201");
    const char * createParamsStr3 = CreateGroupParams(GROUP_TYPE_P2P, g_deviceId1, nullptr, VISIBILITY_PUBLIC);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr3);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Example Create a peer-to-peer group with groupType set to -1
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0202
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0202, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0202");
    LOGI("CreateGroup groupType is -1");
    const char * createParamsStr1 = CreateGroupParams(GROUP_TYPE_ERROR, g_deviceId1, GROUPNAME,
                                                        VISIBILITY_PUBLIC);
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr1);
    ASSERT_EQ(ret[1] == DEV_ERR_NULL_PTR, true);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Create a peer-to-peer group, groupVisibility outlier construct
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0203
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0203, TestSize.Level4)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0203");
    int groupVisibility[2] = {0, -1};
    int groupVisibilityError = 9;
    int *ret;
    const char * createParamsStr1;
    for (int i = 0;i < 2;i = i + 1) {
        createParamsStr1 = CreateGroupParams(GROUP_TYPE_P2P, g_deviceId1, GROUPNAME, groupVisibility[i]);
        ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr1);
        ASSERT_EQ(ret[2] == ON_FINISH, true);
        char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
        const char *groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
        char *deletParamsStr = DeleteGroupParams(groupId);
        DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    }
    
    createParamsStr1 = CreateGroupParams(GROUP_TYPE_P2P, g_deviceId1, GROUPNAME, groupVisibilityError);
    ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, createParamsStr1);
    ASSERT_EQ(ret[1] == DEV_ERR_INVALID_PARAMS, true);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}

/**
* @tc.name      Delete the group as User1 and check whether the group is deleted successfully
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0301
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level1
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0301, TestSize.Level1)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0301");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
    char* deleteParams = DeleteGroupParams(GROUP_ID_ERROR);
    ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deleteParams);
    ASSERT_EQ(ret[1] == DEV_ERR_GROUP_NOT_EXIST, true);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
    ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_deleteParams);
    ASSERT_EQ(ret[2] == ON_FINISH, true);
}

/**
* @tc.name      To delete a group, the parameter must be empty
* @tc.number    Security_DevAuth_P2P_GroupManage_Func_0302
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level1
*/
HWTEST_F(GroupManage, Security_DevAuth_P2P_GroupManage_Func_0302, TestSize.Level0)
{
    LOGI("start Security_DevAuth_P2P_GroupManage_Func_0302");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    ASSERT_EQ(ret[2] == ON_FINISH, true);

    char* deleteParamsStrNull = DeleteGroupParams(nullptr);
    ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deleteParamsStrNull);
    ASSERT_EQ(ret[1] == DEV_ERR_JSON_GET, true);
    ASSERT_EQ(ret[2] == ON_ERROR, true);

    ret = DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, nullptr, g_deleteParams);
    ASSERT_EQ(ret[1] == DEV_ERR_JSON_GET, true);
    ASSERT_EQ(ret[2] == ON_ERROR, true);
}
}