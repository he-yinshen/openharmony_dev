/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C" {
#include "common_utils.h"
#include "device_auth_func.h"
#include "device_auth_socket.h"
}


using namespace testing::ext;
using namespace std;
namespace {

int g_sockfd;
char g_localDeviceId[INPUT_UDID_LEN] = { 0 };
const char *g_peerDeviceId;
const char *g_groupId;

const char *g_clientBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412341234\"}}";
const char *g_clientCredential;
const char *g_serverCredential;
const char *g_serverBaseInfo = "{\"groupType\":1,\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":102,"
"\"authCode\":\"1234567812345678123456781234567812345678123456781234567812345678\"}}";

// The parameters of P2P
const char *g_createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);

// The parameters of the same account
const char *g_symmInfo1 = "{\"userId\":\"4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\","
"\"credential\":{\"credentialType\":1,\"authCodeId\":102,\"authCode\":\"10F9F0576E61730193D2052B7F771887124A"
"68F1607EFCF7796C1491F834CD92\"}}";
const char *g_symmInfo2 = "{\"udid\":\"devC\",\"deviceId\":\"devC\",\"userId\":\"4269DC28B639681698809A67EDA"
"D08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":103,\"authCode\":"
"\"1234123412341234123412341234123412341234123412341234123412341233\"}}";

class GetPkInfo : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - GroupManage_P2P */
void GetPkInfo::SetUpTestCase()
{
    SetAccessToken();
    const char *ip = "192.168.1.11";
    static int port = 15051;
    g_sockfd = InitClient(ip, port);
    GetLocalDeviceId(g_localDeviceId);
    g_peerDeviceId = GetPeerDevId(g_sockfd);

    ToServerInit(g_sockfd);
    InitEnv();
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
    TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
    const char *peerDeviceId = GetPeerDevId(g_sockfd);
    const char *authParamsStr = AuthParams(peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
    TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
}

void GetPkInfo::TearDownTestCase()
{
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_P2P_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    Destory();
    ToServerDeleteGroup(g_sockfd);
    ToServerDestroy(g_sockfd);
    ToServerClose(g_sockfd);
    sleep(1);
    CloseClient(g_sockfd);
}

void GetPkInfo::SetUp()
{
}

void GetPkInfo::TearDown()
{
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_GetPkInfo_Func_0101
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GetPkInfo, Security_DevAuth_GetPkInfo_Func_0101, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_GetPkInfo_Func_0101");
    uint32_t returnInfoNum;
    TestGetPkInfo(OS_ACCOUNT_ID1, APPNAME, g_localDeviceId, true, &returnInfoNum);
    printf("InfoNum:%d\n", (int)returnInfoNum);
    ASSERT_EQ(returnInfoNum == (uint32_t)1, true);
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_GetPkInfo_Func_0102
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GetPkInfo, Security_DevAuth_GetPkInfo_Func_0102, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_GetPkInfo_Func_0102");
    uint32_t returnInfoNum;
    TestGetPkInfo(OS_ACCOUNT_ID1, APPNAME, g_localDeviceId, false, &returnInfoNum);
    ASSERT_EQ((int)returnInfoNum == 1, true);
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_GetPkInfo_Func_0103
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GetPkInfo, Security_DevAuth_GetPkInfo_Func_0103, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_GetPkInfo_Func_0103");
    uint32_t returnInfoNum;
    TestGetPkInfo(OS_ACCOUNT_ID1, APPNAME, g_peerDeviceId, true, &returnInfoNum);
    ASSERT_EQ((int)returnInfoNum == 1, true);
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_GetPkInfo_Func_0104
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GetPkInfo, Security_DevAuth_GetPkInfo_Func_0104, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_GetPkInfo_Func_0104");
    uint32_t returnInfoNum;
    TestGetPkInfo(OS_ACCOUNT_ID1, APPNAME, g_peerDeviceId, false, &returnInfoNum);
    ASSERT_EQ((int)returnInfoNum == 1, true);
}

/**
* @tc.name      Not imported credentials Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_GetPkInfo_Func_0105
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GetPkInfo, Security_DevAuth_GetPkInfo_Func_0105, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_GetPkInfo_Func_0105");
    // create account relation by symmetric credentials
    g_clientCredential = CreateLocalCred(g_clientBaseInfo, g_localDeviceId);
    g_serverCredential = CreateLocalCred(g_serverBaseInfo, g_peerDeviceId);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_clientCredential);
    // Notifies the peer to create symmetric credentials
    ToServerCreateGroup(g_sockfd, g_serverCredential);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    const char *createSymmInfo = CreateSymmInfo(g_symmInfo1, g_symmInfo2, g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, createSymmInfo);

    uint32_t returnInfoNum;
    TestGetPkInfo(OS_ACCOUNT_ID1, APPNAME, g_peerDeviceId, false, &returnInfoNum);
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    ToServerDeleteGroup(g_sockfd);
    ToServerDeleteGroup(g_sockfd);
    ASSERT_EQ((int)returnInfoNum == 1, true);
}

/**
* @tc.name      Not imported credentials. Symmetric credentials are used for authentication with the same
                account for the first time
* @tc.number    Security_DevAuth_GetPkInfo_Func_0106
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(GetPkInfo, Security_DevAuth_GetPkInfo_Func_0106, TestSize.Level2)
{
    LOGI("Start Security_DevAuth_GetPkInfo_Func_0106");
    // create account relation by Asymmetric credentials
    const char *reqJsonStr = CreateReqJson(VERSION, g_localDeviceId, USER_ID1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    // Simulate signatures on the cloud side & Create groups
    const char *accoutGroup = CreateAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, pkInfoStr);
    CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, accoutGroup);
    // Obtain the public key of the peer end, simulate the cloud signature, and notify the peer end
    // to create asymmetric credentials
    const char *serverPk = ToGetServerPK(g_sockfd);
    const char *serverParams = CreateCroAccoutCG(REQUEST_ID, OS_ACCOUNT_ID1, USER_ID1, serverPk);
    ToServerCreateGroup(g_sockfd, serverParams);
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);

    const char *importInfo = GetCroPKInfo(g_groupId, GROUP_TYPE_ACCOUNT);
    AddMultiMembersTest(OS_ACCOUNT_ID1, APPNAME, importInfo);
    uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
    ASSERT_EQ(deviceNum == 2, true);

    uint32_t returnInfoNum;
    TestGetPkInfo(OS_ACCOUNT_ID1, APPNAME, g_peerDeviceId, false, &returnInfoNum);
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ACCOUNT_GROUP);
    g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    ASSERT_EQ((int)returnInfoNum == 1, true);
}

}