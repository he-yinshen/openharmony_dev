/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C"
{
#include "device_auth_socket.h"
#include "device_auth_func.h"
#include "device_auth_define.h"
#include "common_utils.h"
}

using namespace testing::ext;
using namespace std;
namespace {
    static const char *g_createParamsStr = "{}";
    static const char *g_groupId;
    static int g_sockfd;
    static char g_deviceId[INPUT_UDID_LEN] = { 0 };
    static const char *g_peerDeviceId;

    class DevauthP2PCancelBind : public testing::Test {
    public:
        static void SetUpTestCase(void);

        static void TearDownTestCase(void);

        void SetUp();

        void TearDown();
    };

    void DevauthP2PCancelBind::SetUpTestCase()
    {
        SetAccessToken();
        const char *ip = "192.168.1.11";
        const int port = 15051;
        g_sockfd = InitClient(ip, port);
        GetLocalDeviceId(g_deviceId);
        g_peerDeviceId = GetPeerDevId(g_sockfd);
        printf("deviceId: %s", g_deviceId);
        ToServerInit(g_sockfd);
        InitEnv();
    }

    void DevauthP2PCancelBind::TearDownTestCase()
    {
        ResetPeerDeviceId();
        ToServerDestroy(g_sockfd);
        ToServerClose(g_sockfd);
        sleep(1);
        CloseClient(g_sockfd);
        DeInitTest();
    }

    void DevauthP2PCancelBind::SetUp()
    {
        g_createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, NULL, GROUPNAME, VISIBILITY_PUBLIC);
        CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
        char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
        g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
    }

    void DevauthP2PCancelBind::TearDown()
    {
        ToServerDeleteGroup(g_sockfd);
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
        FreeTmpStr();
    }


    /**
    * @tc.name      Delete a group. User1 The group is bound to a device
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0501
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2PCancelBind, Security_DevAuth_P2P_DeviceBind_Func_0501, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0501");
        CancelBind(REQUEST_ID_ERROR, APPNAME);
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, NULL);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        FreeTmpStr();
        ASSERT_EQ(deviceNum == 2, true);
    }
    
    /**
    * @tc.name      The pincodes of the initiator and receiver are inconsistent when the device is bound
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0502
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2PCancelBind, Security_DevAuth_P2P_DeviceBind_Func_0502, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0502");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, NULL);
        AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        const char *serverData = RecvData(g_sockfd);
        CancelBind(REQUEST_ID, APPNAME_ERROR);
        GMProcessData(serverData, BIND, g_sockfd);
        serverData = RecvData(g_sockfd);
        GMProcessData(serverData, BIND, g_sockfd);
        serverData = RecvData(g_sockfd);
        GMProcessData(serverData, BIND, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        FreeTmpStr();
        ASSERT_EQ(deviceNum == 2, true);
    }

    /**
    * @tc.name      After the device is bound, the sender normally initiates the packet, and the
    *               receiver returns the packet in the binding process
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0503
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2PCancelBind, Security_DevAuth_P2P_DeviceBind_Func_0503, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0503");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, NULL);
        AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        RecvData(g_sockfd);
        CancelBind(REQUEST_ID, APPNAME);
        ToSerCancelBind(g_sockfd, REQUEST_ID, APPNAME);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
    }

    /**
    * @tc.name      After the device is bound, the sender sends back the packet in the binding process,
    *               and the receiver replies normally
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0504
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2PCancelBind, Security_DevAuth_P2P_DeviceBind_Func_0504, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0504");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, NULL);
        AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        RecvData(g_sockfd);
        CancelBind(REQUEST_ID, APPNAME);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID_NEW, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
    }

}