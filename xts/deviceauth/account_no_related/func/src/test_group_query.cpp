/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <gtest/gtest.h>
#include "hc_log.h"
extern "C" {
#include "device_auth_define.h"
#include "device_auth_func.h"
#include "common_utils.h"
}
using namespace testing::ext;
namespace {
const char *g_appName1 = "TestApp1";
const char *g_normalGroupId = "BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54DC0B";
const char *g_normalGroupId2 = "91393F0147E52CA5FD37DF9FF9C0CDE6D54EFFF7560E02DCF85B49F4A176EAFE";
const char *g_privateGroupId = "5DAEBA9E1AAC902979199D6A8B77367E85A5E1D9F397080FDC7B6B4BBFD4052A";
int32_t g_osAccountNull = 0;

class QueryP2P : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

void QueryP2P::SetUpTestCase()
{
    SetAccessToken();
    PrepareQueryData();
}

void QueryP2P::TearDownTestCase()
{
    InitEnv();
    char *deletParamsStr = DeleteGroupParams(g_normalGroupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    deletParamsStr = DeleteGroupParams(g_privateGroupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
    deletParamsStr = DeleteGroupParams(g_normalGroupId2);
    DoubleCallBack(g_appName1);
    DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, g_appName1, deletParamsStr);
    DeInitTest();
}

void QueryP2P::SetUp()
{
    InitEnv();
}

void QueryP2P::TearDown()
{
    DeInitTest();
}

/**
* @tc.name      Query the specified group user1, appId1 create a public group, and appId2 query the group
* @tc.number    Security_DevAuth_P2P_Query_Func_0101
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0101, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0101");
    char *groupInfo = GetGroupInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, g_normalGroupId);
    int ret = CompareInfo1(groupInfo, GROUP_ID_TEXT, g_normalGroupId);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
    ASSERT_EQ(ret == DEV_SUCCESS, true);
}

/**
* @tc.name      Example Query user1, appId1 Create a private group, and appId2 query the specified group
* @tc.number    Security_DevAuth_P2P_Query_Func_0102
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0102, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0102");
    char *groupInfo = GetGroupInfoByIdTest(OS_ACCOUNT_ID1, g_appName1, g_privateGroupId);
    int ret = CompareInfo1(groupInfo, GROUP_ID_TEXT, g_privateGroupId);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
    ASSERT_EQ(ret != DEV_SUCCESS, true);
}

/**
* @tc.name      Query the specified group user1, appId1 Create the public group, user2 appId1 query the group
* @tc.number    Security_DevAuth_P2P_Query_Func_0103
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0103, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0103");
    char *groupInfo = GetGroupInfoByIdTest(OS_ACCOUNT_ID2, APPNAME, g_normalGroupId);
    int ret = CompareInfo1(groupInfo, GROUP_ID_TEXT, g_normalGroupId);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
    ASSERT_EQ(ret != DEV_SUCCESS, true);
}

/**
* @tc.name      None Example Query the specified group GetGroupInfoById appId, groupId, and osAccountId
* @tc.number    Security_DevAuth_P2P_Query_Func_0104
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level4
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0104, TestSize.Level4)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0104");
    char *groupInfo = GetGroupInfoByIdTest(OS_ACCOUNT_ID1, nullptr, g_normalGroupId);
    int ret = CompareInfo1(groupInfo, GROUP_ID_TEXT, g_normalGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret != DEV_SUCCESS, true);

    groupInfo = GetGroupInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, nullptr);
    ret = CompareInfo1(groupInfo, GROUP_ID_TEXT, g_normalGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret != DEV_SUCCESS, true);

    DestoryTest(groupInfo);
}

/**
* @tc.name      Group query, groupName1 groupOwner1
* @tc.number    Security_DevAuth_P2P_Query_Func_0201
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0201, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0201");
    const char *queryParams = "{\"groupOwner\":\"TestApp\",\"groupName\":\"P2PGroup\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    int ret = CompareInfo(groupInfo, GROUP_ID_TEXT, GROUP_INDEX, g_normalGroupId);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
    ASSERT_EQ(ret == DEV_SUCCESS, true);
}

/**
* @tc.name      Group query, groupName1 groupOwner2
* @tc.number    Security_DevAuth_P2P_Query_Func_0201
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0202, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0202");
    const char *queryParams = "{\"groupOwner\":\"TestApp1\",\"groupName\":\"P2PGroup\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, g_appName1, queryParams);
    int ret = CompareInfo(groupInfo, GROUP_ID_TEXT, GROUP_INDEX, g_normalGroupId2);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
    ASSERT_EQ(ret == DEV_SUCCESS, true);
}

/**
* @tc.name      Example Query the specified group GetGroupInfo appId and osAccountId
* @tc.number    Security_DevAuth_P2P_Query_Func_0203
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0203, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0203");
    const char *queryParams = "{\"groupOwner\":\"TestApp1\"}";
    char *groupInfo = GetGroupInfoTest(g_osAccountNull, APPNAME, queryParams);
    int ret = CompareInfo(groupInfo, GROUP_ID_TEXT, GROUP_INDEX, g_normalGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret != DEV_SUCCESS, true);

    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, nullptr, queryParams);
    ret = CompareInfo(groupInfo, GROUP_ID_TEXT, GROUP_INDEX, g_normalGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret != DEV_SUCCESS, true);

    DestoryTest(groupInfo);
}

/**
* @tc.name      按照类型查询，user1 appId1 256
* @tc.number    Security_DevAuth_P2P_Query_Func_0301
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0301, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0301");
    int type = 256;
    char *groupInfo = GetJoinedGroupsTest(OS_ACCOUNT_ID1, APPNAME, type);
    CompareInfo(groupInfo, GROUP_ID_TEXT, GROUP_INDEX, g_normalGroupId);
    int ret = GetResultItemNum(groupInfo);
    ASSERT_EQ(ret == 3, true);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
}

/**
* @tc.name      When queried by type, the groupType is -1
* @tc.number    Security_DevAuth_P2P_Query_Func_0302
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0302, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0302");
    int type = -1;
    char *groupInfo = GetJoinedGroupsTest(OS_ACCOUNT_ID1, APPNAME, type);
    CompareInfo(groupInfo, GROUP_ID_TEXT, GROUP_INDEX, g_normalGroupId);
    int ret = GetResultItemNum(groupInfo);
    ASSERT_EQ(ret == 0, true);
    printf("data:%d\n", ret);
    DestoryTest(groupInfo);
}

/**
* @tc.name      When queried by type, GetJoinedGroups appId, groupType, and osAccountId are empty
* @tc.number    Security_DevAuth_P2P_Query_Func_0303
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0303, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0303");
    int type = 256;
    char *groupInfo = GetJoinedGroupsTest(OS_ACCOUNT_ID1, nullptr, type);
    int ret = GetResultItemNum(groupInfo);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret == 0, true);
    DestoryTest(groupInfo);
}

/**
* @tc.name      Query the access rights user1, appId1 Create a public group, and query the group by appId2
* @tc.number    Security_DevAuth_P2P_Query_Func_0501
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0501, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0501");
    int ret = CheckAccessToGroupTest(OS_ACCOUNT_ID1, g_appName1, g_normalGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret == DEV_SUCCESS, true);
}

/**
* @tc.name      Query the access permission user1, appId1 Create a private group, and appId2 query the group
* @tc.number    Security_DevAuth_P2P_Query_Func_0502
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0502, TestSize.Level2)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0502");
    int ret = CheckAccessToGroupTest(OS_ACCOUNT_ID1, g_appName1, g_privateGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret == DEV_ERR_ACCESS_DENIED, true);
}

/**
* @tc.name      Query access rights user1, appId1 Create a private group, user2 appId1 query
* @tc.number    Security_DevAuth_P2P_Query_Func_0503
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level3
*/
HWTEST_F(QueryP2P, Security_DevAuth_P2P_Query_Func_0503, TestSize.Level3)
{
    LOGI("start Security_DevAuth_P2P_Query_Func_0503");
    int ret = CheckAccessToGroupTest(OS_ACCOUNT_ID2, APPNAME, g_normalGroupId);
    printf("data:%d\n", ret);
    ASSERT_EQ(ret == DEV_ERR_ACCESS_DENIED, true);
}
}
