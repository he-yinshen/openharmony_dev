/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include "hc_log.h"
extern "C"
{
#include "device_auth_socket.h"
#include "device_auth_func.h"
#include "device_auth_define.h"
#include "common_utils.h"
}

using namespace testing::ext;
using namespace std;
namespace {
    const char *g_createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);
    const char *g_groupId;
    const char *g_authId;

    static int g_sockfd;
    char g_deviceId[INPUT_UDID_LEN] = { 0 };
    class DevauthP2P : public testing::Test {
    public:
        static void SetUpTestCase(void);

        static void TearDownTestCase(void);

        void SetUp();

        void TearDown();
    };

    void DevauthP2P::SetUpTestCase()
    {
        SetAccessToken();
        const char *ip = "192.168.1.11";
        const int port = 15051;
        g_sockfd = InitClient(ip, port);
        sleep(SLEEP_TIME);
        GetLocalDeviceId(g_deviceId);
        printf("deviceId: %s", g_deviceId);
    }

    void DevauthP2P::TearDownTestCase()
    {
        ToServerClose(g_sockfd);
        sleep(1);
        CloseClient(g_sockfd);
    }

    void DevauthP2P::SetUp()
    {
        ToServerInit(g_sockfd);
        InitEnv();
        CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, g_createParamsStr);
        char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
        g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
        sleep(SLEEP_TIME);
    }

    void DevauthP2P::TearDown()
    {
        CancelBind(REQUEST_ID, APPNAME);
        ToSerCancelBind(g_sockfd, REQUEST_ID, APPNAME);
        CancelAuth(REQUEST_ID, APPNAME);
        ToSerCancelAuth(g_sockfd, REQUEST_ID, APPNAME);
        ToServerDeleteGroup(g_sockfd);
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
        ToServerDestroy(g_sockfd);
        Destory();
    }

    /**
    * @tc.name      Bind and unbind a device
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0101
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level0
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0101, TestSize.Level0)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0101");
        printf("start add member");
        printf("g_groupId:%s", g_groupId);
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);

        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
        FreeTmpStr();
        printf("end add member");
        g_authId = GetPeerDevId(g_sockfd);
        printf("g_authId:%s", g_authId);
        const char *authParamsStr = AuthParams(g_authId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
        
        FreeTmpStr();
        // char *deletParamsStr = DeleteParams(g_authId, g_groupId, false);
        // ret = TestCoDelete(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        // deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        // ASSERT_EQ(deviceNum == 1, true);
        // printf("deviceNum:%d\n", deviceNum);
    }

    /**
    * @tc.name      Delete a group. User1 The group is bound to a device
    * @tc.number    Security_DevAuth_P2P_GroupManage_Func_0304
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupManage_Func_0304, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_GroupManage_Func_0304");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        printf("star delete\n");
        char *deletParamsStr = DeleteGroupParams(g_groupId);
        ret = DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr);
        sleep(SLEEP_TIME);
        printf("star delete\n");
        deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 0, true);
    }
    
    /**
    * @tc.name      The pincodes of the initiator and receiver are inconsistent when the device is bound
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0102
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0102, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0102");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE_ERROR, nullptr);
        int *ret = AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        const char *serverData = RecvData(g_sockfd);
        ret = GMProcessData(serverData, BIND, g_sockfd);
        serverData = RecvData(g_sockfd);
        ret = GMProcessData(serverData, BIND, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      After the device is bound, the sender normally initiates the packet, and the
    *               receiver returns the packet in the binding process
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0103
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0103, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0103");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, ERRORFIRSTMESSAGE,
            g_sockfd);
        const char *serverData = RecvData(g_sockfd);
        ret = GMProcessData(serverData, ERRORSECONDMESSAGE, g_sockfd);
        serverData = RecvData(g_sockfd);
        ret = GMProcessData(serverData, ERRORSECONDMESSAGE, g_sockfd);
        // serverData = RecvData(g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 1, true);
        printf("deviceNum:%d\n", deviceNum);
    }

    /**
    * @tc.name      After the device is bound, the sender sends back the packet in the binding process,
    *               and the receiver replies normally
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0104
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_0104, TestSize.Level4)
    {
        const char *clientFirstMessage = "{\"authForm\":0,\"payload\":{\"isoSalt\":\"564C27FF8F64BC9455B7A5AEC1588"
        "33C\",\"peerAuthId\":\"3534344145383930464638363634374343423345443534313242434144444136344538303238443846"
        "4138384343324245433343453030394630453844463331\",\"operationCode\":1,\"seed\":\"67467C5116EA8C5876B89AEE5"
        "47E168E08B9FA46AD0799BA9661075D1EDE4F2A\",\"peerUserType\":0,\"support256mod\":false,\"version\":{\"minVe"
        "rsion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},\"message\":1,\"groupAndModuleVersion\":\"2.0.1\",\"gro"
        "upId\":\"BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54DC0B\",\"groupName\":\"P2PGroup\",\"g"
        "roupOp\":2,\"groupType\":256,\"peerDeviceId\":\"544AE890FF86647CCB3ED5412BCADDA64E8028D8FA88CC2BEC3CE009F0"
        "E8DF31\",\"connDeviceId\":\"544AE890FF86647CCB3ED5412BCADDA64E8028D8FA88CC2BEC3CE009F0E8DF31\",\"appId\":\"com."
        "huawei.security\",\"requestId\":\"90\",\"ownerName\":\"\",\"operCode\":101}";
        SendMess(g_sockfd, clientFirstMessage);
        const char *recvData = RecvData(g_sockfd);
        printf("recvData:%s\n", recvData);
        const char *clientSecondMessage = "{\"payload\":{\"epk\":\"FB88270D0B50CD0B78E9D3AAEE8A69B7B4E3F90C905274E"
        "27E7D24A2D279A221\",\"kcfData\":\"AE8DA21C76C0F0B11F7FB35A1C8701CA448B8C37C17A0D8042144F61CD6A6952\",\"ch"
        "allenge\":\"1749881FB6D54B3C89AFE2F7267DD711\",\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2"
        ".0.26\"}},\"message\":2,\"groupAndModuleVersion\":\"2.0.1\",\"groupId\":\"BC680ED1137A5731F4A5A90B1AACC4"
        "A0A3663F6FC2387B7273EFBCC66A54DC0B\",\"groupName\":\"P2PGroup\",\"groupOp\":2,\"groupType\":256,\"peerDev"
        "iceId\":\"544AE890FF86647CCB3ED5412BCADDA64E8028D8FA88CC2BEC3CE009F0E8DF31\",\"connDeviceId\":\"544AE890F"
        "F86647CCB3ED5412BCADDA64E8028D8FA88CC2BEC3CE009F0E8DF31\",\"appId\":\"com.huawei.security\",\"requestId\":\"90\",\"o"
        "wnerName\":\"\",\"operCode\":101}";
        SendMess(g_sockfd, clientSecondMessage);
        recvData = RecvData(g_sockfd);
        int errorCode = GetErrorCode(recvData);
        ASSERT_EQ(errorCode == DEV_SUCCESS, false);
    }

    /**
    * @tc.name      After the device is bound, the client repeatedly sends the same packet
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0105
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0105, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0105");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int ret = DuplicateMessage(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        printf("ret:%d\n", ret);
        ASSERT_EQ(ret == 0, true);
    }

    /**
    * @tc.name      Bind device: Bind a device twice to the same user
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0107
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0107, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0107");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);

        ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
    }

    /**
    * @tc.name      Bind devices. Device 1 user1 Create a group, and bind the group of User1 under User2
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0108
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0108, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0108");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = AddMemberTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        // ASSERT_EQ(ret[2] == 3, true);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID2, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 0, true);
    }

    /**
    * @tc.name      绑定设备，入参为空
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0201
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0201, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0201");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, nullptr, addParamsStr, BIND, g_sockfd);
        ret = AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, nullptr, BIND, g_sockfd);
        addParamsStr = AddParams(nullptr, GROUP_TYPE_P2P, PINCODE, nullptr);
        ret = AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        // ASSERT_EQ(ret[2] == 3, true);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Bound device, groupId is null
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0202
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0202, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0202");
        printf("start add member");
        const char *addParamsStr = AddParams(NULL, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, g_sockfd);
        printf("bind ret:%d\n", ret[2]);
        // ASSERT_EQ(ret[2] == 3, true);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Bind the device with groupType -1
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0203
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0203, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0203");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_ERROR, PINCODE, nullptr);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
    }

    /**
    * @tc.name      Bound device, processData The mandatory parameter is null
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0204
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0204, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0204");
        printf("start add member");
        const char *data = "{\"\":\"\"}";
        int *ret = GMProcessData(data, BIND, g_sockfd);
        printf("bind ret:%d\n", ret[0]);
        ASSERT_EQ(ret[0] == 12302, true);
    }

    /**
    * @tc.name      Unbind the device and check whether the device is unbound successfully
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0301
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level1
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0301, TestSize.Level1)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0301");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        // int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
        // const char *peerDeviceId = GetPeerDevId(g_sockfd);
        // char *deletParamsStr = DeleteParams(peerDeviceId, g_groupId, false);
        // ret = TestCoDelete(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        // deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        // ASSERT_EQ(deviceNum == 1, true);
        // printf("deviceNum:%d\n", deviceNum);
    }

    /**
    * @tc.name      Unbind the device. Device 1 user1 binds device 2. Unbind device 1 unilaterally,
    *               and device 2 initiates binding
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0302
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0302, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0302");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        ToServerDeleteGroup(g_sockfd);
        ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
    }

    /**
    * @tc.name      Unbind the device. Device 1 user1 Unbind the device bound to user2
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0303
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0303, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0303");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        char *deletParamsStr = DeleteParams(peerDeviceId, g_groupId, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Unbind device. Input parameter is empty --- 待确认
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0401
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0401, TestSize.Level3)
    {
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);

        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        char *deletParamsStr = DeleteParams(nullptr, g_groupId, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
  
        deletParamsStr = DeleteParams(peerDeviceId, nullptr, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);

        deletParamsStr = DeleteParams(peerDeviceId, g_groupId, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, nullptr, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Unbind the device and unbind the non-existent groupId
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0402
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0402, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0402");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);

        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        char *deletParamsStr = DeleteParams(peerDeviceId, GROUP_ID_ERROR, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Unbind the device and unbind the non-existent peerUdid
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0403
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0403, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0403");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);

        char *deletParamsStr = DeleteParams(PEER_DEVICE_ID_ERROR, g_groupId, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Unbind the device. Unbind the local device
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0404
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0404, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0403");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);

        char *deletParamsStr = DeleteParams(g_deviceId, g_groupId, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Unbind the device and negotiate to unbind the device---待确认
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0405
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_DeviceBind_Func_0405, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_DeviceBind_Func_0405");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        char *deletParamsStr = DeleteParams(g_deviceId, GROUP_ID_ERROR, true);
        ret = DeleteMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, deletParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Initiates authentication. The initiator normally initiates authentication,
    *               and the receiver returns the packet during authentication
    * @tc.number    Security_DevAuth_P2P_DeviceBind_Func_0107
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0103, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0103");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        g_authId = GetPeerDevId(g_sockfd);
        printf("g_authId:%s", g_authId);
        const char *authParamsStr = AuthParams(g_authId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, AUTHERRMESSFIRST, g_sockfd);
        const char *recvData = RecvData(g_sockfd);
        ret = ProcessAuthData(recvData, AUTHERRMESSSECOND, g_sockfd);
        recvData = RecvData(g_sockfd);
        ASSERT_EQ(ret[2] != ON_FINISH, true);
    }

    /**
    * @tc.name      Initiating authentication: The sender uses the packets in the previous authentication process,
    *               and the receiver normally processes the packets
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0104
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0104, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0104");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        const char *clientFirstMessage = "{\"authForm\":0,\"payload\":{\"isoSalt\":\"9F2C02114E84E6C0096D04F20906"
        "2EFB\",\"peerAuthId\":\"35343441453839304646383636343743434233454435343132424341444441363445383032384438"
        "464138384343324245433343453030394630453844463331\",\"operationCode\":2,\"seed\":\"97A5170C87CB2025294FD7"
        "B151FA3111E529812EE098BF6CC0D9DD0D9AF7722E\",\"peerUserType\":0,\"pkgName\":\"com.huawei.devicegroupmana"
        "ge\",\"serviceType\":\"BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54DC0B\",\"keyLength\":"
        "32,\"support256mod\":false,\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},\"messag"
        "e\":17,\"groupAndModuleVersion\":\"2.0.1\",\"isDeviceLevel\":false, \"operCode\":210}";
        SendMess(g_sockfd, clientFirstMessage);
        const char *recvData = RecvData(g_sockfd);
        int errorMsg = GetErrorMsg(recvData);
        printf("errorMsg:%d\n", errorMsg);
        ASSERT_EQ(errorMsg == 32896, true);
    }

    /**
    * @tc.name      Initiating authentication: user1 initiates authentication on the trusted devices of user2
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0105
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0105, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0105");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        g_authId = GetPeerDevId(g_sockfd);
        printf("g_authId:%s", g_authId);
        const char *authParamsStr = AuthParams(g_authId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID2, REQUEST_ID, authParamsStr, AUTH, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Initiate authentication. user1 and user2 are bound to the same device to initiate authentication
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0106
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0106, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0106");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        const char *createParamsStr = CreateGroupParams(GROUP_TYPE_P2P, nullptr, GROUPNAME, VISIBILITY_PUBLIC);
        CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, createParamsStr);
        char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, QUERY_ALL_GROUP);
        g_groupId = GetGroupIdfromData(groupInfo, GROUP_INDEX);
        addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        ret = TestBind(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID2, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        g_authId = GetPeerDevId(g_sockfd);
        printf("g_authId:%s", g_authId);
        const char *authParamsStr = AuthParams(g_authId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
        ASSERT_EQ(ret[2] != ON_ERROR, true);
    }

    /**
    * @tc.name      A certification, authReqId/peerConnDeviceId/servicePkgName/isClient is empty
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0201
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0201, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0201");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, nullptr, AUTH, g_sockfd);
        ASSERT_EQ(ret[0] == ON_ERROR, true);
        const char *authParamsStr1 = AuthParams(nullptr, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr1, AUTH, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
        authParamsStr1 = AuthParams(g_authId, nullptr, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr1, AUTH, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      peerConDeviceId does not exist
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0202
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0202, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0202");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        const char *authParamsStr1 = AuthParams(PEER_DEVICE_ID_ERROR, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL,
            nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr1, AUTH, g_sockfd);
        ASSERT_EQ(ret[2] == ON_ERROR, true);
    }

    /**
    * @tc.name      Detach device 1, bind device 2, detach device 1, and initiate authentication
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0203
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0203, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0203");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        double sleepTime = 3.0;
        sleep(sleepTime);
        ToServerDeleteGroup(g_sockfd);

        sleep(sleepTime);
        g_authId = GetPeerDevId(g_sockfd);
        printf("g_authId:%s", g_authId);
        const char *authParamsStr = AuthParams(g_authId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL, nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, AUTH, g_sockfd);
        const char *recvData = RecvData(g_sockfd);
        int errorMsg = GetErrorMsg(recvData);
        printf("errorMsg:%d\n", errorMsg);
        ASSERT_EQ(errorMsg == 32896, true);
    }

    /**
    * @tc.name      peerConnDeviceId is the local device
    * @tc.number    Security_DevAuth_P2P_GroupAuth_Func_0204
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_GroupAuth_Func_0204, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_GroupAuth_Func_0204");
        printf("start add member");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("deviceNum:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 2, true);
        const char *authParamsStr1 = AuthParams(g_deviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL,
                                        nullptr, 0, nullptr);
        ret = AuthDeviceTest(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr1, AUTH, g_sockfd);
        const char *serverData = RecvData(g_sockfd);
        ret = ProcessAuthData(serverData, AUTH, g_sockfd);
        sleep(SLEEP_TIME);
        ASSERT_EQ(ret[2] != ON_FINISH, true);
    }

    /**
    * @tc.name      Query a specified device. No corresponding deviceId/groupId exists for the user to which
    *               the device belongs
    * @tc.number    Security_DevAuth_P2P_Query_Func_0401
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0401, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0401");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
        printf("end add member");
        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        const char *authParamsStr = AuthParams(peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL,
                                    nullptr, 0, nullptr);
        ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
        char *groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, DEVICE_ID_ERROR, g_groupId);
        ASSERT_EQ(groupInfo == nullptr, true);
        groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, peerDeviceId, GROUP_ID_ERROR);
        ASSERT_EQ(groupInfo == nullptr, true);
    }

    /**
    * @tc.name      Example Query the specified group user1. user1 and user2 are bound to the same device
    * @tc.number    Security_DevAuth_P2P_Query_Func_0402
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level2
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0402, TestSize.Level2)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0402");
        int *ret = CreateGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, g_createParamsStr);
        char *groupInfo2 = GetGroupInfoTest(OS_ACCOUNT_ID2, APPNAME, QUERY_ALL_GROUP);
        const char *groupId2 = GetGroupIdfromData(groupInfo2, GROUP_INDEX);
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
        printf("end add member");
        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        const char *authParamsStr = AuthParams(peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL,
                                    nullptr, 0, nullptr);
        ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
        const char *addParamsStr2 = AddParams(groupId2, GROUP_TYPE_P2P, PINCODE, nullptr);
        ret = TestBind(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, addParamsStr2, g_sockfd);
        sleep(30);
        uint32_t deviceNum2 = GetTrustedDevicesTest(OS_ACCOUNT_ID2, APPNAME, groupId2);
        ASSERT_EQ(deviceNum2 == 2, true);
        printf("deviceNum2:%d\n", deviceNum2);
        printf("end add member");
        ret = TestAuth(OS_ACCOUNT_ID2, REQUEST_ID, authParamsStr, g_sockfd);
        printf("ret:%d", ret[2]);
        char *groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, peerDeviceId, g_groupId);
        printf("--------------------------------groupInfo:%s", groupInfo);
        ASSERT_EQ(groupInfo != nullptr, true);
        char *groupInfo3 = GetDeviceInfoByIdTest(OS_ACCOUNT_ID2, APPNAME, peerDeviceId, groupId2);
        printf("--------------------------------groupInfo3:%s", groupInfo3);
        ASSERT_EQ(groupInfo != nullptr, true);
        char *deletParamsStr = DeleteGroupParams(groupId2);
        DeleteGroupTest(OS_ACCOUNT_ID2, REQUEST_ID, APPNAME, deletParamsStr);
    }

    /**
    * @tc.name      Query a specified device. appId1 and appId2 are bound to the same device in user1.
    *               Query the same device in user1 appId1 groupId2
    * @tc.number    Security_DevAuth_P2P_Query_Func_0403
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0403, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0403");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
        printf("end add member");
        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        const char *authParamsStr = AuthParams(peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL,
                                    nullptr, 0, nullptr);
        ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
        sleep(2);
        DoubleCallBack(APPNAME2);
        ToServerReg(g_sockfd, APPNAME2);
        ret = CreateGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME2, g_createParamsStr);

        char *groupInfo2 = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME2, QUERY_ALL_GROUP_Owner2);
        const char *groupId2 = GetGroupIdfromData(groupInfo2, GROUP_INDEX);
        const char *addParamsStr2 = AddParams(groupId2, GROUP_TYPE_P2P, PINCODE, nullptr);
        ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME2, addParamsStr2, g_sockfd);

        sleep(SLEEP_TIME);
        uint32_t deviceNum2 = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME2, groupId2);
        ASSERT_EQ(deviceNum2 == 2, true);
        printf("deviceNum2:%d\n", deviceNum2);
        printf("end add member");
        char *groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, peerDeviceId, g_groupId);
        printf("--------------------------------groupInfo:%s", groupInfo);
        ASSERT_EQ(groupInfo != nullptr, true);
        char *groupInfo3 = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, APPNAME2, peerDeviceId, groupId2);
        printf("--------------------------------groupInfo3:%s", groupInfo3);
        char *deletParamsStr = DeleteGroupParams(groupId2);
        DeleteGroupTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME2, deletParamsStr);
        DoubleCallBack(APPNAME);
    }

    /**
    * @tc.name      Query GetDeviceInfoById by type. appId, groupType, and osAccountId are empty
    * @tc.number    Security_DevAuth_P2P_Query_Func_0404
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0404, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0404");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);

        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        ASSERT_EQ(deviceNum == 2, true);
        printf("deviceNum:%d\n", deviceNum);
        printf("end add member");
        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        const char *authParamsStr = AuthParams(peerDeviceId, APPNAME, IS_CLIENT, NO_DEVICE_LEVEL,
                                    nullptr, 0, nullptr);
        ret = TestAuth(OS_ACCOUNT_ID1, REQUEST_ID, authParamsStr, g_sockfd);
        
        char *groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_NULL, APPNAME, peerDeviceId, g_groupId);
        printf("--------------------------------OS_ACCOUNT_NULL:%s\n", groupInfo);
        ASSERT_EQ(groupInfo == nullptr, true);
        groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, nullptr, peerDeviceId, g_groupId);
        printf("--------------------------------g_appNameNull:%s\n", groupInfo);
        ASSERT_EQ(groupInfo == nullptr, true);
        groupInfo = GetDeviceInfoByIdTest(OS_ACCOUNT_ID1, APPNAME, nullptr, g_groupId);
        printf("--------------------------------g_authIdNull:%s\n", groupInfo);
        ASSERT_EQ(groupInfo == nullptr, true);
    }

    /**
    * @tc.name      Query access permission checkAccessToGroup appId, groupId, and osAccountId are empty
    * @tc.number    Security_DevAuth_P2P_Query_Func_0504
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0504, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0504");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        printf("----------------------------BINDret = %d----------------------------", ret[2]);
        int res = CheckAccessToGroupTest(OS_ACCOUNT_ID1, APPNAME, g_groupId);
        printf("-------------------------normal:%d\n", res);
        ASSERT_EQ(res == 0, true);
        res = CheckAccessToGroupTest(OS_ACCOUNT_ID1, nullptr, g_groupId);
        printf("-------------------------g_appNameNull:%d\n", res);
        ASSERT_EQ(res != 0, true);
        res = CheckAccessToGroupTest(OS_ACCOUNT_ID1, APPNAME, nullptr);
        printf("-------------------------g_groupIdNull:%d\n", res);
        ASSERT_EQ(res != 0, true);
    }

    /**
    * @tc.name      No appId or groupId exists for the user to which a trusted device belongs
    * @tc.number    Security_DevAuth_P2P_Query_Func_0602
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0602, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0602");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        printf("----------------------------BINDret = %d----------------------------", ret[2]);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME_ERROR, GROUP_ID_ERROR);
        ASSERT_EQ(deviceNum == 0, true);
        printf("-------------------------GROUP_ID_ERROR:%d\n", deviceNum);
    }

    /**
    * @tc.name      None Example Query trusted device getTrustDevices appId, groupId, and osAccountId
    * @tc.number    Security_DevAuth_P2P_Query_Func_0603
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0603, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0603");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        printf("----------------------------BINDret = %d----------------------------", ret[2]);
        uint32_t deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, nullptr, g_groupId);
        printf("-------------------------g_groupNameNull:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 0, true);
        deviceNum = GetTrustedDevicesTest(OS_ACCOUNT_ID1, APPNAME, nullptr);
        printf("-------------------------g_groupIdNull:%d\n", deviceNum);
        ASSERT_EQ(deviceNum == 0, true);
    }

    /**
    * @tc.name      None Example Query the IsDeviceInGroup appId, groupId, and osAccountId of a specified device
    * @tc.number    Security_DevAuth_P2P_Query_Func_0604
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level4
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0604, TestSize.Level4)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0604");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);

        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        bool res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, APPNAME, g_groupId, peerDeviceId);
        printf("-------------------------normal:%d\n", res);
        ASSERT_EQ(res == 1, true);
        res = IsDeviceInGroupTest(OS_ACCOUNT_NULL, APPNAME, g_groupId, peerDeviceId);
        printf("-------------------------OS_ACCOUNT_NULL:%d\n", res);
        ASSERT_EQ(res == 0, true);
        res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, nullptr, g_groupId, peerDeviceId);
        printf("-------------------------g_appNameNull:%d\n", res);
        ASSERT_EQ(res == 0, true);
        res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, APPNAME, nullptr, peerDeviceId);
        printf("-------------------------g_groupIdNull:%d\n", res);
        ASSERT_EQ(res == 0, true);
        res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, APPNAME, g_groupId, nullptr);
        printf("-------------------------g_authIdNull:%d\n", res);
        ASSERT_EQ(res == 0, true);
    }

    /**
    * @tc.name      Query the appId, groupId, and peerDeviceId of the user to which the specified device belongs
    * @tc.number    Security_DevAuth_P2P_Query_Func_0605
    * @tc.size      MEDIUM
    * @tc.type      FUNC
    * @tc.level     Level3
    */
    HWTEST_F(DevauthP2P, Security_DevAuth_P2P_Query_Func_0605, TestSize.Level3)
    {
        LOGI("Security_DevAuth_P2P_Query_Func_0605");
        const char *addParamsStr = AddParams(g_groupId, GROUP_TYPE_P2P, PINCODE, nullptr);
        int *ret = TestBind(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, g_sockfd);
        const char *peerDeviceId = GetPeerDevId(g_sockfd);
        printf("----------------------------BINDret = %d----------------------------", ret[2]);
        bool res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, APPNAME_ERROR, g_groupId, peerDeviceId);
        printf("-------------------------normal:%d\n", res);
        ASSERT_EQ(res == 1, true);
        res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, APPNAME, GROUP_ID_ERROR, peerDeviceId);
        printf("-------------------------GROUP_ID_ERROR:%d\n", res);
        ASSERT_EQ(res == 0, true);
        res = IsDeviceInGroupTest(OS_ACCOUNT_ID1, APPNAME, g_groupId, DEVICE_ID_ERROR);
        printf("-------------------------DEVICE_ID_ERROR:%d\n", res);
        ASSERT_EQ(res == 0, true);
    }
}