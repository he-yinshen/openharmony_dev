/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>


#include <pthread.h>
#include <thread>

#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <unistd.h>
extern "C" {
#include "hc_log.h"
#include "device_auth_socket.h"
#include "device_auth_func.h"
}


using namespace testing::ext;
using namespace std;
namespace {

// env
const char *g_ip = "192.168.0.157";
static int g_multNum = 1;
static int g_presNum = 100;
// Basic parameters for creating groups
int32_t g_osAccountId = 100;
int32_t g_requestId = 10;
const char *g_appName = "TestApp";
const char *g_pinCode = "123456";
static int g_groupTypeP2P = 256;
const char *g_connectParams = nullptr;
const char *g_groupName = "TestApp";
static int g_groupVisibilityPublic = -1;
const char *g_queryParams = "{\"groupOwner\":\"TestApp\"}";
const char *g_groupId;
const char *g_authParamsStr;
static int g_ipPort = 15051;
// Parameters for authing device
const char *g_authId;
bool g_isClient = true;
static int g_index = 0;

class P2PMTBF : public testing::Test {
public:
    static void SetUpTestCase(void);

    static void TearDownTestCase(void);

    void SetUp();

    void TearDown();
};

/* test suit - GroupManage_P2P */
void P2PMTBF::SetUpTestCase()
{
    printf("Please input g_multNum: ");
    fflush(stdout);
    scanf_s("%d", &g_multNum);
}

void P2PMTBF::TearDownTestCase()
{
}

void P2PMTBF::SetUp()
{ 
    InitEnv();
    InitResultPool(g_requestId);
    const char *createParamsStr = CreateGroupParams(g_groupTypeP2P, nullptr, g_groupName, g_groupVisibilityPublic);
    CreateGroupTest(g_osAccountId, g_requestId, g_appName ,createParamsStr);
    FreeTmpStr();
    char *groupInfo = GetGroupInfoTest(g_osAccountId, g_appName, g_queryParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
    
}

void P2PMTBF::TearDown()
{
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId, g_requestId, g_appName, deletParamsStr);
    FreeTmpStr();
    Destory();
}

void EXIT(int sig)
{
    printf("程序退出，信号值=%d\n",sig);
    exit(0);
}

void *PthMain(void *arg)
{
    
    int requestId = (long)arg;
    int sockfd = InitClient(g_ip, g_ipPort);
    if (sockfd == false) {
        printf("TcpClient.ConnectToServer(\"172.16.0.15\",5051) failed,exit...\n");
    }
    SetSocketPool(requestId, sockfd);

    for(int i = 0; i < g_presNum; i++)
    {
        LOGI("star auth thread:%d, test num:%d \n", requestId, i);
        int *ret = TestAuth(g_osAccountId, requestId, g_authParamsStr, sockfd);
        printf("ret:%d",ret[MESSAGE]);
    }

    CloseClient(sockfd);
    printf("客户端已断开连接, requestId: %d。\n", requestId);
    pthread_exit(nullptr);
}

/**
* @tc.name      设备认证压测
* @tc.number    Security_DevAuth_P2P_DeviceAuth_MTBF_0101
* @tc.size      MEDIUM
* @tc.type      FUNC
* @tc.level     Level2
*/
HWTEST_F(P2PMTBF, Security_DevAuth_P2P_DeviceAuth_MTBF_0101, TestSize.Level2)
{

    printf("start add member");
    ToServerInit(sockfd);
    const char *addParamsStr = AddParams(g_groupId, g_groupTypeP2P, g_pinCode, g_connectParams);
    int *ret = TestBind(g_osAccountId, g_requestId, g_appName, addParamsStr, sockfd);
    uint32_t deviceNum = GetTrustedDevicesTest(g_osAccountId, g_appName, g_groupId);
    ASSERT_EQ(deviceNum == 2, true);
    printf("deviceNum:%d\n", deviceNum);
    printf("ret:%d", ret[MESSAGE]);
    CloseClient(sockfd);
    g_authId = GetPeerDevId();
    g_authParamsStr = AuthParams(g_authId, g_appName, g_isClient, "nullptr");

    InitResultPool(g_multNum);
    pthread_t thread[g_multNum];
    for (int j = 1; j < g_multNum + 1; j++)
    {
        // 向服务器发起连接请求
        // 创建一线程，与新连接上来的客户端通信
        if (pthread_create(&thread[j], nullptr, PthMain, (void*)((long)j)) != 0)
        {
            printf("创建线程失败, 程序退出。n");
        }
        printf("与客户端通信的线程已创建。\n");
    }
    PrintResult(g_multNum);
}
}