/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <sys/types.h>
#include <dirent.h>
extern "C" {
#include "device_auth.h"
#include "device_auth_define.h"
#include "device_auth_func.h"
}

using namespace testing::ext;
using namespace std;
namespace {
const char *g_appName = "TestApp";
int32_t g_requestId = 123;
int32_t g_osAccountId1 = 100;
static int g_groupTypeP2P = 256;
const char *g_pinCode = "123456";
const char *g_queryParams = "{\"groupOwner\":\"TestApp\"}";
const char *g_connectParams = nullptr;

const char *g_normalGroupId = "BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54DC0B";
const char *g_deviceId1 = "3C58C27533D8";
const char *g_groupName1 = "P2PGroup";
static int g_visibilityPublic = -1;
const char *g_createParamsStr = CreateGroupParams(g_groupTypeP2P, g_deviceId1, g_groupName1, g_visibilityPublic);
const char *g_groupId;
static int g_sockfd = -1;
static int g_index = 0;

void DemoPreparEnv()
{
    CreateGroupTest(g_osAccountId1, g_requestId, g_appName, g_createParamsStr);
    char *groupInfo = GetGroupInfoTest(g_osAccountId1, g_appName, g_queryParams);
    g_groupId = GetGroupIdfromData(groupInfo, g_index);
}

void EcspekeFuzz::SetUp()
{
    SetAccessToken();
    InitEnv();

}

void EcspekeFuzz::TearDown()
{
    char *deletParamsStr = DeleteGroupParams(g_groupId);
    DeleteGroupTest(g_osAccountId1, g_requestId, g_appName, deletParamsStr);
    DeInitTest();
}


/**
 * @tc.name: EcspekeFuzz.Security_DevAuth_P2P_ CreateGroup_0101
 * @tc.desc: Use correct parameterss to createGroup;
 * @tc.type: FUNC
 * @tc.size:Level0
 */
HWTEST_F(EcspekeFuzz, Security_DevAuth_Account_Reliability_0102, TestSize.Level0)
{
    printf("open messager dir\n");
    // 打开目录文件
    DIR *dir = nullptr;
    if ((dir = opendir("/data/test/message")) == nullptr) {
    printf("opendir failed!");
    return;
    }
    struct dirent *entry;
    printf("start read\n");
    while((entry = readdir(dir)) != nullptr)
    {
        printf("read message\n");
        if(strstr(entry->d_name, ".txt"))
        {
            char dirPath[50] = "/data/test/message/";
            printf("%s\n", entry->d_name);
            char buff[2048];
            FILE *fp = nullptr;
            // client 第一发报文 发起绑定
            const char *addParamsStr = AddParams(g_groupId, g_groupTypeP2P, g_pinCode, g_connectParams);
            AddMemberTest(g_osAccountId1, g_requestId, g_appName, addParamsStr, ERRORFIRSTMESSAGE,
                    g_sockfd);
            // server 第一发报文
            const char *serverFirstMessage = "{\"payload\":{\"salt\":\"3AFB3F5165C0E308DD59357F7A508E50\",\"epk\":\"7E3D8F2CCA"
            "5FFB9749F50F62B3EE5C7EA35E0F2E017209F53DA75B8482508B7D\",\"challenge\":\"9075F44D5811F636C476F0348F317B75\","
            "\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},\"message\":32769,"
            "\"groupAndModuleVersion\":\"2.0.1\",\"groupId\":\"BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBC"
            "C66A54DC0B\",\"groupName\":\"P2PGroup\",\"groupOp\":2,\"groupType\":256,\"peerDeviceId\":\"004FB07744533"
            "F00E763EEBB3194F9E183989AAF1502D3C86F31251973CB8F7F\",\"connDeviceId\":\"004FB07744533F00E763EEBB3194F9E"
            "183989AAF1502D3C86F31251973CB8F7F\",\"appId\":\"TestApp\",\"requestId\":\"123\",\"ownerName\":\"\"}";
            // client 处理并回复
            GMProcessData(serverFirstMessage, BIND, g_sockfd);
            // server 第二发报文
            fp = fopen(strcat_s(dirPath, sizeof(entry->d_name), entry->d_name), "r");
            fgets(buff, 2048, (FILE*)fp);
            const char *serverSecondMessage = buff;
            printf("server second message: %s\n", serverSecondMessage );
            // client 处理并回复
            GMProcessData(serverSecondMessage, BIND, g_sockfd);
            fclose(fp);
        }
    }




}

static struct option longOpts[] = {
    { "bind", no_argument, NULL, 'b' },
    { "delete", no_argument, NULL, 'd' },
    { "query", no_argument, NULL, 'q' },
    { "server", no_argument, NULL, 's' },
    { 0, 0, 0, 0 }
};

static int32_t ParseTestParams(int argc, char **argv)
{
    int ch;
    int ret = 0;
    int idx = 0;
    const char *shortOpts = "bdqs";

    while ((ch = getopt_long(argc, argv, shortOpts, longOpts, &idx)) >= 0) {
        switch (ch) {
            case 'b':
                DemoCreateAndBind();
                break;
            case 'd':
                DemoDeleteGroup();
                break;
            case 'q':
                DemoQueryDeviceInfo();
                break;
            case 's':
                DemoServer();
        }
    }
    return ret;
}

int main(int argc, char **argv)
{
    SetAccessToken();
    InitEnv();
    if (ParseTestParams(argc, argv)) {
    return 0;
    }
    return 0;
}

}