/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */
#include "device_auth_func.h"
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/select.h>
// #include <sys/time.h>
#include "time.h"
#include "hc_log.h"
#include "device_auth.h"
#include "securec.h"
#include "json_utils.h"
#include "string_util.h"
#include "hc_tlv_parser.h"
#include "account_module_defines.h"
#include "alg_loader.h"
#include "common_defs.h"
#include <parameter.h>
#include <pthread.h>
#include "hc_condition.h"
#include "device_auth_socket.h"

#define REQUEST_ID_TEXT "requestId"
#define PEERDEVICEID "addId"
#define OS_ACCOUNT_ID "osAccountId"
#define PEERDEVICEIDSERVER "peerAuthId"
#define BUFFER_SIZE 2048

// The Parameters of initialization
static const DeviceGroupManager *g_gmInstance = NULL;
static const GroupAuthManager *g_gaInstance = NULL;
static DeviceAuthCallback g_gmCallback = {NULL};
static DeviceAuthCallback g_gaCallback = {NULL};
static DataChangeListener g_testListener = {NULL};
static HcCondition g_testCondition;
static char *g_tmpStr = NULL;

// The Parameters of group
static int32_t g_requestId = 90;
static int g_accpet = 1;
char g_tempGroupId[128] = "FF9B5A053D74CC8835DB98FAADBCE1A8E4C441637A3A170CB95D4DDD83319C49";

// The Parameters of binding devices
static char g_pinCode[128] = "123456";
static const char *g_peerUdid;
static char g_peerDeviceId[128] = {};
// static const char *g_peerDeviceId;
// static const char *g_localDeviceId;
static char g_duplicateMessage[BUFFER_SIZE];
static int g_receivedMessageNum[7] = {0};
static int g_isAccepted = 1;
// static const char *g_serverAuthId = "CAF34E13190CBA510AA8DABB70CDFF8E9F623656DED400EF0D4CFD9E88FD6202";
static int g_index = 0;

// The parameters of dual-device networking
static int g_socketPool[100];
static int g_resultPool[200];
static int g_operCode[150];
static int g_poolNums = 100;

// Indicates the execution result
static int g_operationCode = -1;
static int g_errorCode = 1;
static int g_messageCode = -1;
static int g_result[4];

// Test suite configuration parameters
static int g_minLength = 2;
static int g_minGroupInfoLength = 10;
static int g_keyLength = 32;
static char g_pkInfoStr[BUFFER_SIZE] = {0};

//Single device simulates authentication with the same account, and temporarily stores authentication data
char g_data[BUFFER_SIZE] = {0};
const char *g_version = "1.0.0";
const char *g_serverDeviceId = "devB";
const char *g_userId1 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4";
const char *g_userId2 = "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3";
const char *g_croPK;
const char *g_queryCroParams = "{\"groupType\":1282,\"groupOwner\":\"com.huawei.security\"}";

// The server has symmetric credentials with the account
const char *g_serverAsymm = "{\"groupType\":1,\"deviceId\":\"devB\",\"userId\":\"4269DC28B639681698809"
"A67EDAD08E39F207900038F91FEF95DD042FE2874E4\",\"credential\":{\"credentialType\":1,\"authCodeId\":101,"
"\"authCode\":\"1234123412341234123412341234123412341234123412341234123412345678\"}}";
static bool g_isClient = true;

static void OnError(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    g_messageCode = ON_ERROR;
    g_requestId = requestId;
    g_operationCode = operationCode;
    g_errorCode = errorCode;
    g_testCondition.notify(&g_testCondition);
}

static void OnFinish(int64_t requestId, int operationCode, const char *returnData)
{
    g_messageCode = ON_FINISH;
    printf("data:%d\n", g_messageCode);
    g_requestId = requestId;
    g_operationCode = operationCode;
    if (operationCode == GROUP_CREATE) {
        CJson *json = CreateJsonFromString(returnData);
        const char *groupId = GetStringFromJson(json, FIELD_GROUP_ID);
        if (memcpy_s(g_data, BUFFER_SIZE, groupId, strlen(groupId)) < 0) {
            printf("memcpy_s failed");
        }
        FreeJson(json);
    }
    g_testCondition.notify(&g_testCondition);
}

static void OnSessionKeyReturned(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    g_messageCode = ON_SESSION_KEY_RETURNED;
    g_requestId = requestId;
}

// Callback for double device authentication
static bool OnTransmitDouble(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    printf("-------------OnTransmit---------------\n");
    printf("|  RequestId: %d  |\n", (int)requestId);
    printf("|  sendData: %s  |\n", (char *)data);

    if (requestId < g_poolNums && g_socketPool[requestId] != -1) {
        CJson *json = CreateJsonFromString((char *)data);
        int64_t tmpReqId = 0;
        if (GetInt64FromJson(json, REQUEST_ID_TEXT, &tmpReqId) != 0) {
            AddInt64StringToJson(json, REQUEST_ID_TEXT, requestId);
        }
        AddIntToJson(json, "operCode", g_operCode[requestId]);
        char *sendData = PackJsonToString(json);
        SendMess(g_socketPool[requestId], sendData);
        printf("finish send ");
        FreeJson(json);
        FreeJsonString(sendData);

        sleep(SLEEP_TIME);
        printf("send size %d, ", (int)dataLen);
    }
    g_messageCode = ON_TRANSMIT;
    g_requestId = requestId;
    g_testCondition.notify(&g_testCondition);

    return true;
}

static bool OnTransmitDupMessDouble(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    printf("-------------OnTransmit---------------\n");
    printf("|  RequestId: %d  |\n", (int)requestId);
    printf("|  sendData: %s  |\n", (char *)data);
    CJson *json = CreateJsonFromString((char *)data);
    int64_t tmpReqId = 0;
    if (GetInt64FromJson(json, REQUEST_ID_TEXT, &tmpReqId) != 0) {
        AddInt64StringToJson(json, REQUEST_ID_TEXT, requestId);
    }
    AddIntToJson(json, "operCode", DUPLICATEMESSAGE);
    char *sendData = PackJsonToString(json);
    if (strcpy_s(g_duplicateMessage, strlen(sendData) + 1, sendData) != 0) {
        printf("strcpy_s fail");
    }
    printf("g_duplicateMessage:%s\n", g_duplicateMessage);
    AddIntToJson(json, "operCode", g_operCode[requestId]);
    sendData = PackJsonToString(json);
    SendMess(g_socketPool[requestId], sendData);
    printf("finish send ");
    FreeJson(json);
    FreeJsonString(sendData);
    sleep(SLEEP_TIME);
    printf("send size %d\n, ", (int)dataLen);
    g_messageCode = ON_TRANSMIT;
    g_requestId = requestId;
    g_testCondition.notify(&g_testCondition);
    return true;
}

static bool OnTransmitDeleteDouble(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    printf("-------------OnTransmit---------------\n");
    printf("|  RequestId: %d  |\n", (int)requestId);
    printf("|  sendData: %s  |\n", (char *)data);
    CJson *json = CreateJsonFromString((char *)data);
    int64_t tmpReqId = 0;
    if (GetInt64FromJson(json, REQUEST_ID_TEXT, &tmpReqId) != 0) {
        AddInt64StringToJson(json, REQUEST_ID_TEXT, requestId);
    }

    AddIntToJson(json, "operCode", g_operCode[requestId]);
    char *sendData = PackJsonToString(json);

    printf("deleteOnTransmitData:%s\n", sendData);
    printf("finish send.\n");
    FreeJson(json);
    FreeJsonString(sendData);

    printf("send size %d, \n", (int)dataLen);
    g_messageCode = ON_TRANSMIT;
    g_requestId = requestId;
    g_testCondition.notify(&g_testCondition);
    return true;
}

static void OnSessionKeyReturnedDouble(int64_t requestId, const uint8_t *sessionKey, uint32_t sessionKeyLen)
{
    printf("-------------OnSessionKeyReturned---------------\n");
    printf("|  RequestId: %d  |\n", (int)requestId);
    printf("|  SessionKey: %-36d  |\n", (int)sessionKeyLen);
    printf("-------------OnSessionKeyReturned---------------\n");
    g_messageCode = ON_SESSION_KEY_RETURNED;
    g_requestId = requestId;
    g_testCondition.notify(&g_testCondition);
}

static void OnFinishDouble(int64_t requestId, int operationCode, const char *authReturn)
{
    printf("-------------OnFinish---------------\n");
    printf("|  RequestId: %-27d  |\n", (int)requestId);
    printf("|  OperationCode: %-23d  |\n", operationCode);
    printf("|  ReturnData: %-26s  |\n", authReturn);
    printf("-------------OnFinish---------------\n");
    if ((operationCode == GROUP_CREATED && g_operCode[requestId] < AUTH) || operationCode == DEVICE_BOUND) {
        CJson *dataJson = CreateJsonFromString(authReturn);
        const char *peerDeviceId = NULL;
        if (dataJson != NULL) {
            peerDeviceId = GetStringFromJson(dataJson, PEERDEVICEID);
            if (peerDeviceId != NULL) {
                FreeJson(dataJson);
                printf("peerDeviceId:%s", g_peerUdid);
            }
        }
    } else {
        CJson *dataJson = CreateJsonFromString(authReturn);
        const char *peerDeviceId = NULL;
        if (dataJson != NULL) {
            peerDeviceId = GetStringFromJson(dataJson, PEERDEVICEIDSERVER);
            if (peerDeviceId != NULL) {
                g_peerUdid = peerDeviceId;
                FreeJson(dataJson);
                printf("peerDeviceId:%s", g_peerUdid);
            }
        }
    }

    g_resultPool[requestId]++;
    printf("peerDeviceId:%s\n", g_peerUdid);
    g_messageCode = ON_FINISH;
    printf("data:%d\n", g_messageCode);
    g_requestId = requestId;
    g_operationCode = operationCode;
    g_testCondition.notify(&g_testCondition);
}

static void OnErrorDouble(int64_t requestId, int operationCode, int errorCode, const char *errorReturn)
{
    printf("-------------OnError---------------\n");
    printf("|  RequestId: %-27d  |\n", (int)requestId);
    printf("|  OperationCode: %-23d  |\n", operationCode);
    printf("|  ErrorCode: %-27d  |\n", errorCode);
    printf("-------------OnError---------------\n");
    g_resultPool[requestId + g_poolNums]++;
    g_messageCode = ON_ERROR;
    g_requestId = requestId;
    g_operationCode = operationCode;
    g_errorCode = errorCode;
    g_testCondition.notify(&g_testCondition);
}

static char *OnBindRequestControllerDouble(int64_t requestId, int operationCode, const char *reqParam)
{
    printf("-------------OnBindRequestController---------------\n");
    printf("|  RequestId: %-29d  |\n", (int)requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParam: %s  |\n", reqParam);
    CJson *json = CreateJson();
    if (g_isAccepted == 1) {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    } else {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_REJECTED);
    }
    char localUdid[INPUT_UDID_LEN] = { 0 };
    GetLocalDeviceId(localUdid);
    AddIntToJson(json, OS_ACCOUNT_ID, OS_ACCOUNT_ID1);
    AddStringToJson(json, FIELD_PIN_CODE, g_pinCode);
    AddStringToJson(json, FIELD_DEVICE_ID, (const char*)localUdid);

    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    printf("|  returnDataStr: %s  |\n", g_tmpStr);
    printf("-------------OnBindRequestController---------------\n");
    return g_tmpStr;
}

static char *OnAuthRequestControllerDouble(int64_t requestId, int operationCode, const char *reqParam)
{
    printf("-------------OnAuthRequestController---------------\n");
    printf("|  RequestId: %-29d  |\n", (int)requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParam: %s  |\n", reqParam);
    printf("-------------OnAuthRequestController---------------\n");
    CJson *json = CreateJson();
    if (g_isAccepted == 1) {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_ACCEPTED);
    } else {
        AddIntToJson(json, FIELD_CONFIRMATION, REQUEST_REJECTED);
    }
    // CJson *dataJson = CreateJsonFromString(reqParam);
    // const char *peerDeviceId = NULL;
    // if (dataJson != NULL) {
    //     peerDeviceId = GetStringFromJson(dataJson, PEERDEVICEIDSERVER);
    // }
    // if (peerDeviceId == NULL) {
    //     peerDeviceId = GetStringFromJson(dataJson, FIELD_DEVICE_ID);
    // }
    // const char *userId = GetStringFromJson(dataJson, FIELD_USER_ID);
    // if (userId != NULL) {
    //     printf("userId:%s\n", userId);
    //     AddStringToJson(json, FIELD_USER_ID, userId);
    // }
    
    AddIntToJson(json, OS_ACCOUNT_ID, OS_ACCOUNT_ID1);
    AddStringToJson(json, FIELD_PEER_CONN_DEVICE_ID, g_peerDeviceId);
    AddStringToJson(json, FIELD_SERVICE_PKG_NAME, APPNAME);

    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    // FreeJson(dataJson);
    return g_tmpStr;
}

// Listener callback
static void OnGroupCreated(const char *groupInfo)
{
    if (groupInfo == NULL) {
        printf("not expected\n");
        return;
    }
    g_receivedMessageNum[GROUP_CREATED]++;
    printf("-------------OnGroupCreated---------------\n");
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnGroupCreated---------------\n");
}

static void OnGroupDeleted(const char *groupInfo)
{
    if (groupInfo == NULL) {
        printf("not expected\n");
        return;
    }
    g_receivedMessageNum[GROUP_DELETED]++;
    printf("-------------OnGroupDeleted---------------\n");
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnGroupDeleted---------------\n");
}

static void OnDeviceBound(const char *peerUdid, const char *groupInfo)
{
    printf("-------------OnDeviceBound---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    g_peerUdid = peerUdid;
    g_receivedMessageNum[DEVICE_BOUND]++;
    printf("GroupInfo:\n%s\n", groupInfo);
    printf("-------------OnDeviceBound---------------\n");
}
static void OnDeviceUnBound(const char *peerUdid, const char *groupInfo)
{
    printf("-------------OnDeviceUnBound---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupInfo:\n%s\n", groupInfo);
    g_receivedMessageNum[DEVICE_UNBOUND]++;
    printf("-------------OnDeviceUnBound---------------\n");
}
static void OnDeviceNotTrusted(const char *peerUdid)
{
    printf("-------------OnDeviceNotTrusted---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    g_receivedMessageNum[DEVICE_NOT_TRUSTED]++;
    printf("-------------OnDeviceNotTrusted---------------\n");
}
static void OnLastGroupDeleted(const char *peerUdid, int groupType)
{
    printf("-------------OnLastGroupDeleted---------------\n");
    printf("PeerUdid:\n%s\n", peerUdid);
    printf("GroupType:\n%d\n", groupType);
    g_receivedMessageNum[LAST_GROUP_DELETED]++;
    printf("-------------OnLastGroupDeleted---------------\n");
}
static void OnTrustedDeviceNumChanged(int curTrustedDeviceNum)
{
    printf("-------------OnTrustedDeviceNumChanged---------------\n");
    printf("CurTrustedDeviceNum:\n%d\n", curTrustedDeviceNum);
    g_receivedMessageNum[TRUSTED_DEVICE_NUM_CHANGED]++;
    printf("-------------OnTrustedDeviceNumChanged---------------\n");
}

//base utils
void FreeTmpStr()
{
    if (!g_tmpStr) {
        sleep(SLEEP_TIME);
        FreeJsonString(g_tmpStr);
        printf("---- release g_tmpStr ----\n");
    } else {
        printf("---- g_tmpStr is NULL\n");
    }
}

// basic function
void InitTest(const char *appId)
{
    InitDeviceAuthService();
    InitHcCond(&g_testCondition, NULL);
    g_gmInstance = GetGmInstance();
    DoubleCallBack(appId);
}

void DeInitTest(void)
{
    DestroyDeviceAuthService();
    ClearTempValue();
    DestroyHcCond(&g_testCondition);
    g_gmInstance = NULL;
}

void ClearTempValue()
{
    g_requestId = 0L;
    g_operationCode = -1;
    g_errorCode = 1;
    g_tmpStr = NULL;
    g_messageCode = -1;
}

int InitEnv()
{
    printf("start to init env.\n");
    InitHcCond(&g_testCondition, NULL);
    InitDeviceAuthService();
    g_gmInstance = GetGmInstance();
    g_gaInstance = GetGaInstance();
    InitListener(APPNAME);
    int ret = DoubleCallBack(APPNAME);
    printf("init callback%d\n", ret);
    g_gaCallback.onError = OnErrorDouble;
    g_gaCallback.onFinish = OnFinishDouble;
    g_gaCallback.onSessionKeyReturned = OnSessionKeyReturnedDouble;
    g_gaCallback.onTransmit = OnTransmitDouble;
    g_gaCallback.onRequest = OnAuthRequestControllerDouble;
    printf("init env success.\n");
    return 0;
}

int DoubleCallBack(const char *appId)
{
    g_gmCallback.onError = OnErrorDouble;
    g_gmCallback.onFinish = OnFinishDouble;
    g_gmCallback.onSessionKeyReturned = OnSessionKeyReturnedDouble;
    g_gmCallback.onTransmit = OnTransmitDouble;
    g_gmCallback.onRequest = OnBindRequestControllerDouble;
    return g_gmInstance->regCallback(appId, &g_gmCallback);
}

void Destory()
{
    ClearTempValue();
    DestroyDeviceAuthService();
    DestroyHcCond(&g_testCondition);
    printf("Destory finished.\n");
}

// mutil thread test
void InitResultPool(int requestId)
{
    g_resultPool[requestId] = 0;
    g_resultPool[requestId + g_poolNums] = 0;
}

void SetSocketPool(int requestId, int sockfd)
{
    g_socketPool[requestId] = sockfd;
}

void PrintResult(int num)
{
    for (int i = 1; i < num + 1; i++) {
        printf("The num of thread is %d, device auth success %d, device auth failed %d\n",
                                        i, g_resultPool[i], g_resultPool[i + g_poolNums]);
    }
}

// Test reg listener
void InitListener(const char *appId)
{
    g_testListener.onGroupCreated = OnGroupCreated;
    g_testListener.onGroupDeleted = OnGroupDeleted;
    g_testListener.onDeviceBound = OnDeviceBound;
    g_testListener.onDeviceUnBound = OnDeviceUnBound;
    g_testListener.onDeviceNotTrusted = OnDeviceNotTrusted;
    g_testListener.onLastGroupDeleted = OnLastGroupDeleted;
    g_testListener.onTrustedDeviceNumChanged = OnTrustedDeviceNumChanged;
    g_gmInstance->regDataChangeListener(appId, &g_testListener);
}

const char *CreateGroupParams(const int groupType, const char *deviceId, const char *groupName,
const int groupVisibility)
{
    CJson *createParams = CreateJson();
    AddIntToJson(createParams, FIELD_GROUP_TYPE, groupType);
    AddStringToJson(createParams, FIELD_GROUP_NAME, groupName);
    AddIntToJson(createParams, FIELD_GROUP_VISIBILITY, groupVisibility);
    char *createParamsStr = PackJsonToString(createParams);
    FreeJson(createParams);
    return createParamsStr;
}

int *CreateGroupTest(int32_t osAccountId, int32_t requestId, const char *groupOwner, const char *createParamsStr)
{
    LOGI("start createGroup ...");
    int ret = g_gmInstance->createGroup(osAccountId, requestId, groupOwner, createParamsStr);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
    }
    sleep(SLEEP_TIME);
    g_result[OPERATION] = g_operationCode;
    g_result[ERROR] = g_errorCode;
    g_result[MESSAGE] = g_messageCode;
    g_result[EXCUTE] = ret;
    printf("g_messageCode%d\n", g_result[MESSAGE]);
    printf("ret:%d\n", g_result[EXCUTE]);
    return g_result;
}

char *DeleteGroupParams(const char *groupId)
{
    CJson *deleteParams = CreateJson();
    AddStringToJson(deleteParams, "groupId", groupId);
    char *deleteParamsStr = PackJsonToString(deleteParams);
    FreeJson(deleteParams);
    return deleteParamsStr;
}

int *DeleteGroupTest(int32_t osAccountId, int32_t requestId, const char *groupOwner, const char *disbandParams)
{
    LOGI("start deletGroup ...");
    int ret = g_gmInstance->deleteGroup(osAccountId, requestId, groupOwner, disbandParams);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
    }
    g_result[OPERATION] = g_operationCode;
    g_result[ERROR] = g_errorCode;
    g_result[MESSAGE] = g_messageCode;
    g_result[EXCUTE] = ret;
    printf("g_messageCode:%d\n", g_result[MESSAGE]);
    printf("ret:%d\n", ret);
    return g_result;
}

// Test addmember devices
char *AddParams(const char *groupId, const int groupType, const char *pincode, const char *connectParams)
{
    CJson *data = CreateJson();
    AddStringToJson(data, FIELD_GROUP_ID, groupId);
    AddIntToJson(data, FIELD_GROUP_TYPE, groupType);
    AddStringToJson(data, FIELD_PIN_CODE, pincode);
    AddStringToJson(data, FIELD_CONNECT_PARAMS, connectParams);
    AddBoolToJson(data, FIELD_IS_ADMIN, g_isClient);
    if (!g_isClient) {
        AddStringToJson(data, FIELD_GROUP_NAME, GROUPNAME);
    } 
    g_tmpStr = PackJsonToString(data);
    FreeJson(data);
    printf("data:%s\n", g_tmpStr);
    return g_tmpStr;
}

int *AddMemberTest(int32_t osAccountId, int64_t requestId, const char *appId, const char *authParams, int code,
    int sockfd)
{
    if (requestId < g_poolNums) {
        g_operCode[requestId] = code;
        g_socketPool[requestId] = sockfd;
    }
    LOGI("start addMem ...\n");
    int ret = g_gmInstance->addMemberToGroup(osAccountId, requestId, appId, authParams);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
        sleep(SLEEP_TIME);
    }
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("MESSAGE:%d\n", g_result[MESSAGE]);
    return g_result;
}

int *GMProcessData(const char *data, int code, int sockfd)
{
    int64_t requestId = 10;
    GetReqIdFromData(&requestId, data);
    LOGI("requestId is: %lld\n", requestId);
    if (requestId < g_poolNums) {
        g_operCode[requestId] = code;
        g_socketPool[requestId] = sockfd;
    }
    printf("start process GM Data ...\n");
    int ret = g_gmInstance->processData(requestId, (uint8_t *)data, strlen(data) + 1);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
        sleep(SLEEP_TIME);
    }
    printf("ret:%d", ret);
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("%d", g_result[MESSAGE]);
    return g_result;
}

int *TestBind(int32_t osAccountId, int64_t requestId, const char *appId, const char *authParams, int sockfd)
{
    int *ret = AddMemberTest(osAccountId, requestId, appId, authParams, BIND, sockfd);
    const char *serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, BIND, sockfd);
    serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, BIND, sockfd);
    serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, BIND, sockfd);
    sleep(1);
    FreeTmpStr();
    return ret;
}

void CancelBind(int32_t requestId, const char *appId)
{
    g_gmInstance->cancelRequest(requestId, appId);
}

void ReturnFirstMessage(int sockfd)
{
    const char *serverFirstMessage = "{\"payload\":{\"salt\":\"3AFB3F5165C0E308DD59357F7A508E50\",\"epk\":\"7E3D8F2CCA"
    "5FFB9749F50F62B3EE5C7EA35E0F2E017209F53DA75B8482508B7D\",\"challenge\":\"9075F44D5811F636C476F0348F317B75\","
    "\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},\"message\":32769,"
    "\"groupAndModuleVersion\":\"2.0.1\",\"groupId\":\"BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54D"
    "C0B\",\"groupName\":\"P2PGroup\",\"groupOp\":2,\"groupType\":256,\"peerDeviceId\":\"004FB07744533F00E763EEBB3194F"
    "9E183989AAF1502D3C86F31251973CB8F7F\",\"connDeviceId\":\"004FB07744533F00E763EEBB3194F9E183989AAF1502D3C86F312519"
    "73CB8F7F\",\"appId\":\"com.huawei.security\",\"requestId\":\"10\",\"ownerName\":\"\"}";
    SendMess(sockfd, serverFirstMessage);
}

void ReturnSecondMessage(int sockfd)
{
    const char *serverSecondMessage = "{\"payload\":{\"kcfData\":\"3550BE669F8ACD172087A7F62B146725D5EB70231CF8666A28"
    "AB301CD10D7123\",\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},\"message\":32770,\"group"
    "AndModuleVersion\":\"2.0.1\",\"groupId\":\"BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54DC0B\","
    "\"groupName\":\"P2PGroup\",\"groupOp\":2,\"groupType\":256,\"peerDeviceId\":\"004FB07744533F00E763EEBB3194F9E183"
    "989AAF1502D3C86F31251973CB8F7F\",\"connDeviceId\":\"004FB07744533F00E763EEBB3194F9E183989AAF1502D3C86F31251973CB"
    "8F7F\",\"appId\":\"com.huawei.security\",\"requestId\":\"10\",\"ownerName\":\"\"}";
    SendMess(sockfd, serverSecondMessage);
}

int DuplicateMessage(int32_t osAccountId, int64_t requestId, const char *appId, const char *authParams, int sockfd)
{
    g_gmCallback.onTransmit = OnTransmitDupMessDouble;
    g_gmInstance->regCallback(APPNAME, &g_gmCallback);
    int *ret = AddMemberTest(osAccountId, requestId, appId, authParams, BIND, sockfd);
    printf("ret:%d\n", ret[MESSAGE]);
    const char *recvData = RecvData(sockfd);
    SendMess(g_socketPool[requestId], g_duplicateMessage);
    recvData = RecvData(sockfd);
    CJson *data = CreateJsonFromString(recvData);
    int result;
    GetIntFromJson(data, "serverResult", &result);
    FreeJson(data);
    return result;
}

void DoDuplicateMessage(int64_t requestId, const char *data, int code, int sockfd)
{
    g_operCode[requestId] = code;
    g_socketPool[requestId] = sockfd;
    LOGI("start process GM Data ...");
    int ret = g_gmInstance->processData(requestId, (uint8_t *)data, strlen(data) + 1);
    CJson *json = CreateJson();
    AddIntToJson(json, "serverResult", ret);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
}

// Test device auth
char *AuthParams(const char *peerConnDeviceId, const char *pkgName, bool isClient, bool level,
    const char *groupId, int keyLength, const char *userId)
{
    CJson *data = CreateJson();
    if (peerConnDeviceId != NULL) {
        AddStringToJson(data, FIELD_PEER_CONN_DEVICE_ID, peerConnDeviceId);
    }
    AddStringToJson(data, FIELD_SERVICE_PKG_NAME, pkgName);
    AddBoolToJson(data, FIELD_IS_CLIENT, isClient);
    AddBoolToJson(data, FIELD_IS_DEVICE_LEVEL, level);
    if (groupId != NULL) {
        AddStringToJson(data, FIELD_GROUP_ID, groupId);
    }
    if (keyLength != 0) {
        AddIntToJson(data, FIELD_KEY_LENGTH, keyLength);
    }
    if (userId != NULL) {
        AddStringToJson(data, FIELD_USER_ID, userId);
    }
    g_tmpStr = PackJsonToString(data);
    FreeJson(data);
    return g_tmpStr;
}

char *AuthParamsError(const char *peerConnDeviceId, const char *pkgName, bool isClient, bool level, const char *groupId,
    int keyLength, const char *userId)
{
    CJson *data = CreateJson();
    if (peerConnDeviceId != NULL) {
        AddStringToJson(data, FIELD_PEER_CONN_DEVICE_ID, peerConnDeviceId);
    }
    AddStringToJson(data, FIELD_SERVICE_PKG_NAME, pkgName);
    AddBoolToJson(data, FIELD_IS_DEVICE_LEVEL, level);
    if (groupId != NULL) {
        AddStringToJson(data, FIELD_GROUP_ID, groupId);
    }
    if (keyLength != 0) {
        AddIntToJson(data, FIELD_KEY_LENGTH, keyLength);
    }
    if (userId != NULL) {
        AddStringToJson(data, FIELD_USER_ID, userId);
    }
    g_tmpStr = PackJsonToString(data);
    FreeJson(data);
    return g_tmpStr;
}

int *AuthDeviceTest(int32_t osAccountId, int64_t requestId, const char *authParams, int code, int sockfd)
{
    g_operCode[requestId] = code;
    g_socketPool[requestId] = sockfd;
    LOGI("start AuthDevice ... requestId:%ld\n", requestId);
    int ret = g_gaInstance->authDevice(osAccountId, requestId, authParams, &g_gaCallback);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
        sleep(1);
    }
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("auth end\n");
    // FreeTmpStr();
    return g_result;
}

int *ProcessAuthData(const char *data, int code, int sockfd)
{
    int64_t requestId = 10;
    GetReqIdFromData(&requestId, data);
    g_operCode[requestId] = code;
    g_socketPool[requestId] = sockfd;
    LOGI("start process auth data ...requestId:%ld", requestId);
    int ret = g_gaInstance->processData(requestId, (uint8_t *)data, strlen(data) + 1, &g_gaCallback);
    // double sleepTime = 1.0;
    // WaitTime(sleepTime);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
        sleep(1);
        
    }
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("ProcessAuthData OPERATION ret:%d\n", g_result[OPERATION]);
    printf("ProcessAuthData MESSAGE ret:%d\n", g_result[MESSAGE]);
    printf("Process end\n");
    return g_result;
}

int *TestAuth(int32_t osAccountId, int64_t requestId, const char *authParams, int sockfd)
{
    int *ret = AuthDeviceTest(osAccountId, requestId, authParams, AUTH, sockfd);
    const char *serverData = RecvData(sockfd);
    ret = ProcessAuthData(serverData, AUTH, sockfd);
    serverData = RecvData(sockfd);
    ret = ProcessAuthData(serverData, AUTH, sockfd);
    sleep(1);
    FreeTmpStr();
    return ret;
}

void CancelAuth(int32_t requestId, const char *appId)
{
    g_gaInstance->cancelRequest(requestId, appId);
}

void ReturnFirstAuthMessage(int sockfd)
{
    const char *serverFirstMessage = "{\"authForm\":0,\"payload\":{\"salt\":\"870C7A62DE79F45BC7BE1922806F31D9\","
    "\"epk\":\"BAF848B091DA4FA319FF609CB1E70DD48337FE91CD7B3EE9EF27CA2884CD6A5E\",\"nonce\":\"B4CCECC8E23970986CC27"
    "BBD702D726301DF2AD6BC4823B0E16F999C1EFE0E06\",\"peerUserType\":0,\"challenge\":\"F1C921D1DDD3673BFABB0EB6294C3"
    "E60\",\"peerAuthId\":\"303034464230373734343533334630304537363345454242333139344639453138333938394141463135303"
    "24433433836463331323531393733434238463746\",\"version\":{\"minVersion\":\"1.0.0\","
    "\"currentVersion\":\"2.0.26\"}},\"message\":32785,\"groupAndModuleVersion\":\"2.0.1\",\"isDeviceLevel\":false}";
    SendMess(sockfd, serverFirstMessage);
}

void ReturnSecondAuthMessage(int sockfd)
{
    const char *serverSecondMessage = "{\"authForm\":0,\"payload\":{\"kcfData\":\"26D3FC1F63492E415424F10D91DBD6E52"
    "DAEB2BFBA3DA87B7C6E16EF331092C1\",\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},"
    "\"message\":32786,\"groupAndModuleVersion\":\"2.0.1\"}";

    SendMess(sockfd, serverSecondMessage);
}

void AuthAttack(int64_t requestId, const char *data, int code, int sockfd)
{
    g_operCode[requestId] = code;
    g_socketPool[requestId] = sockfd;
    LOGI("start process auth data ...");
    int ret = g_gaInstance->processData(requestId, (uint8_t *)data, strlen(data) + 1, &g_gaCallback);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
    }
}

// Test query function
void PrepareQueryData()
{
    InitEnv();
    const char *createParamsStr = "{\"groupType\":256,\"deviceId\":\"3C58C27533D8\",\""
                                  "groupVisibility\":-1,\"groupName\":\"P2PGroup\"}";
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, createParamsStr);
    const char *createParamsStr1 = "{\"groupType\":256,\"deviceId\":\"3C58C27533D8\",\""
                                   "groupVisibility\":0,\"groupName\":\"P2PGroup1\"}";
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, createParamsStr1);
    const char *createParamsStr2 = "{\"groupType\":256,\"deviceId\":\"3C58C27533D8\",\""
                                   "groupVisibility \":-1,\"groupName\":\"P2PGroup\"}";
    DoubleCallBack(APPNAME2);
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME2, createParamsStr2);
    Destory();
}

int CompareInfo(char *groupVec, const char *key, const int index, const char *compareStr)
{
    LOGI("get groupId");
    printf("groupVec:%s\n", groupVec);
    if (groupVec != NULL) {
        if ((int)strlen(groupVec) > g_minGroupInfoLength) {
            printf("1031\n");
            CJson *groupVecJson = CreateJsonFromString(groupVec);
            CJson *groupInfo = GetItemFromArray(groupVecJson, index);
            LOGI("print json");
            char *jsondata = cJSON_Print(groupInfo);
            printf("data:%s\n", jsondata);
            LOGI("get groupId from json");
            const char *groupId = GetStringFromJson(groupInfo, key);
            int ret;
            if (groupId != NULL) {
                ret = strcmp(groupId, compareStr);
                FreeJson(groupVecJson);
                return ret;
            } else {
                FreeJson(groupVecJson);
                return -1;
            }
        } else {
            return -1;
        }
    } else {
        return -1;
    }
}

int GetResultItemNum(char *groupVec)
{
    LOGI("get groupId");
    CJson *groupVecJson = CreateJsonFromString(groupVec);
    return GetItemNum(groupVecJson);
}

int CompareInfo1(char *groupVec, const char *key, const char *compareStr)
{
    LOGI("get groupId");
    CJson *groupVecJson = CreateJsonFromString(groupVec);
    LOGI("print json");
    printf("data:%s\n", groupVec);
    LOGI("get groupId from json");
    const char *groupId = GetStringFromJson(groupVecJson, key);
    int ret;
    if (groupId != NULL) {
        ret = strcmp(groupId, compareStr);
        FreeJson(groupVecJson);
        return ret;
    } else {
        FreeJson(groupVecJson);
        return -1;
    }
}

void DestoryTest(char *returnInfo)
{
    g_gmInstance->destroyInfo(&returnInfo);
}

char *GetGroupInfoByIdTest(int32_t osAccountId, const char *appId, const char *groupId)
{
    char *returnGroupInfo = NULL;
    g_gmInstance->getGroupInfoById(osAccountId, appId, groupId, &returnGroupInfo);
    printf("groupInfo:%s", returnGroupInfo);
    return returnGroupInfo;
}

char *GetGroupInfoTest(int32_t osAccountId, const char *groupOwner, const char *queryParams)
{
    uint32_t num = 0;
    LOGI("start getGroupInfo");
    char *groupVec = NULL;
    g_gmInstance->getGroupInfo(osAccountId, groupOwner, queryParams, &groupVec, &num);
    return groupVec;
}

char *GetJoinedGroupsTest(int32_t osAccountId, const char *appId, int groupType)
{
    uint32_t num = 0;
    LOGI("start getJoinedGroups");
    char *groupVec = NULL;
    g_gmInstance->getJoinedGroups(osAccountId, appId, groupType, &groupVec, &num);
    return groupVec;
}

char *GetRelatedGroupsTest(int32_t osAccountId, const char *appId, const char *peerDeviceId)
{
    uint32_t num = 0;
    LOGI("start getRelatedGroups");
    char *groupVec = NULL;
    g_gmInstance->getRelatedGroups(osAccountId, appId, peerDeviceId, &groupVec, &num);
    return groupVec;
}

char *GetDeviceInfoByIdTest(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId)
{
    char *returnDeviceInfo = NULL;
    int res = g_gmInstance->getDeviceInfoById(osAccountId, appId, deviceId, groupId, &returnDeviceInfo);
    if (res == 0) {
        printf("[RESULT][GetDeviceInfoById]: SUCCESS\n");
        printf("Return device infos: %s\n", returnDeviceInfo);
    }
    return returnDeviceInfo;
}

uint32_t GetTrustedDevicesTest(int32_t osAccountId, const char *appId, const char *groupId)
{
    char *returnDevices = NULL;
    uint32_t resultNum = 0;
    int res = g_gmInstance->getTrustedDevices(osAccountId, appId, groupId, &returnDevices, &resultNum);
    printf("res:%d\n", res);
    if (res == 0) {
        printf("[RESULT][GetTrustedDevices]: SUCCESS\n");
        printf("Return device infos: %s\n", returnDevices);
        printf("Devices count: %d\n", (int)resultNum);
        g_gmInstance->destroyInfo(&returnDevices);
    }
    printf("resultNum:%u\n", resultNum);
    LOGI("trusted device number: %d\n", resultNum);
    return resultNum;
}

bool IsDeviceInGroupTest(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId)
{
    return g_gmInstance->isDeviceInGroup(osAccountId, appId, groupId, deviceId);
}

int CheckAccessToGroupTest(int32_t osAccountId, const char *appId, const char *groupId)
{
    return g_gmInstance->checkAccessToGroup(osAccountId, appId, groupId);
}

int64_t GetReqIdFromData(int64_t *reqId, const char *data)
{
    CJson *json = CreateJsonFromString(data);
    // int64_t reqId = 10;
    GetInt64FromJson(json, REQUEST_ID_TEXT, reqId);
    FreeJson(json);
    return *reqId;
}

int GetOperCodeFromData(const char *data)
{
    CJson *json = CreateJsonFromString(data);
    int code;
    GetIntFromJson(json, "operCode", &code);
    FreeJson(json);
    return code;
}

const char *GetGroupIdfromData(char *groupVec, int index)
{
    LOGI("get groupId");
    printf("data:%s\n", groupVec);
// if (groupVec != NULL) {
    // if ((int)strlen(groupVec) > g_minLength) {
    if (strcmp(groupVec, "[]") != 0) {    
        printf("groupVec:%d\n",(int)strlen(groupVec));
        CJson *groupVecJson = CreateJsonFromString(groupVec);
        CJson *groupInfo = GetItemFromArray(groupVecJson, index);
        char *jsondata = cJSON_Print(groupInfo);
        printf("data:%s\n", jsondata);
        LOGI("get groupId from json");
        const char *groupId = GetStringFromJson(groupInfo, FIELD_GROUP_ID);
        if (strcpy_s(g_tempGroupId, strlen(groupId) + 1, groupId) != 0) {
            printf("strcpy_s fail");
        }
        printf("groupId:%s\n", g_tempGroupId);
        FreeJson(groupVecJson);
        FreeJsonString(jsondata);
        printf("groupId:%s\n", g_tempGroupId);
        return g_tempGroupId;
    } else {
        return "null groupId";
    }
    // } else {
    //     return "null groupId";
    // }
}

int GetErrorCode(const char *data)
{
    CJson *createParams = CreateJsonFromString(data);
    CJson *tmp = GetObjFromJson(createParams, "payload");
    int errorCode;
    if (tmp !=NULL) {
        GetIntFromJson(tmp, "errorCode", &errorCode);
    } else {
        GetIntFromJson(createParams, "errorCode", &errorCode);
    }
    FreeJson(createParams);
    return errorCode;
}

int GetErrorMsg(const char *data)
{
    CJson *createParams = CreateJsonFromString(data);
    int errorCode;
    GetIntFromJson(createParams, "groupErrorMsg", &errorCode);
    FreeJson(createParams);
    return errorCode;
}

static void writePeerDevice(const char *data)
{
    CJson *json = CreateJsonFromString(data);
    const char *tmp = GetStringFromJson(json, "deviceId");
    printf("----- deviceId: %s -----\n", tmp);
    if (strcpy_s(g_peerDeviceId, strlen(tmp) + 1, tmp) != 0) {
        printf("memcpy_s failed");
    }
    FreeJson(json);
}

const char *GetPeerDevId(int sockfd)
{
    printf("----- length: %d -----\n", strlen(g_peerDeviceId));
    if (strcmp(g_peerDeviceId, "") == 0 || strlen(g_peerDeviceId) < 30){
        CJson *json = CreateJson();
        char localUdid[INPUT_UDID_LEN] = { 0 };
        GetLocalDeviceId(localUdid);
        AddStringToJson(json, "deviceId", localUdid);
        AddIntToJson(json, "operCode", GETPEERDEVICEID);
        char *sendData = PackJsonToString(json);
        SendMess(sockfd, sendData);
        FreeJson(json);
        FreeJsonString(sendData);
        const char *recvData = RecvData(sockfd);
        CJson *data = CreateJsonFromString(recvData);
        const char *tmp = GetStringFromJson(data, "deviceId");
        printf("----- deviceId: %s -----\n", tmp);
        if (strcpy_s(g_peerDeviceId, strlen(tmp) + 1, tmp) != 0) {
            printf("memcpy_s failed");
        }
        FreeJson(data);
    }
    const char *peerDeviceId = g_peerDeviceId;
    return peerDeviceId;
}

void ResetPeerDeviceId()
{
    if (strcpy_s(g_peerDeviceId, strlen("") + 1, "") != 0) {
            printf("memcpy_s failed");
        }
    // g_peerDeviceId = NULL;
}

void SendDeviceId(int sockfd)
{
    CJson *json = CreateJson();
    char localUdid[INPUT_UDID_LEN] = { 0 };
    GetLocalDeviceId(localUdid);
    AddStringToJson(json, "deviceId", localUdid);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
}

int GetLocalDeviceId(char *udid)
{
    int ret = GetDevUdid(udid, INPUT_UDID_LEN);
    return ret;
}

char *DeleteParams(const char *peerAuthId, const char *groupId, bool isForceDelete)
{
    CJson *json = CreateJson();
    AddStringToJson(json, FIELD_DELETE_ID, peerAuthId);
    AddStringToJson(json, FIELD_GROUP_ID, groupId);
    AddBoolToJson(json, FIELD_IS_FORCE_DELETE, isForceDelete);
    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    return g_tmpStr;
}
int *DeleteMemberTest(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams, int sockfd)
{
    g_operCode[requestId] = BIND;
    g_socketPool[requestId] = sockfd;
    LOGI("start process delete member ...");
    int ret = g_gmInstance->deleteMemberFromGroup(osAccountId, requestId, appId, deleteParams);
    sleep(SLEEP_TIME);
    if (ret == 0) {
        g_testCondition.wait(&g_testCondition);
    }
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    return g_result;
}

int *TestCoDelete(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams, int sockfd)
{
    int *ret = DeleteMemberTest(osAccountId, requestId, appId, deleteParams, sockfd);
    const char *serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, DELETE, sockfd);
    serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, DELETE, sockfd);
    serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, DELETE, sockfd);
    sleep(1);
    FreeTmpStr();
    return ret;
}

const char *CreateReqJson(const char *version, const char *deviceId, const char *userId)
{
    CJson *json = CreateJson();
    printf("start to get register info\n");
    AddStringToJson(json, "version", version);
    AddStringToJson(json, FIELD_DEVICE_ID, deviceId);
    AddStringToJson(json, FIELD_USER_ID, userId);
    AddIntToJson(json, "credentialType", ASYMMETRIC_CRED);

    g_tmpStr = PackJsonToString(json);
    printf("reqJsonStr: %s\n", g_tmpStr);
    FreeJson(json);
    return g_tmpStr;
}

const char *TestGetRegisterInfo(const char *reqJsonStr)
{
    char *returnJsonStr = NULL;
    int res = g_gmInstance->getRegisterInfo(reqJsonStr, &returnJsonStr);
    if (res == 0) {
        if (memcpy_s(g_pkInfoStr, BUFFER_SIZE, "0", strlen("0")) == 0) {
            printf("memcpy_s 0 failed");
        }
        if (memcpy_s(g_pkInfoStr, BUFFER_SIZE, returnJsonStr, strlen(returnJsonStr) + 1) != 0) {
            printf("memcpy_s g_pkInfoStr failed");
        }
        printf("get register info success\nregister info: %s\n", returnJsonStr);
        g_gmInstance->destroyInfo(&returnJsonStr);
    } else {
        printf("get register info fail\nret: %d\n", res);
    }
    const char *pkInfoStr = g_pkInfoStr;
    return pkInfoStr;
}

const char *FixPkInfoStr(const char *pkInfoStr)
{
    CJson *json = CreateJsonFromString(pkInfoStr);
    AddStringToJson(json, FIELD_USER_ID, "123456");
    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    return g_tmpStr;
}

const char *GetCroPKInfo(const char *groupId, int groupType)
{
    const char *tempStr = CreateImportData(g_croPK, groupId, groupType);
    return tempStr;
}

const char *CredentialInfo(const char *pkInfoStr, Uint8Buff serverPk, Uint8Buff signature)
{
    CJson *credJson = CreateJson();
    (void)AddByteToJson(credJson, "serverPk", serverPk.val, serverPk.length);
    (void)AddByteToJson(credJson, "pkInfoSignature", signature.val, signature.length);
    CJson *pkInfoJson = CreateJsonFromString(pkInfoStr);
    (void)AddObjToJson(credJson, "pkInfo", pkInfoJson);
    FreeJson(pkInfoJson);
    (void)AddIntToJson(credJson, "credentialType", ASYMMETRIC_CRED);
    g_tmpStr = PackJsonToString(credJson);
    printf("credentialStr: %s\n", g_tmpStr);
    FreeJson(credJson);
    return g_tmpStr;
}

const char *SignPkInfo(int64_t requestId, int osAccountId, const char *pkInfoStr)
{
    const char *keyAliasValue = "TestKeyPair";
    Uint8Buff keyAlias = {
        .val = (uint8_t *)keyAliasValue,
        .length = strlen(keyAliasValue) + 1
    };
    int ret = GetLoaderInstance()->checkKeyExist(&keyAlias);
    if (ret != HC_SUCCESS) {
        LOGI("Key pair not exist, start to generate");
        int32_t authId = 0;
        Uint8Buff authIdBuff = { (uint8_t *)&authId, sizeof(int32_t) };
        ExtraInfo extInfo = { authIdBuff, -1, -1 };
        ret = GetLoaderInstance()->generateKeyPairWithStorage(&keyAlias, g_keyLength,
                            P256, KEY_PURPOSE_SIGN_VERIFY, &extInfo);
    }
    if (ret != HC_SUCCESS) {
        LOGE("Generate key pair failed");
        return "null";
    }
    uint8_t *serverPkVal = (uint8_t *)HcMalloc(SERVER_PK_SIZE, 0);
    Uint8Buff serverPk = {
        .val = serverPkVal,
        .length = SERVER_PK_SIZE
    };
    ret = GetLoaderInstance()->exportPublicKey(&keyAlias, &serverPk);
    if (ret != HC_SUCCESS) {
        LOGE("exportPublicKey failed");
        HcFree(serverPkVal);
        return "null";
    }
    // sign begin
    Uint8Buff messageBuff = {
        .val = (uint8_t *)pkInfoStr,
        .length = strlen(pkInfoStr) + 1
    };
    uint8_t *signatureValue = (uint8_t *)HcMalloc(SIGNATURE_SIZE, 0);
    Uint8Buff signature = {
        .val = signatureValue,
        .length = SIGNATURE_SIZE
    };
    int32_t result = GetLoaderInstance()->sign(&keyAlias, &messageBuff, P256, &signature, true);
    if (result != HC_SUCCESS) {
        HcFree(serverPkVal);
        HcFree(signatureValue);
        return "null";
    }
    //sign end
    const char *cloudCredential = CredentialInfo(pkInfoStr, serverPk, signature);
    HcFree(serverPkVal);
    HcFree(signatureValue);
    return cloudCredential;
}

const char *CreateAccoutCG(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr)
{
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_GROUP_TYPE, IDENTICAL_ACCOUNT_GROUP);
    AddStringToJson(json, FIELD_USER_ID, userId);
    CJson *credJson = CreateJsonFromString(SignPkInfo(requestId, osAccountId, pkInfoStr));
    (void)AddObjToJson(json, "credential", credJson);
    FreeJson(credJson);
    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    return g_tmpStr;
}

const char *CreateCroAccoutCG(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr)
{
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_GROUP_TYPE, IDENTICAL_ACCOUNT_GROUP);
    AddStringToJson(json, FIELD_USER_ID, userId);
    // Encapsulate the same account data
    CJson *credJson = CreateJsonFromString(SignPkInfo(requestId, osAccountId, pkInfoStr));
    (void)AddObjToJson(json, "credential", credJson);
    g_tmpStr = PackJsonToString(json);
    // Encapsulate import credentials
    CJson *pkInfoJson = CreateJsonFromString(pkInfoStr);
    const char *udid = GetStringFromJson(pkInfoJson, FIELD_DEVICE_ID);
    // const char *baseInfo = "{\"udid\":\"devB\",\"deviceId\":\"devB\",\"userId\":\"4269DC28B639"
    // "681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4\"}";
    // CJson *credentialJson = CreateJsonFromString(baseInfo);
    CJson *credentialJson = CreateJson();
    AddStringToJson(credentialJson, FIELD_UDID, udid);
    AddStringToJson(credentialJson, FIELD_DEVICE_ID, "devB");
    AddStringToJson(credentialJson, FIELD_USER_ID, "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4");
    AddObjToJson(credentialJson, "credential", credJson);
    CJson *deviceList = CreateJsonArray();
    AddObjToArray(deviceList, credentialJson);
    CJson *mutliData = CreateJson();
    AddObjToJson(mutliData, "deviceList", deviceList);
    AddStringToJson(mutliData, "udid", udid);
    g_croPK = PackJsonToString(mutliData);
    FreeJson(pkInfoJson);
    FreeJson(credJson);
    FreeJson(credentialJson);
    FreeJson(mutliData);
    FreeJson(json);
    return g_tmpStr;
}

const char *CreateAccoutPK(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr)
{
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_GROUP_TYPE, IDENTICAL_ACCOUNT_GROUP);
    AddStringToJson(json, FIELD_USER_ID, userId);
    CJson *credJson = CreateJsonFromString(SignPkInfo(requestId, osAccountId, pkInfoStr));
    // Replace the wrong serverPk
    uint8_t *serverPkVal = (uint8_t *)"1234567";
    (void)AddByteToJson(credJson, "serverPk", serverPkVal, SERVER_PK_SIZE);
    (void)AddObjToJson(json, "credential", credJson);
    FreeJson(credJson);
    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    return g_tmpStr;
}

const char *CreateAccoutError(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr)
{
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_GROUP_TYPE, IDENTICAL_ACCOUNT_GROUP);
    AddStringToJson(json, FIELD_USER_ID, userId);
    CJson *credJson = CreateJsonFromString(SignPkInfo(requestId, osAccountId, pkInfoStr));
    uint8_t *serverPkVal = (uint8_t *)"1234567";
    (void)AddByteToJson(credJson, "pkInfoSignature", serverPkVal, SERVER_PK_SIZE);
    (void)AddObjToJson(json, "credential", credJson);
    FreeJson(credJson);
    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    return g_tmpStr;
}

const char *CreateDiffAccoutCG(int64_t requestId, const char *userId, const char *peerUserId)
{
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_GROUP_TYPE, ACROSS_ACCOUNT_AUTHORIZE_GROUP);
    AddStringToJson(json, FIELD_USER_ID, userId);
    AddStringToJson(json, FIELD_PEER_USER_ID, peerUserId);
    g_tmpStr = PackJsonToString(json);
    printf("createParamsStr: %s\n", g_tmpStr);
    FreeJson(json);
    return g_tmpStr;
}

const char *CreatAsyCroAccount(int sockfd, const char *deviceId)
{
    const char *queryAccParams = "{\"groupType\":1,\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryAccParams);
    const char *groupId = GetGroupIdfromData(groupInfo, g_index);
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, deletParamsStr);
    // Create a group of user2 in user space 1
    char localUdid[INPUT_UDID_LEN] = { 0 };
    GetLocalDeviceId(localUdid);
    const char *reqJsonStr = CreateReqJson(g_version, localUdid, g_userId2);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    const char *accoutGroup = CreateAccoutCG(g_requestId, OS_ACCOUNT_ID1, g_userId2, pkInfoStr);
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, accoutGroup);
    // Create a cross-account group for user1 in Userspace 1
    accoutGroup = CreateDiffAccoutCG(g_requestId, g_userId2, g_userId1);
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, accoutGroup);
    groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, g_queryCroParams);
    groupId = GetGroupIdfromData(groupInfo, g_index);
    // Notifies the peer end to create a cross-account group
    accoutGroup = CreateDiffAccoutCG(g_requestId, g_userId1, g_userId2);
    ToServerCreateGroup(sockfd, accoutGroup);
    return groupId;
}

const char *CreateSymmCroAcount(int sockfd, const char *groupId, const char *info)
{
    const char *accoutGroup = CreateDiffAccoutCG(g_requestId, g_userId1, g_userId2);
    ToServerCreateGroup(sockfd, accoutGroup);
    // Reset the local credentials
    char *deletParamsStr = DeleteGroupParams(groupId);
    DeleteGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, deletParamsStr);
    char localUdid[INPUT_UDID_LEN] = { 0 };
    GetLocalDeviceId(localUdid);
    const char *clientCredential = CreateLocalCred(info, localUdid);
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, clientCredential);
    accoutGroup = CreateDiffAccoutCG(g_requestId, g_userId2, g_userId1);
    CreateGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, accoutGroup);

    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, g_queryCroParams);
    const char *groupId1 = GetGroupIdfromData(groupInfo, g_index);
    return groupId1;
}

static char *GaOnRequestIdenticalAuth(int64_t requestId, int operationCode, const char *reqParams)
{
    printf("\n----------------GaOnRequestIdenticalAuth---------------\n");
    printf("|  RequestId: %-29d  |\n", (int)requestId);
    printf("|  OperationCode: %-25d  |\n", operationCode);
    printf("|  reqParams:  %s  |\n", (char *)reqParams);
    printf("\n----------------GaOnRequestIdenticalAuth---------------\n");
    CJson *json = CreateJson();
    AddIntToJson(json, FIELD_CONFIRMATION, g_accpet);
    AddIntToJson(json, FIELD_OS_ACCOUNT_ID, OS_ACCOUNT_ID1);
    AddStringToJson(json, FIELD_SERVICE_PKG_NAME, APPNAME);
    AddStringToJson(json, FIELD_PEER_CONN_DEVICE_ID, "udid1");
    char *returnDataStr = PackJsonToString(json);
    FreeJson(json);
    return returnDataStr;
}

static bool GaOnTransmitLocal(int64_t requestId, const uint8_t *data, uint32_t dataLen)
{
    printf("\n----------------GaOnTransmitLocal---------------\n");
    printf("|  RequestId: %-29d  |\n", (int)requestId);
    printf("|  SendMess:  %s  |\n", (char *)data);
    printf("\n----------------GaOnTransmitLocal---------------\n");
    if (memset_s(g_data, BUFFER_SIZE, 0, BUFFER_SIZE) != 0) {
        printf("GaOnTransmitLocal memset_s failed\n");
    }
    if (strcpy_s(g_data, dataLen, (char *)data) != 0) {
        printf("GaOnTransmitLocal strcpy_s failed\n");
    }
    return true;
}

// Initialize a single device analog gaCallback
int *LocalAccountAuth(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams)
{
    DeviceAuthCallback gaCallbackIdentical;
    gaCallbackIdentical.onRequest = GaOnRequestIdenticalAuth;
    gaCallbackIdentical.onError = OnError;
    gaCallbackIdentical.onFinish = OnFinish;
    gaCallbackIdentical.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallbackIdentical.onTransmit = GaOnTransmitLocal;
    
    int ret = g_gaInstance->authDevice(osAccountId, clientReqId, authParams, &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(serverReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                        &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(clientReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                        &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(serverReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                        &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(clientReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                        &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("auth end\n");
    return g_result;
}

int *LocalAccountClientDup(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams)
{
    DeviceAuthCallback gaCallbackIdentical;
    gaCallbackIdentical.onRequest = GaOnRequestIdenticalAuth;
    gaCallbackIdentical.onError = OnError;
    gaCallbackIdentical.onFinish = OnFinish;
    gaCallbackIdentical.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallbackIdentical.onTransmit = GaOnTransmitLocal;
    
    // client 1
    char clientFirst[BUFFER_SIZE] = "{\"supportedVersion\":\"1\",\"authForm\":1,\"userId\":\"123456\",\"step\":48,"
    "\"deviceId\":\"3CC8BEEABE527FE1DA9CD5E086D8E2A3A79F1D92611BADCFD47DA4C18CBCC877\",\"devId\":\"null\",\"data\":"
    "{\"authKeyAlgEncode\":0,\"authPkInfo\":\"{\"devicePk\":\"3059301306072A8648CE3D020106082A8648CE3D030107034200046"
    "C77C69CCCA07ACF505134FAEB7EF143F5E9262C27D0DC22F1822B04E5B049F7EF58CD7C4D34686A1BBADD190B286B1952885DC7C54D9B0FEE"
    "84AB88FAD0509E\",\"userId\":\"123456\",\"deviceId\":\"null\",\"version\":\"null\"}\",\"authPkInfoSign\":\"3045022"
    "0023A5D8EEAD2288FAFAAAA3A9BAB52056A382258B27D02D45A18B88AE66E0598022100D48EEB061DB7249AF47448EDEA66C43EAEC1B8DFB9A"
    "54C4331EE154705ECAD21\"},\"groupAndModuleVersion\":\"2.0.17\",\"isDeviceLevel\":false}";
    // client 2
    char clientSecond[BUFFER_SIZE] = "{\"authForm\":1,\"step\":50,\"data\":{\"kcfData\":\"75809EA611ECC825F51E22261"
    "C5416322C46B75F7FB60B5E490869E1692077C1\",\"deviceId\":\"3CC8BEEABE527FE1DA9CD5E086D8E2A3A79F1D92611BADCFD47DA"
    "4C18CBCC877\",\"epk\":\"C1800D6D487E7AD9C2411AD48140AD41E1ADDD277B2910EBF91C7AA719348EA6A6CD67BF1E3F87E3102361"
    "5D73F2F0AD48580B9CB9129D64135E89DDB8E42884\"},\"groupAndModuleVersion\":\"2.0.17\",\"isDeviceLevel\":false}";

    int ret = g_gaInstance->processData(serverReqId, (const uint8_t *)clientFirst,
                                        (uint32_t)strlen(clientFirst) + 1, &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(serverReqId, (const uint8_t *)clientSecond,
                                        (uint32_t)strlen(clientSecond) + 1, &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("auth end\n");
    return g_result;
}

int *LocalAccountServerDup(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams)
{
    DeviceAuthCallback gaCallbackIdentical;
    gaCallbackIdentical.onRequest = GaOnRequestIdenticalAuth;
    gaCallbackIdentical.onError = OnError;
    gaCallbackIdentical.onFinish = OnFinish;
    gaCallbackIdentical.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallbackIdentical.onTransmit = GaOnTransmitLocal;
    
    // server 1
    char serverFirst[BUFFER_SIZE] = "{\"supportedVersion\":\"1\",\"userId\":\"123456\",\"step\":49,\"deviceId\":"
    "\"3CC8BEEABE527FE1DA9CD5E086D8E2A3A79F1D92611BADCFD47DA4C18CBCC877\",\"authForm\":1,\"devId\":\"null\",\"data\":"
    "{\"authKeyAlgEncode\":0,\"authPkInfo\":\"{\"devicePk\":\"3059301306072A8648CE3D020106082A8648CE3D030107034200046C"
    "77C69CCCA07ACF505134FAEB7EF143F5E9262C27D0DC22F1822B04E5B049F7EF58CD7C4D34686A1BBADD190B286B1952885DC7C54D9B0FEE8"
    "4AB88FAD0509E\",\"userId\":\"123456\",\"deviceId\":\"null\",\"version\":\"null\"}\",\"authPkInfoSign\":\"30450220"
    "023A5D8EEAD2288FAFAAAA3A9BAB52056A382258B27D02D45A18B88AE66E0598022100D48EEB061DB7249AF47448EDEA66C43EAEC1B8DFB9A5"
    "4C4331EE154705ECAD21\",\"epk\":\"0D6D8B5C6745B1C48989BE7E597280F05BC0FACC6AC7116D78C78095235F979B4E71AAD381F92F0E4"
    "A5D6BDDF678953BCA264300F3A33069C2F49F9A157F4F32\",\"salt\":\"98208FE16FB539998E65DB8595A11D2E\"},\"groupAndModuleV"
    "ersion\":\"2.0.17\",\"isDeviceLevel\":false}";
    // server 2
    char serverSecond[BUFFER_SIZE] = "{\"authForm\":1,\"step\":51,\"data\":{\"kcfData\":\"A662EC75CC8E371625EBEEDF9FB35"
    "728EF96321E7D1FA052AB3199883DEB3C62\"},\"groupAndModuleVersion\":\"2.0.17\"}";

    int ret = g_gaInstance->authDevice(osAccountId, clientReqId, authParams, &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(clientReqId, (const uint8_t *)serverFirst, (uint32_t)strlen(serverFirst) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(clientReqId, (const uint8_t *)serverSecond, (uint32_t)strlen(serverSecond) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("auth end\n");
    return g_result;
}

int *LocalClientSame(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams)
{
    LOGI("LocalClientSame\n");
    DeviceAuthCallback gaCallbackIdentical;
    gaCallbackIdentical.onRequest = GaOnRequestIdenticalAuth;
    gaCallbackIdentical.onError = OnError;
    gaCallbackIdentical.onFinish = OnFinish;
    gaCallbackIdentical.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallbackIdentical.onTransmit = GaOnTransmitLocal;
    
    int ret = g_gaInstance->authDevice(osAccountId, clientReqId, authParams, &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    char sameMessage[BUFFER_SIZE] = {0};
    if (memset_s(sameMessage, BUFFER_SIZE, 0, BUFFER_SIZE) != 0) {
        printf("LocalClientSame memset_s failed\n");
    }
    if (strcpy_s(sameMessage, sizeof(sameMessage), g_data) != 0) {
        printf("LocalClientSame strcpy_s failed\n");
    }
    printf("sameMessage:%s\n", sameMessage);
    ret = g_gaInstance->processData(serverReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    printf("sameMessage:%s\n", sameMessage);
    ret = g_gaInstance->processData(serverReqId, (const uint8_t *)sameMessage, (uint32_t)strlen(sameMessage) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("auth end\n");
    return g_result;
}

int *LocalServerSame(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams)
{
    DeviceAuthCallback gaCallbackIdentical;
    gaCallbackIdentical.onRequest = GaOnRequestIdenticalAuth;
    gaCallbackIdentical.onError = OnError;
    gaCallbackIdentical.onFinish = OnFinish;
    gaCallbackIdentical.onSessionKeyReturned = OnSessionKeyReturned;
    gaCallbackIdentical.onTransmit = GaOnTransmitLocal;
    
    int ret = g_gaInstance->authDevice(osAccountId, clientReqId, authParams, &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(serverReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    char sameMessage[BUFFER_SIZE] = {0};
    if (memset_s(sameMessage, BUFFER_SIZE, 0, BUFFER_SIZE) != 0) {
        printf("LocalServerSame memset_s failed");
    }
    if (strcpy_s(sameMessage, sizeof(sameMessage), g_data) != 0) {
        printf("LocalServerSame strcpy_s failed");
    }

    ret = g_gaInstance->processData(clientReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    ret = g_gaInstance->processData(clientReqId, (const uint8_t *)g_data, (uint32_t)strlen(g_data) + 1,
                                    &gaCallbackIdentical);
    sleep(SLEEP_TIME);
    g_result[OPERATION] = ret;
    g_result[MESSAGE] = g_messageCode;
    printf("auth end\n");
    return g_result;
}

const char *CreateLocalCred(const char *baseInfo, const char *deviceId)
{
    CJson *json = CreateJsonFromString(baseInfo);
    AddStringToJson(json, FIELD_DEVICE_ID, deviceId);
    g_tmpStr = PackJsonToString(json);
    FreeJson(json);
    return g_tmpStr;
}

const char *CreateSymmInfo(const char *info1, const char *info2, const char *groupId, int groupType)
{
    CJson *device1 = CreateJsonFromString(info1);
    if (strcmp(g_peerDeviceId, "") != 0) {
        AddStringToJson(device1, FIELD_UDID, g_peerDeviceId);
        AddStringToJson(device1, FIELD_DEVICE_ID, g_peerDeviceId);
    } else {
        AddStringToJson(device1, FIELD_UDID, "devB");
        AddStringToJson(device1, FIELD_DEVICE_ID, "devB");
    }
    
    CJson *device2 = CreateJsonFromString(info2);
    // [{},{}]
    CJson *deviceList = CreateJsonArray();
    AddObjToArray(deviceList, device1);
    AddObjToArray(deviceList, device2);

    // {deviceList:[]}
    CJson *json = CreateJson();
    AddObjToJson(json, FIELD_DEVICE_LIST, deviceList);
    AddStringToJson(json, FIELD_GROUP_ID, groupId);
    AddIntToJson(json, FIELD_GROUP_TYPE, groupType);
    g_tmpStr = PackJsonToString(json);
    printf("CreateSymmInfo: %s\n", g_tmpStr);
    FreeJson(json);
    FreeJson(deviceList);
    return g_tmpStr;
}

const char *CreateImportData(const char *baseInfo, const char *groupId, int groupType)
{
    CJson *json = CreateJsonFromString(baseInfo);
    AddStringToJson(json, FIELD_GROUP_ID, groupId);
    AddIntToJson(json, FIELD_GROUP_TYPE, groupType);
    char *tmpStr = PackJsonToString(json);
    printf("CreateSymmInfo: %s\n", tmpStr);
    FreeJson(json);
    return tmpStr;
}

int *AddMultiMembersTest(int32_t osAccountId, const char *appId, const char *addParams)
{
    LOGI("start addMultiMembersToGroup ...");
    int ret = g_gmInstance->addMultiMembersToGroup(osAccountId, appId, addParams);
    g_result[OPERATION] = g_operationCode;
    g_result[ERROR] = g_errorCode;
    g_result[MESSAGE] = g_messageCode;
    g_result[EXCUTE] = ret;
    printf("%d", g_result[EXCUTE]);
    return g_result;
}

const char *DelSymmInfo(const char *groupId, int groupType)
{
    const char *baseInfo = "{\"deviceList\":["
    "{\"udid\":\"devB\",\"deviceId\":\"devB\"},"
    "{\"udid\":\"devC\",\"deviceId\":\"devC\"}]}";
    CJson *json = CreateJsonFromString(baseInfo);
    AddStringToJson(json, FIELD_GROUP_ID, groupId);
    AddIntToJson(json, FIELD_GROUP_TYPE, groupType);
    g_tmpStr = PackJsonToString(json);
    printf("CreateSymmInfo: %s\n", g_tmpStr);
    FreeJson(json);
    return g_tmpStr;
}

int *DelMultiMembersTest(int32_t osAccountId, const char *appId, const char *deleteParams)
{
    LOGI("start delMultiMembersFromGroup ...");
    int ret = g_gmInstance->delMultiMembersFromGroup(osAccountId, appId, deleteParams);
    g_result[OPERATION] = g_operationCode;
    g_result[ERROR] = g_errorCode;
    g_result[MESSAGE] = g_messageCode;
    g_result[EXCUTE] = ret;
    printf("delMultiMembersFromGroup:%d\n", g_result[EXCUTE]);
    return g_result;
}

const char *ToGetServerPK(int sockfd)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", GETSERVERPK);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
    const char *recvData = RecvData(sockfd);
    return recvData;
}

void GetServerPK(int sockfd)
{
    char localUdid[INPUT_UDID_LEN] = { 0 };
    GetLocalDeviceId(localUdid);
    const char *reqJsonStr = CreateReqJson(g_version, (const char*)localUdid, g_userId1);
    const char *pkInfoStr = TestGetRegisterInfo(reqJsonStr);
    SendMess(sockfd, pkInfoStr);
}

// The client notifies the server, and the server processes the logic
void ToServerInit(int sockfd)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", TOSERVERINIT);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != SERVERINITSUCC) {
        LOGE("Server init fail");
    }
}

void SetServer()
{
    g_isClient = false;
}

void ServerInit(int sockfd)
{
    InitEnv();
    // double sleepTime = 2.0;
    // WaitTime(sleepTime);
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERINITSUCC);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
}

void ToServerDestroy(int sockfd)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERDESTROY);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != SERVERDESTROYSUCC) {
        LOGE("Server destroy fail");
    }
}

void ServerDestroy(int sockfd)
{
    Destory();
    sleep(SLEEP_TIME);
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERDESTROYSUCC);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
}

void ToServerReg(int sockfd, const char *appId)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", REG);
    AddStringToJson(json, "pkg", appId);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != REG) {
        LOGE("Server destroy fail");
    }
}

void ServerReg(int sockfd, const char *data)
{
    CJson *clientJson = CreateJsonFromString(data);
    const char *appId = GetStringFromJson(clientJson, "pkg");
    DoubleCallBack(appId);
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", REG);
    char *sendData = PackJsonToString(json);
    FreeJsonString(sendData);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJson(clientJson);
}


void ToServerDeleteGroup(int sockfd)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERDELETEGROUP);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != SERVERSUCCESS) {
        LOGE("Server deleteGroup fail");
    }
}

void ServerDeleteGroup(int sockfd)
{
    const char *queryParams = "{\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    printf("groupInfo:%s\n", groupInfo);
    if ((int)strlen(groupInfo) > g_minLength) {
        const char *groupId = GetGroupIdfromData(groupInfo, g_index);
        char *deletParamsStr = DeleteGroupParams(groupId);
        int *ret = DeleteGroupTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, deletParamsStr);
        printf("ret:%d\n", ret[MESSAGE]);
    }
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERSUCCESS);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
}


void ToServerDelete(int sockfd)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERDELETE);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != SERVERSUCCESS) {
        LOGE("Server deleteGroup fail");
    }
}

void TestServerDelete(int sockfd)
{
    const char *queryParams = "{\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    printf("groupInfo:%s", groupInfo);
    if ((int)strlen(groupInfo) > g_minLength) {
        g_gmCallback.onTransmit = OnTransmitDeleteDouble;
        g_gmInstance->regCallback(APPNAME, &g_gmCallback);
        const char *groupId = GetGroupIdfromData(groupInfo, g_index);
        const char *peerDeviceId = GetPeerDevId(sockfd);
        char *deletParamsStr = DeleteParams(peerDeviceId, groupId, true);
        DeleteMemberTest(OS_ACCOUNT_ID1, g_requestId, APPNAME, deletParamsStr, sockfd);
        // double sleepTime = 3.0;
        // WaitTime(sleepTime);
    }
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERSUCCESS);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
}


const char *ToServerCreateGroup(int sockfd, const char *credential)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERCREATE);
    AddStringToJson(json, "credential", credential);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
    const char *recvData = RecvData(sockfd);
    return recvData;
}

void ServerCreateGroup(int64_t requestId, const char *data, int sockfd)
{
    printf("requestId:%d\n", (int)requestId);
    CJson *json = CreateJsonFromString(data);
    const char *params = GetStringFromJson(json, "credential");
    int *ret = CreateGroupTest(OS_ACCOUNT_ID1, requestId, APPNAME, params);
    if (ret[MESSAGE] == ON_FINISH) {
        SendMess(sockfd, "ON_FINISH");
    } else {
        SendMess(sockfd, "ON_ERROR");
    }
    FreeJson(json);
}

int *ToServerBind(int sockfd, const char *groupId, const char *pinCode)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERBIND);
    AddStringToJson(json, GROUP_ID_TEXT, groupId);
    AddStringToJson(json, "pinCode", pinCode);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
    const char *serverData = RecvData(sockfd);
    // Processing peer message
    int *ret = GMProcessData(serverData, BIND, sockfd);
    serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, BIND, sockfd);
    serverData = RecvData(sockfd);
    ret = GMProcessData(serverData, BIND, sockfd);
    return ret;
}

void ServerBind(int sockfd, const char *data)
{
    CJson *json = CreateJsonFromString(data);
    const char *groupId = GetStringFromJson(json, GROUP_ID_TEXT);
    const char *pinCode = GetStringFromJson(json, "pinCode");
    const char *addParamsStr = AddParams(groupId, GROUP_TYPE_P2P, pinCode, NULL);
    AddMemberTest(OS_ACCOUNT_ID1, REQUEST_ID, APPNAME, addParamsStr, BIND, sockfd);
    FreeJson(json);
}

int *ToServerAuth(int sockfd, const char *deviceId, const char *userId, int type, bool isDeviceLevel)
{
    // Notifies the peer end to initiate authentication
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", STARTAUTH);
    AddStringToJson(json, FIELD_DEVICE_ID, deviceId);
    AddStringToJson(json, FIELD_USER_ID, userId);
    AddIntToJson(json, FIELD_GROUP_TYPE, type);
    AddBoolToJson(json, FIELD_IS_DEVICE_LEVEL, isDeviceLevel);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJson(json);
    FreeJsonString(sendData);
    const char *serverData = RecvData(sockfd);
    // Processing peer message
    int *ret = ProcessAuthData(serverData, AUTH, sockfd);
    serverData = RecvData(sockfd);
    ret = ProcessAuthData(serverData, AUTH, sockfd);
    return ret;
}

void ServerAuth(const char *data, int sockfd)
{
    CJson *json = CreateJsonFromString(data);
    const char *deviceId = GetStringFromJson(json, FIELD_DEVICE_ID);
    const char *userId = GetStringFromJson(json, FIELD_USER_ID);
    bool isDeviceLevel = false;
    GetBoolFromJson(json, FIELD_IS_DEVICE_LEVEL, &isDeviceLevel);
    int type;
    GetIntFromJson(json, FIELD_GROUP_TYPE, &type);
    const char *groupId = NULL;
    if (isDeviceLevel == false) {
        if (type == GROUP_TYPE_ALL) {
            groupId = NULL;
        } else if (type == GROUP_TYPE_ACCOUNT) {
            const char *queryAccParams = "{\"groupType\":1,\"groupOwner\":\"com.huawei.security\"}";
            char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryAccParams);
            groupId = GetGroupIdfromData(groupInfo, g_index);
        } else if (type == GROUP_TYPE_CRO_ACCOUNT) {
            const char *queryCroParams = "{\"groupType\":1282,\"groupOwner\":\"com.huawei.security\"}";
            char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryCroParams);
            groupId = GetGroupIdfromData(groupInfo, g_index);
        } else {
            const char *queryParams = "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}";
            char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
            groupId = GetGroupIdfromData(groupInfo, g_index);
        }
        char *authParams = AuthParams(deviceId, APPNAME, true, isDeviceLevel, groupId, 0, userId);
        AuthDeviceTest(OS_ACCOUNT_ID1, g_requestId, authParams, AUTH, sockfd);
    } else {
        char *authParams = AuthParams(deviceId, APPNAME, true, isDeviceLevel, NULL, 0, userId);
        AuthDeviceTest(OS_ACCOUNT_ID1, g_requestId, authParams, AUTH, sockfd);
    }

    FreeJson(json);
}

void ToSerCancelBind(int sockfd, int32_t requestId, const char *appId)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", CANCELBIND);
    AddIntToJson(json, "requestId", (int)requestId);
    AddStringToJson(json, "appId", appId);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != SERVERINITSUCC) {
        LOGE("Server init fail");
    }
}

static void SerCancelBind(int sockfd, const char *data)
{
    CJson *clientJson = CreateJsonFromString(data);
    int requestId;
    GetIntFromJson(clientJson, "requestId", &requestId);
    const char *appId = GetStringFromJson(clientJson, "appId");
    CancelBind(requestId, appId);
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERINITSUCC);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
}

void ToSerCancelAuth(int sockfd, int32_t requestId, const char *appId)
{
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", CANCELAUTH);
    AddIntToJson(json, "requestId", (int)requestId);
    AddStringToJson(json, "appId", appId);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
    const char *recvData = RecvData(sockfd);
    if (GetOperCodeFromData(recvData) != SERVERINITSUCC) {
        LOGE("Server init fail");
    }
}

static void SerCancelAuth(int sockfd, const char *data)
{
    CJson *clientJson = CreateJsonFromString(data);
    int requestId;
    GetIntFromJson(clientJson, "requestId", &requestId);
    const char *appId = GetStringFromJson(clientJson, "appId");
    CancelAuth(requestId, appId);
    CJson *json = CreateJson();
    AddIntToJson(json, "operCode", SERVERINITSUCC);
    char *sendData = PackJsonToString(json);
    SendMess(sockfd, sendData);
    FreeJsonString(sendData);
    FreeJson(json);
}

void ToServerClose(int sockfd)
{
    char *sendData = "close";
    SendMess(sockfd, sendData);
}

void TestGetPkInfo(int32_t osAccountId, const char *appId, const char *udid, bool isSelfPk, uint32_t *returnInfoNum)
{
    CJson *queryJson = CreateJson();
    AddStringToJson(queryJson, FIELD_UDID, udid);
    AddBoolToJson(queryJson, FIELD_IS_SELF_PK, isSelfPk);
    char *queryParams = PackJsonToString(queryJson);
    char *returnInfoList = NULL;
    g_gmInstance->getPkInfoList(osAccountId, appId, queryParams, &returnInfoList, returnInfoNum);
    printf("pkInfoList:%s\n", returnInfoList);
    FreeJsonString(queryParams);
    FreeJson(queryJson);
}

void SwitchCase(int64_t requestId, int code, const char *data, int sockfd)
{
    switch (code) {
        case REG:
            ServerReg(sockfd, data);
            break;
        case REGREG:
            ServerReg(sockfd, data);
            break;
        case BIND:
            GMProcessData(data, code, sockfd);
            break;
        case ERRORFIRSTMESSAGE:
            ReturnFirstMessage(sockfd);
            break;
        case ERRORSECONDMESSAGE:
            ReturnSecondMessage(sockfd);
            break;
        case DUPLICATEMESSAGE:
            DoDuplicateMessage(requestId, data, code, sockfd);
            break;
        case SERVERBIND:
            SetServer();
            ServerBind(sockfd, data);
            break;
        case AUTH:
            ProcessAuthData(data, code, sockfd);
            break;
        case AUTHERRMESSFIRST:
            ReturnFirstAuthMessage(sockfd);
            break;
        case AUTHERRMESSSECOND:
            ReturnSecondAuthMessage(sockfd);
            break;
        case AUTHATTACK:
            AuthAttack(requestId, data, code, sockfd);
            break;
        case DELETE:
            GMProcessData(data, code, sockfd);
            break;
        case SERVERDELETE:
            TestServerDelete(sockfd);
            break;
        case SERVERDELETEGROUP:
            ServerDeleteGroup(sockfd);
            break;
        case TOSERVERINIT:
            ServerInit(sockfd);
            break;
        case SERVERDESTROY:
            ServerDestroy(sockfd);
            break;
        case GETSERVERPK:
            GetServerPK(sockfd);
            break;
        case SERVERCREATE:
            ServerCreateGroup(requestId, data, sockfd);
            break;
        case STARTAUTH:
            ServerAuth(data, sockfd);
            break;
        case GETPEERDEVICEID:
            writePeerDevice(data);
            SendDeviceId(sockfd);
            break;
        case CANCELBIND:
            SerCancelBind(sockfd, data);
            break;
        case CANCELAUTH:
            SerCancelAuth(sockfd, data);
            break;
        default:
            break;
    }
}

void DemoEcSpeke(const char* messageDir, int32_t requestId)
{
    int sockfd = -1;
    char dirPath[50] = "/data/test/message/";
    char buff[BUFFER_SIZE];
    LOGI("test protocol messeage:%s\n", messageDir);
    FILE *fp = NULL;
    // client first message to bind device
    const char *queryParams = "{\"groupOwner\":\"com.huawei.security\"}";
    char *groupInfo = GetGroupInfoTest(OS_ACCOUNT_ID1, APPNAME, queryParams);
    const char *groupId = GetGroupIdfromData(groupInfo, g_index);
    int groupType = 256;
    const char *addParamsStr = AddParams(groupId, groupType, g_pinCode, NULL);
    printf("The first message from client.\n");
    AddMemberTest(OS_ACCOUNT_ID1, requestId, APPNAME, addParamsStr, ERRORFIRSTMESSAGE,
            sockfd);
    // server first message
    printf("The first message from server.\n");
    const char *serverFirstMessage = "{\"payload\":{\"salt\":\"3AFB3F5165C0E308DD59357F7A508E50\",\"epk\":\"7E3D8F2CCA"
    "5FFB9749F50F62B3EE5C7EA35E0F2E017209F53DA75B8482508B7D\",\"challenge\":\"9075F44D5811F636C476F0348F317B75\","
    "\"version\":{\"minVersion\":\"1.0.0\",\"currentVersion\":\"2.0.26\"}},\"message\":32769,"
    "\"groupAndModuleVersion\":\"2.0.1\",\"groupId\":\"BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBC"
    "C66A54DC0B\",\"groupName\":\"P2PGroup\",\"groupOp\":2,\"groupType\":256,\"peerDeviceId\":\"004FB07744533"
    "F00E763EEBB3194F9E183989AAF1502D3C86F31251973CB8F7F\",\"connDeviceId\":\"004FB07744533F00E763EEBB3194F9E"
    "183989AAF1502D3C86F31251973CB8F7F\",\"appId\":\"com.huawei.security\",\"ownerName\":\"\"}";
    // client second message
    printf("The second message from client.\n");
    CJson *tempJson = CreateJsonFromString(serverFirstMessage);
    AddInt64StringToJson(tempJson, REQUEST_ID_TEXT, (int64_t)requestId);
    char *tempChar = PackJsonToString(tempJson);
    GMProcessData(tempChar, BIND, sockfd);
    FreeJsonString(tempChar);
    FreeJson(tempJson);
    // server second message
    if (strcat_s(dirPath, sizeof(dirPath), messageDir) != 0) {
        printf("strcat failed.\n");
    }
    fp = fopen(dirPath, "r");
    if (fgets(buff, BUFFER_SIZE, (FILE*)fp) != 0) {
        printf("buff: %s\n", buff);
    }
    const char *serverSecondMessage = buff;
    CJson *tempJson1 = CreateJsonFromString(serverSecondMessage);
    AddInt64StringToJson(tempJson1, REQUEST_ID_TEXT, (int64_t)requestId);
    char *tempChar1 = PackJsonToString(tempJson1);
    printf("server second message: %s\n", tempChar1);
    // client third message
    GMProcessData(tempChar1, BIND, sockfd);
    if (fclose(fp) !=0) {
        printf("close file filed.\n");
    }
    FreeJsonString(tempChar1);
    FreeJson(tempJson1);
}

void testhread3861(void (*entry)(void *arg))
{
    #define MAINLOOP_STACK_SIZE 5120
    pthread_t tid;
    pthread_attr_t threadAttr;
    pthread_attr_init(&threadAttr);
    pthread_attr_setstacksize(&threadAttr, MAINLOOP_STACK_SIZE);
    if (pthread_create(&tid, &threadAttr, (void *)entry, 0) != 0) {
        printf("create DeathProcTask failed");
        return;
    }
    printf("[testhread3861] loop thread creating");
    return;
}


int GetGroupNum(char* data)
{
    int step = 0;
    int count = 1;
    for (int i=0; i < (int)strlen(data); i++) {
        if (data[i] == '}') {
            step = 1;
        } else if (data[i] == ',' && step == 1) {
            step = 2;
        } else if (data[i] == '{' && step == 2) {
            count += 1;
        } else {
            step  = 0;
        }
    }
    return count;
}