/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */

#include "device_auth_socket.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <securec.h>

char g_strbuffer[2048];
int g_sbuflen = 8192 * 2;
int g_listenMode = 5;
void SetSocket(int listenClient)
{
    setsockopt(listenClient, SOL_SOCKET, SO_SNDBUF, (const char*)&g_sbuflen, sizeof(int));
}

int InitClient(const char *serverip, const int port)
{
    // Create a socket for the client
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    printf("sockfd:%d\n", sockfd);
    // The data structure of ip address information
    struct hostent *h;
    if ((h = gethostbyname(serverip)) == 0) {
        printf("connect erro");
        perror("gethostbyname");
        close(sockfd);

        return -1;
    }
    // Translate the server's address and port into a data structure
    struct sockaddr_in servaddr;
    memset_s(&servaddr, sizeof(servaddr), 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(port);
    memcpy_s(&servaddr.sin_addr, sizeof(servaddr.sin_addr), h->h_addr, h->h_length);
    // Make a connection request to the server
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0) {
        perror("connect");
        close(sockfd);

        return -1;
        }
    if (sockfd <= 0) {
        printf("Failed to connect to server.\n");

        return -1;
    } else {
        printf("Succeed to connect to server.\n");
        printf("Succeed sockfd:%d\n", sockfd);
        return sockfd;
    }
}

int InitServer(int port)
{
    // Create a server socket
    int listenfd = socket(AF_INET, SOCK_STREAM, 0);
    // The address and port used by the server to communicate are bound to the socket
    struct sockaddr_in servaddr;
    memset_s(&servaddr, sizeof(servaddr), 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(port);
    if (bind(listenfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) != 0) {
        perror("bind");
        close(listenfd);
        return -1;
        }
    // Set the socket to listen mode
    if (listen(listenfd, g_listenMode) != 0) {
        perror("listen");
        close(listenfd);
        return -1;
    }
    return listenfd;
}

int Accept(int listenClient)
{
    int sockfd;
    // Accept the connection from the client.
    printf("start accept\n");
    if ((sockfd = accept(listenClient, 0, 0)) <= 0) {
        printf("The server failed to accept.\n");
        return -1;
    } else {
        printf("accept sockfd:%d\n", sockfd);
        return sockfd;
    }
}

void SendMess(int sockfd, const char *buf)
{
    char strbuffer[2048];
    memset_s(strbuffer, sizeof(strbuffer), 0, sizeof(strbuffer));
    if (sprintf_s(strbuffer, sizeof(strbuffer), "%s", buf) < 0) {
        printf("sprintf_s failed");
    }
    if (send(sockfd, strbuffer, strlen(strbuffer) + 1, 0) <= 0) {
        printf("Send filed:%s\n", strbuffer);
    } else {
        printf("sockfd:%d, send:%s\n", sockfd, strbuffer);
    }
    printf("send.\n");
}

char *RecvData(int sockfd)
{
    memset_s(g_strbuffer, sizeof(g_strbuffer), 0, sizeof(g_strbuffer));
    if (recv(sockfd, g_strbuffer, sizeof(g_strbuffer), 0) <= 0) {
        return "";
    } else {
        printf("sockfd:%d, recv:%s\n", sockfd, g_strbuffer);
        return g_strbuffer;
    }
}

void CloseClient(int sockfd)
{
    close(sockfd);
}

void CloseListen(int listenClient)
{
    printf("The client is disconnected。\n");
    close(listenClient);
    listenClient = 0;
}