/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */

#ifndef DEVICE_AUTH_FUNC_H
#define DEVICE_AUTH_FUNC_H

#include <stdbool.h>
#include <stdint.h>
#include "device_auth.h"

// operCode
#define REG 701
#define REGREG 702
#define BIND 101
#define ERRORFIRSTMESSAGE 102
#define ERRORSECONDMESSAGE 103
#define DUPLICATEMESSAGE 104
#define SETACCESSTOKEN 105
#define ERRORFIRSTMESSAGELITE 106
#define ERRORSECONDMESSAGELITE 107
#define CANCELBIND 108
#define SERVERBIND 109
#define AUTH 201
#define AUTHERRMESSFIRST 202
#define AUTHERRMESSSECOND 203
#define AUTHATTACK 210
#define CANCELAUTH 204
#define DELETE 301
#define SERVERDELETE 302
#define SERVERSUCCESS 303
#define SERVERDELETEGROUP 304
#define TOSERVERINIT 401
#define SERVERINITSUCC 402
#define SERVERDESTROY 501
#define SERVERDESTROYSUCC 502
#define GETSERVERPK 800
#define SERVERCREATE 801
#define STARTAUTH 802
#define GETPEERDEVICEID 1001

// limit udid length
#define INPUT_UDID_LEN 65
#define MAX_INPUT_UDID_LEN 200

// groupType
#define GROUP_TYPE_ERROR -1
#define GROUP_TYPE_ALL 0
#define GROUP_TYPE_P2P 256
#define GROUP_TYPE_ACCOUNT 1
#define GROUP_TYPE_CRO_ACCOUNT 1282

// group info
#define OS_ACCOUNT_NULL 0
#define OS_ACCOUNT_ID1 100
#define OS_ACCOUNT_ID2 101
#define REQUEST_ID 10
#define REQUEST_ID_NEW 11
#define REQUEST_ID_ERROR 1000
#define APPNAME "com.huawei.security"
#define APPNAME2 "TestApp2"
#define APPNAME_ERROR "TestAppERROR"
#define PINCODE "123456"
#define PINCODE_ERROR "111111"
#define GROUPNAME "P2PGroup"
#define VISIBILITY_PUBLIC -1
#define GROUP_ID_TEXT "groupId"
#define GROUP_ID_NORMAL "FF9B5A053D74CC8835DB98FAADBCE1A8E4C441637A3A170CB95D4DDD83319C49"
#define GROUP_ID_ERROR "BC680ED1137A5731F4A5A90B1AACC4A0A3663F6FC2387B7273EFBCC66A54DCA0"
#define DEVICE_ID_ERROR "1FA5B42ADEB61D7D8084B2148AF18FA5516CC66FA62E15BF65EFA16336052222"
#define PEER_DEVICE_ID_ERROR "004FB07744533F00E763EEBB3194F9E183989AAF1502D3C86F31251973C08000"

// query group info
#define GROUP_INDEX 0
#define QUERY_ACCOUNT_GROUP "{\"groupType\":1,\"groupOwner\":\"com.huawei.security\"}"
#define QUERY_CROACCOUNT_GROUP "{\"groupType\":1282,\"groupOwner\":\"com.huawei.security\"}"
#define QUERY_P2P_GROUP "{\"groupType\":256,\"groupOwner\":\"com.huawei.security\"}"
#define QUERY_ALL_GROUP "{\"groupOwner\":\"com.huawei.security\"}"
#define QUERY_ALL_GROUP_Owner2 "{\"groupOwner\":\"TestApp2\"}"

// bind param
#define IS_CLIENT true
#define NO_DEVICE_LEVEL false
#define SLEEP_TIME 1

// account device auth
#define VERSION "1.0.0"
#define ACCOUNT_DEVICE_ID "123456789"
#define USER_ID1 "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E4"
#define USER_ID2 "4269DC28B639681698809A67EDAD08E39F207900038F91FEF95DD042FE2874E3"

typedef enum {
    ON_REQUEST = 1,
    ON_ERROR = 2,
    ON_FINISH = 3,
    ON_SESSION_KEY_RETURNED = 4,
    ON_TRANSMIT = 5
} CallbackType;

enum {
    GROUP_CREATED = 0,
    GROUP_DELETED,
    DEVICE_BOUND,
    DEVICE_UNBOUND,
    DEVICE_NOT_TRUSTED,
    LAST_GROUP_DELETED,
    TRUSTED_DEVICE_NUM_CHANGED
};

enum {
    OPERATION = 0,
    ERROR,
    MESSAGE,
    EXCUTE
};

// Double device
void InitTest(const char *appId);
void DeInitTest(void);

// 辅助功能 - 环境配置
int InitEnv(void);
void Destory(void);
void InitResultPool(int requestId);
void SetSocketPool(int requestId, int sockfd);
int GetLocalDeviceId(char *udid);
const char *GetPeerDevId(int sockfd);
void ClearTempValue(void);
void ResetPeerDeviceId(void);
void testhread3861(void (*entry)(void *arg));
// 辅助功能 - 返回值处理
int GetResultItemNum(char *groupVec);
const char *GetGroupIdfromData(char *groupVec, int index);
int CompareInfo(char *groupVec, const char *key, const int index, const char *compareStr);
int CompareInfo1(char *groupVec, const char *key, const char *compareStr);
int64_t GetReqIdFromData(int64_t *reqId, const char *data);
int GetOperCodeFromData(const char *data);
int GetErrorCode(const char *data);
int GetErrorMsg(const char *data);

// 辅助功能 - 其他工具
void FreeTmpStr(void);
void WaitTime(double sleepTime);
void PrintResult(int num);
int GetGroupNum(char* data); 
// 基本功能封装
void InitListener(const char *appId);
int DoubleCallBack(const char *appId);
int *CreateGroupTest(int32_t osAccountId, int32_t requestId, const char *groupOwner, const char *createParamsStr);
int *DeleteGroupTest(int32_t osAccountId, int32_t requestId, const char *groupOwner, const char *disbandParams);
int *AddMemberTest(int32_t osAccountId, int64_t requestId, const char *appId, const char *authParams, int code,
    int sockfd);
int *GMProcessData(const char *data, int code, int sockfd);
void CancelBind(int32_t requestId, const char *appId);
int *AuthDeviceTest(int32_t osAccountId, int64_t authReqId, const char *authParams, int code, int sockfd);
int *ProcessAuthData(const char *data, int code, int sockfd);
void CancelAuth(int32_t requestId, const char *appId);
int *DeleteMemberTest(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams, int sockfd);
char *GetGroupInfoByIdTest(int32_t osAccountId, const char *appId, const char *groupId);
char *GetGroupInfoTest(int32_t osAccountId, const char *groupOwner, const char *queryParams);
char *GetJoinedGroupsTest(int32_t osAccountId, const char *appId, int groupType);
char *GetRelatedGroupsTest(int32_t osAccountId, const char *appId, const char *peerDeviceId);
char *GetDeviceInfoByIdTest(int32_t osAccountId, const char *appId, const char *deviceId, const char *groupId);
uint32_t GetTrustedDevicesTest(int32_t osAccountId, const char *appId, const char *groupId);
int CheckAccessToGroupTest(int32_t osAccountId, const char *appId, const char *groupId);
bool IsDeviceInGroupTest(int32_t osAccountId, const char *appId, const char *groupId, const char *deviceId);
void DestoryTest(char *returnInfo);
const char *TestGetRegisterInfo(const char *reqJsonStr);
int *LocalAccountAuth(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams);
int *AddMultiMembersTest(int32_t osAccountId, const char *appId, const char *addParams);
int *DelMultiMembersTest(int32_t osAccountId, const char *appId, const char *deleteParams);
void TestGetPkInfo(int32_t osAccountId, const char *appId, const char *udid, bool isSelfPk, uint32_t *returnInfoNum);
// 核心功能（二次封装）
int *TestBind(int32_t osAccountId, int64_t requestId, const char *appId, const char *authParams, int sockfd);
int *TestAuth(int32_t osAccountId, int64_t authReqId, const char *authParams, int sockfd);
int *TestCoDelete(int32_t osAccountId, int64_t requestId, const char *appId, const char *deleteParams, int sockfd);

// 数据生成
const char *CreateGroupParams(const int groupType, const char *deviceId, const char *groupName,
    const int groupVisibility);
char *DeleteGroupParams(const char *groupId);
char *AddParams(const char *groupId, const int groupType, const char *pincode, const char *connectParams);
char *AuthParams(const char *peerConnDeviceId, const char *pkgName, bool isClient, bool level, const char *groupId,
    int keyLength, const char *userId);
char *DeleteParams(const char *peerAuthId, const char *groupId, bool isForceDelete);
void PrepareQueryData(void);
const char *CreateAccoutCG(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr);
const char *CreateDiffAccoutCG(int64_t requestId, const char *userId, const char *peerUserId);
const char *CreateImportData(const char *baseInfo, const char *groupId, int groupType);
const char *CreateReqJson(const char *version, const char *deviceId, const char *userId);
const char *CreateLocalCred(const char *baseInfo, const char *deviceId);
const char *CreateSymmInfo(const char *info1, const char *info2, const char *groupId, int groupType);
const char *DelSymmInfo(const char *groupId, int groupType);

// 异常场景构造
int DuplicateMessage(int32_t osAccountId, int64_t requestId, const char *appId, const char *authParams, int sockfd);
char *AuthParamsError(const char *peerConnDeviceId, const char *pkgName, bool isClient, bool level, const char *groupId,
    int keyLength, const char *userId);
const char *CreateAccoutError(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr);
const char *CreateAccoutPK(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr);
const char *CreateCroAccoutCG(int64_t requestId, int osAccountId, const char *userId, const char *pkInfoStr);
const char *GetCroPKInfo(const char *groupId, int groupType);
const char *CreatAsyCroAccount(int sockfd, const char *deviceId);
const char *CreateSymmCroAcount(int sockfd, const char *groupId, const char *info);
const char *FixPkInfoStr(const char *pkInfoStr);
int *LocalAccountClientDup(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams);
int *LocalAccountServerDup(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams);
int *LocalClientSame(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams);
int *LocalServerSame(int32_t osAccountId, int64_t clientReqId, int64_t serverReqId, const char *authParams);

// server端逻辑
void SetServer();
void SwitchCase(int64_t requestId, int operCode, const char *data, int sockfd);
void ToServerReg(int sockfd, const char *appId);
void ToServerDeleteGroup(int sockfd);
void ToServerDelete(int sockfd);
void TestServerDelete(int sockfd);
void ToServerInit(int sockfd);
void ServerInit(int sockfd);
void ToServerDestroy(int sockfd);
void ServerDestroy(int sockfd);
const char *ToServerCreateGroup(int sockfd, const char *credential);
const char *ToGetServerPK(int sockfd);
int *ToServerBind(int sockfd, const char *groupId, const char *pinCode);
void ServerBind(int sockfd, const char *data);
int *ToServerAuth(int sockfd, const char *deviceId, const char *userId, int type, bool isDeviceLevel);
void ToServerClose(int sockfd);
void ToSerCancelBind(int sockfd, int32_t requestId, const char *appId);
void ToSerCancelAuth(int sockfd, int32_t requestId, const char *appId);
// demo function
void DemoEcSpeke(const char* messageDir, int32_t requestId);
#endif