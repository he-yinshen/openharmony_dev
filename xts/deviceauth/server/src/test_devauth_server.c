/*
 * Copyright(c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permission and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include "device_auth_socket.h"
#include "device_auth_func.h"

static int g_listenfd;
static int g_maxIterNum = 50;
static int g_port = 15051;
// Handlers for SIGINT and SIGTERM
void EXIT(int sig)
{
    // Close m_g_listenfd to release the resource
    CloseListen(g_listenfd);
    exit(0);
}
 
// The main function of the thread that communicates with the client
void *PthMain(void *arg);
 
int main()
{
    SetAccessToken();
    // Ignore all signals
    for (int ii = 0; ii < g_maxIterNum; ii++) {
        if (signal(ii, SIG_IGN) != 0) {
            printf("signal SIG_IGN.\n");
        }
    }
    // Set the handlers for SIGINT and SIGTERM
    if (signal(SIGINT, EXIT) != 0) {
        printf("signal init.\n");
    }
    if (signal(SIGTERM, EXIT) != 0) {
        printf("signal term.\n");
    }
    g_listenfd = InitServer(g_port);
    if (g_listenfd == -1) {
        printf("server init failed, exit.\n");
        return -1;
    }
    printf("init sucess\n");
    // SetSocket(g_listenfd);
    pthread_attr_t atrr = {0};
    pthread_attr_init(&atrr);
    pthread_attr_setscope(&atrr, PTHREAD_SCOPE_SYSTEM);
    pthread_attr_setdetachstate(&atrr, PTHREAD_CREATE_DETACHED);
    while (1) {
        printf("wait accept:%d\n", g_listenfd);
        int sockfd = Accept(g_listenfd);
        if (sockfd == -1) {
            continue;
        }
        pthread_t pthid;
        if (pthread_create(&pthid, NULL, PthMain, (void*)((long)sockfd)) != 0) {
            printf("Failed to create thread. Program exits.\n");
            return -1;
        }
        printf("A thread to communicate with the client has been created.\n");
    }
}

void *PthMain(void *arg)
{
    // The arg parameter is the socket of the new client.
    int sockfd = (long) arg;
    const char *recvData = RecvData(sockfd);
    while (strcmp(recvData, "close") != 0) {
        int64_t tmpReqId = 10;
        GetReqIdFromData(&tmpReqId, recvData);
        int opercode = GetOperCodeFromData(recvData);
        printf("chose function\n");
        SwitchCase(tmpReqId, opercode, recvData, sockfd);
        recvData = RecvData(sockfd);
        printf("recvData\n");
    }
    CloseClient(sockfd);
    printf("The client has been disconnected.\n");
    return 0;
}