# openharmony_he

#### 介绍
OpenHarmony资料库，用于存放开发资料，包括代码和文档，提升OpenHarmony开发效率。

 **OH TDD用例覆盖率测试指导** 

**Step1：在GN文件中增加编译选项** 

```
cflags = [
  "--coverage",
]

ldflags = [
  "--coverage",
]
```
> 注意：需要在待测试模块的所有编译产物（不包括测试用例产物）的GN文件的cflags和ldflags配置中增加"--coverage"，如果没有定义cflags或者ldflags，则需新增定义，并添加"--coverage"

 **Step2：编译覆盖率版本和目标测试用例** 
1.编译覆盖率版本
```
./build.sh --product-name rk3568 --ccache --target-cpu arm64
```
2.编译测试用例

```
./build.sh --product-name rk3568 --ccache --target-cpu arm64 --build-target crypto_framework_test
```

 **Step3：将step2中编译出的覆盖率版本刷到板子中** 

 **Step4：执行用例环境准备** 
1.windows安装python3.8以上
2.计算云安装lcov（蓝区计算云已安装）

```
sudo apt install lcov
```

3.windows新建test文件夹
4.在test目录下右键打开git-bash，执行如下命令下载developertest和xdevice:

```
git clone https://gitee.com/openharmony/test_developertest developertest
git clone https://gitee.com/openharmony/test_xdevice xdevice
```
下载完之后test路径结构如下：
![输入图片说明](https://foruda.gitee.com/images/1663830658648245488/fb661e07_10044743.jpeg "test路径.JPG")
5.执行如下命令：

```
cd xdevice
python setup.py install
```
 **Step5：将step2中编译出的测试用例复制到test文件夹中** 
将源码out/rk3568/tests路径下的unittest文件夹复制到step4中创建的test目录下，和developertest以及xdevice放在同级目录，如下：
![输入图片说明](https://foruda.gitee.com/images/1663819087813040724/2044a006_10044743.jpeg "路径.jpg")
 **Step6：修改developertest/config/user_config.xml** 
1.将test_cases的dir设置为unittest文件夹所在路径（设置为../..即可）
2.将coverage的outpath设置为源码的out/rk3568的绝对路径
```
<!-- configure test cases path -->
<test_cases>
  <dir>../..</dir>
</test_cases>
<!-- reserved field, configure output path for coverage -->
<coverage>
  <outpath>/home/heyinshen/OH_new/out/rk3568</outpath>
</coverage>
```
 **Step7：执行用例** 
> 注意：必须要使用hdc_std，不能使用hdc，并且要确保windows的hdc_std和设备上的hdcd版本号一致。

cd到developertest路径下执行start.bat：
![输入图片说明](https://foruda.gitee.com/images/1663827000249243201/b43ab0ec_10044743.jpeg "执行用例.jpg")
不用选择，直接回车！会出现如下提示：
![输入图片说明](https://foruda.gitee.com/images/1663827259653530816/8a2cf984_10044743.jpeg "输入命令.JPG")
输入测试命令：

```
run -t UT -tp crypto_framework -cov coverage
```
等待用例跑完即可。跑完之后预期会在developertest/reports目录下自动生成coverage文件夹，如下所示：
![输入图片说明](https://foruda.gitee.com/images/1663827712837766553/6ae280ed_10044743.jpeg "测试结果.JPG")
 **Step:8：解压localCoverage.rar文件并复制到源码的test路径下** 
![输入图片说明](https://foruda.gitee.com/images/1663841188784194488/cf65a16e_10044743.jpeg "test路径.JPG")
在源码根目录下执行：

```
dos2unix test/localCoverage/codeCoverage/codeCoverage_gcov_lcov.py
dos2unix test/localCoverage/codeCoverage/llvm-gcov.sh.py
```
 **Step9：将step7生成的coverage文件夹复制到源码的test/localCoverage/codeCoverage/results路径**
1.在源码test/localCoverage/codeCoverage路径下新建results文件夹：

```
cd test/localCoverage/codeCoverage
mkdir results
```
2.将coverage文件夹复制到results路径下，如下：
![输入图片说明](https://foruda.gitee.com/images/1663828526973752871/731dcfe5_10044743.jpeg "results.JPG")
 **Step10：修改test/localCoverage/codeCoverage/codeCoverage_gcov_lcov.py** 
1.将第20行的CODEPATH改为源码根目录的绝对路径
2.将第30行的OUTPUT改为"out/rk3568"
```
19 #代码根目录
20 CODEPATH = "/home/heyinshen/OH_new"
21 #子系统json目录
22 SYSTEM_JSON = "build/subsystem_config.json"
23 # 覆盖率gcda
24 COVERAGE_GCDA_RESULTS = "test/localCoverage/codeCoverage/results/coverage/data/cxx"
25 #报告路径
26 REPORT_PATH = "test/localCoverage/codeCoverage/results/coverage/reports/cxx"
27 #llvm-gcov.sh
28 LLVM_GCOV = "test/localCoverage/codeCoverage/llvm-gcov.sh"
29 #编译生成的out路径
30 OUTPUT = "out/rk3568"
```
3.将不想要的路径加入到remove变量进行移除(可选):
![输入图片说明](https://foruda.gitee.com/images/1665971870437987405/69221c77_10044743.jpeg "removepath.JPG")
 **Step11：修改test/localCoverage/codeCoverage/llvm-gcov.sh** 
llvm-cov工具路径一般是在源码的如下路径：

```
prebuilts/clang/ohos/linux-x86_64/llvm/bin/llvm-cov
```
将llvm-gcov.sh中的路径改为上面llvm-cov的绝对路径，如下：

```
exec  /home/heyinshen/OH_new/prebuilts/clang/ohos/linux-x86_64/llvm/bin/llvm-cov  gcov "$@"
```
 **Step12：执行python codeCoverage_gcov_lcov.py命令**
 ![输入图片说明](https://foruda.gitee.com/images/1663829352496910564/25b36e0f_10044743.jpeg "执行python命令.JPG")
 **Step13：在test/localCoverage/codeCoverage/results/coverage/reports/cxx/html路径下查看报告**
 打开index.html，可以看到报告如下所示：
![输入图片说明](https://foruda.gitee.com/images/1663842895884239053/b064e8a6_10044743.jpeg "覆盖率测试结果.JPG")